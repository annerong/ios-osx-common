//
// Created by Doug Keen on 5/30/13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "DDXMLNode+CDATA.h"
#import "DDXMLDocument.h"


@implementation DDXMLNode (CDATA)

+ (id)cdataElementWithName:(NSString *)name stringValue:(NSString *)string {
    NSUInteger cdataEndTokenLocation = [string rangeOfString:@"]]>"].location;
    NSString *nodeString;
    if (cdataEndTokenLocation != NSNotFound) {
        // We need to split the ]]> token
        nodeString = [NSString stringWithFormat:@"<%@><![CDATA[%@]]><![CDATA[%@]]></%@>",
                                                name,
                                                [string substringToIndex:cdataEndTokenLocation + 2],
                                                [string substringFromIndex:cdataEndTokenLocation + 2],
                                                name];
    } else {
        nodeString = [NSString stringWithFormat:@"<%@><![CDATA[%@]]></%@>", name, string, name];
    }
    DDXMLElement *cdataNode = [[DDXMLDocument alloc] initWithXMLString:nodeString
                                                               options:DDXMLDocumentXMLKind
                                                                 error:nil].rootElement;
    return [cdataNode copy];
}

@end
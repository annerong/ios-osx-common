#import <Foundation/Foundation.h>
#import "XMPP.h"

@protocol XMPPResource;


@protocol XMPPUser <NSObject>
@required

// HIPCHAT CODE - Add mobile
// Change nickname to name, displayName, mentionName
- (XMPPJID *)jid;
//- (NSString *)nickname;

- (NSString *)name;
- (NSString *)displayName;
- (NSString *)mentionName;
- (NSString *)mobile;
// END HIPCHAT

- (BOOL)isOnline;
- (BOOL)isPendingApproval;

- (id <XMPPResource>)primaryResource;
- (id <XMPPResource>)resourceForJID:(XMPPJID *)jid;

- (NSArray *)allResources;

@end

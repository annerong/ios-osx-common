//
//  ChatHelpers.m
//  HCCommon
//
//  Created by Christopher Rivers on 10/25/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "ChatHelpers.h"

#import "Helpers.h"
#import "HipChatApp.h"
#import "HipChatUser.h"
#import "RegexKitLite.h"
#import "Room.h"
#import "XMPPFramework.h"

@implementation ChatHelpers

static const NSInteger TRUNCATE_CHARS = 800;
static const NSInteger TRUNCATE_LINES = 6;
static const NSInteger IDLE_SECONDS_BEFORE_NEW_BLOCK = 60;

static NSString *imageToggleArrowDown;
static NSString *imageToggleArrowUp;

+ (void)initialize {
    imageToggleArrowDown = [[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"arrow_drawer_down" ofType:@"png"]] absoluteString];
    imageToggleArrowUp = [[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"arrow_drawer" ofType:@"png"]] absoluteString];
}

///////////////////////////////////////////////////////////////////////
#pragma mark Chat display logic
///////////////////////////////////////////////////////////////////////

+ (NSString *)generateBody:(NSString *)text type:(NSString *)type args:(NSDictionary *)args {
    // Escape html and linkify
	// Don't escape system messages (we ensure they"re XHTML on the server)
	BOOL isMonospace = (BOOL)[[args objectForKey:@"monospace"] boolValue];
	BOOL doEscape = ![type isEqualToString:@"system"] && ![[args objectForKey:@"suppressEscaping"] boolValue];

	BOOL doEmoticons = (doEscape &&
                        !isMonospace &&
                        [[NSUserDefaults standardUserDefaults] boolForKey:@"enableEmoticons"]);
	// Don"t linkify if we"re going to truncate a pasted block by # of characters
	// (links are fine if we're truncating by line)
	BOOL doLinkify = (doEscape && !isMonospace);
	BOOL doMentions = doLinkify;
    
    HipChatApp *app = HipChatApp.instance;
	NSString *body = [Helpers escapeAndLinkify:text
                                     nameRegex:app.currentUser.mentionHighlightRegex
                                  mentionRegex:[args objectForKey:@"mentionRegex"]
                                      fullHtml:YES
                                       matches:nil
                                      doEscape:doEscape
                                     doLinkify:doLinkify
                                   doEmoticons:doEmoticons
                                    doMentions:doMentions];

	if (doEscape) {
		body = [Helpers truncatePaste:body maxLines:TRUNCATE_LINES maxLength:TRUNCATE_CHARS isHTML:YES];
	}
    // Escape any backslashes so they don't interfere with the javascript evaluation
    return [body stringByReplacingOccurrencesOfString:@"\\" withString:@"\\\\"];
}

+ (NSArray *)generateJavascriptsForText:(NSString *)text type:(NSString *)type args:(NSMutableDictionary *)args {
	HipChatApp *app = HipChatApp.instance;
    NSMutableArray *javascripts = [NSMutableArray array];

    if (!text)
		return javascripts;

    NSTimeInterval lastActive = [(NSNumber *)[args objectForKey:@"lastActive"] doubleValue];
    XMPPJID *lastMessageFrom = [args objectForKey:@"lastMessageFrom"];
    NSTimeInterval oldestMessageTime = [(NSNumber *)[args objectForKey:@"oldestMessageTime"] integerValue];
    NSNumber *isEchoObj = [args objectForKey:@"isEcho"];
    BOOL isEcho = isEchoObj != nil && [isEchoObj boolValue];
    BOOL isSystem = [[args objectForKey:@"isSystem"] boolValue];

    NSTimeInterval messageTime = [ChatHelpers getMessageTimeForArgs:args];
	BOOL insertOnTop = [[args objectForKey:@"insertOnTop"] boolValue];

	XMPPJID *from = [args objectForKey:@"from"];

	// System messages send in a display name manually
	// Don't override display with fromJID unless it's not set
	if (from && ![args objectForKey:@"displayName"]) {
		NSString *displayName = [app getDisplayName:from abbreviated:YES];
		if (!displayName) {
			displayName = @"Unknown";
		}
		[args setObject:displayName forKey:@"displayName"];
	}

	if (from &&
		[type isEqualToString:@"chat"] &&
		[[text componentsMatchedByRegex:@"^(/me |/em )"] count] > 0) {
		type = @"info";
		text = [NSString stringWithFormat:@"%@ %@", [app getDisplayName:from abbreviated:NO], [text substringFromIndex:4]];
	}

	if (from &&
		[type isEqualToString:@"chat"] &&
		[[text componentsMatchedByRegex:@"^(/quote )"] count] > 0) {
		[args setObject:[NSNumber numberWithBool:YES] forKey:@"monospace"];
		text = [text substringFromIndex:7];
	}
    
	if (from &&
		[type isEqualToString:@"chat"] &&
		[[text componentsMatchedByRegex:@"^(/code )"] count] > 0) {
		[args setObject:[NSNumber numberWithBool:YES] forKey:@"monospace"];
		[args setObject:[NSNumber numberWithBool:YES] forKey:@"code"];
		text = [text substringFromIndex:6];
	}
    
    if (from && insertOnTop) {
        NSDictionary *historyReplacement = [args objectForKey:@"historyReplacement"];
        if (historyReplacement) {
            text = [text stringByReplacingOccurrencesOfString:[historyReplacement objectForKey:@"match"]
                                                   withString:[historyReplacement objectForKey:@"replacement"]];
            [args removeObjectForKey:@"historyReplacement"];
        }
    }

	// Check for multiline text - remove any excess line whitespace from multi-line messagess
	NSInteger numLines = [Helpers getNumLines:text];
	if (numLines > 1) {
		text = [Helpers formatMultilineBlock:text];
	}

    NSString *body = [ChatHelpers generateBody:text type:type args:args];

	NSDate *messageDate = [NSDate dateWithTimeIntervalSince1970:messageTime];
	NSDate *lastActiveDate = (insertOnTop ?
							  [NSDate dateWithTimeIntervalSince1970:oldestMessageTime] :
							  [NSDate dateWithTimeIntervalSince1970:lastActive]);

	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *messageDateComp =
    [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                 fromDate:messageDate];
	NSDateComponents *lastActiveComp =
    [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit)
                 fromDate:lastActiveDate];

	if (!isEcho &&
        lastActive && // Don't add date dividers when we have no real last active date
		([messageDateComp day] != [lastActiveComp day] ||
		 [messageDateComp month] != [lastActiveComp month] ||
		 [messageDateComp year] != [lastActiveComp year])) {
            lastMessageFrom = nil;
			[javascripts addObject:[ChatHelpers getDateDividerJavascript:(insertOnTop ? lastActiveDate : messageDate)
                                                             insertOnTop:insertOnTop]];
        }

	// We only append to the current block if:
    // the type isn't specifically "block",
    // this msg is from the same user that last sent a message
    // it's not monospace or truncated
	// and the message comes within 1 minute of the previous one
	// Make sure we set lastMessageFrom after checking for dividers
	// (since they clear the last from value)
	if (isEcho) {
		// This is just an echo - no chat block / line to add
	} else if (![Helpers isBlockChatType:type] &&
               !insertOnTop &&
               !isSystem &&
               numLines < TRUNCATE_LINES &&
               text.length < TRUNCATE_CHARS &&
               ![args objectForKey:@"monospace"] &&
               from &&
               lastActive &&
               (messageTime - lastActive) < IDLE_SECONDS_BEFORE_NEW_BLOCK &&
               lastMessageFrom &&
               [lastMessageFrom.bare isEqualToString:from.bare]) {
		[javascripts addObject:[ChatHelpers getChatLineJavascriptForBody:body args:args]];
	} else {
		[javascripts addObject:[ChatHelpers getChatBlockJavascriptForBody:body type:type args:args]];
	}

    // Add preview data if found
    NSString *previewDataJs = [ChatHelpers getPreviewDataJavascriptForType:type args:args];
    if (previewDataJs) {
        [javascripts addObject:previewDataJs];
    }

    // Don't udpate any "current state" variables for echoed messages
	if (isEcho) {
        return javascripts;
    }

	// Update oldestMessageTime if we're getting an even older message
	if (messageTime < oldestMessageTime) {
		// If this is an old message, update
        [args setObject:[NSNumber numberWithDouble:messageTime] forKey:@"oldestMessageTime"];
	}

	// Only update lastActive and lastMessageFrom if this message is the newest one we've handled
	if ([[args objectForKey:@"isCurrent"] boolValue] ||  messageTime >= lastActive) {
        [args setObject:[NSNumber numberWithDouble:messageTime] forKey:@"lastActive"];

		// Set last from jid if we're getting a chat or file type block
		if (![Helpers isBlockChatType:type]) {
			if (from) {
                [args setObject:[XMPPJID jidWithString:from.full] forKey:@"lastMessageFrom"];
			}
		} else {
            [args removeObjectForKey:@"lastMessageFrom"];
		}
	}

    return javascripts;
}

+ (NSMutableDictionary *)getArgsForMessage:(XMPPMessage *)msg {
    HipChatApp *app = HipChatApp.instance;
    XMPPJID *from = msg.from;
    Room *room = [app getRoomInfo:from];

    [XMPPJID jidWithString:[Helpers getNodeStringValue:msg forPath:@"./@from"]];
	NSDate *delayDate = msg.delayedDeliveryDate;
	NSMutableDictionary *args = [NSMutableDictionary dictionaryWithObjectsAndKeys:
								 @"chat", @"messageType",
								 (delayDate ? [NSNumber numberWithBool:NO] : [NSNumber numberWithBool:YES]), @"isCurrent",
								 nil];

    // Set mention regex so we only highlight actual mentions
    if (room && room.mentionRegex) {
        [args setObject:room.mentionRegex forKey:@"mentionRegex"];
    } else if (app.mentionRegex) {
        [args setObject:app.mentionRegex forKey:@"mentionRegex"];
    }

	// Sender represents the person that sent the message (not always the same as from)
	// (e.g. getting history for a private chat)
	XMPPJID *sender = [Helpers getSenderForMessage:msg];
    if (delayDate) {
		[args setObject:delayDate forKey:@"time"];
		[args setObject:[NSNumber numberWithBool:YES] forKey:@"isHistory"];
	} else {
		[args setObject:[NSDate date] forKey:@"time"];
	}

	if ([Helpers isRoomJid:from] && sender && from.resource) {
		// Save the name/jid pair for lookup.
		// This should only be necessary if we get a message from someone who we haven't seen in the roster yet
		// Make sure to set display name for guest users
		[app setDisplayNameForJid:sender withFullName:from.resource displayName:from.resource];
		[app setNicknameForJid:sender withNickname:from.resource];
	}

	if (sender) {
		[args setObject:sender forKey:@"from"];
	} else {
		return args;
	}

    // Check for file / announcement / metadata
	NSXMLElement *mucNode = [msg elementForName:@"x" xmlns:@"http://hipchat.com/protocol/muc#room"];
	NSXMLElement *fileNode = [mucNode elementForName:@"file"];
    if (fileNode) {
		[args setObject:@"file" forKey:@"messageType"];
		[args setObject:fileNode forKey:@"fileData"];
    }

	NSXMLElement *typeNode = [mucNode elementForName:@"type"];
    if (typeNode) {
		NSString *typeValue = [typeNode stringValue];
		if ([typeValue isEqualToString:@"system"] || [typeValue isEqualToString:@"announcement"]) {
			[args setObject:from.resource forKey:@"displayName"];
            [args setObject:[NSNumber numberWithBool:YES] forKey:@"isSystem"];
			NSString *color = [[mucNode elementForName:@"color"] stringValue];
			if (color) {
				[args setObject:color forKey:@"color"];
			}
			NSString *messageFormat = [[mucNode elementForName:@"message_format"] stringValue];
			if ([messageFormat isEqualToString:@"text"]) {
				[args setObject:[NSString stringWithFormat:@"systemMessage %@", color] forKey:@"className"];
			} else {
                [args setObject:@"system" forKey:@"messageType"];
            }
		}
    }

	NSXMLElement *hcNode = [msg elementForName:@"x" xmlns:@"http://hipchat.com"];
	NSArray *metadataNodes = [hcNode elementsForName:@"metadata"];
	if ([metadataNodes count] > 0) {
		NSXMLElement *metadataNode = [metadataNodes objectAtIndex:0];
		if ([metadataNode elementForName:@"vcconnect"]) {
			// Video chat request
			[args setObject:@"vcconnect" forKey:@"messageType"];
		} else if ([metadataNode elementForName:@"type"]) {
			// General set the type of the message based on metadata type if available
			[args setObject:[[metadataNode elementForName:@"type"] stringValue] forKey:@"messageType"];
		}
		[args setObject:metadataNodes forKey:@"metadatas"];
	}

	NSXMLElement *guestAccessNode = [hcNode elementForName:@"guest_url"];
    if (guestAccessNode) {
		NSString *url = [guestAccessNode stringValue];
		NSString *name = [app getDisplayName:sender abbreviated:NO];
		[args setObject:@"info" forKey:@"messageType"];
		[args setObject:[NSNumber numberWithBool:NO] forKey:@"notify"];

		if (url && [url length] > 0) {
			// Only change the current state if it's a current message
			if (![args objectForKey:@"isHistory"]) {
                [room setGuestUrl:url];
				// TODO - what to do for guest access?
				//[ChatHelpers enableGuestAcces:url];
			}
			[args setObject:[NSString stringWithFormat:@"%@ turned on guest access", name] forKey:@"body"];
		} else {
			// Only change the current state if it's a current message
			if (![args objectForKey:@"isHistory"]) {
                [room setGuestUrl:nil];
				// TODO: what to do for guest access?
				//[ChatHelpers disableGuestAccess];
			}
			[args setObject:[NSString stringWithFormat:@"%@ turned off guest access", name] forKey:@"body"];
		}
    }

    NSXMLElement *archivedNode = [hcNode elementForName:@"is_archived"];
    if (archivedNode) {
		BOOL isArchived = [archivedNode stringValueAsBool];
		NSString *name = [app getDisplayName:sender abbreviated:NO];
		[args setObject:@"info" forKey:@"messageType"];
		[args setObject:[NSNumber numberWithBool:NO] forKey:@"notify"];

		if (isArchived) {
			// Only change the current state if it's a current message
			if (![args objectForKey:@"isHistory"]) {
                room.isArchived = YES;
			}
			[args setObject:[NSString stringWithFormat:@"%@ archived the room", name] forKey:@"body"];
		} else {
			// Only change the current state if it's a current message
			if (![args objectForKey:@"isHistory"]) {
                room.isArchived = NO;
			}
			[args setObject:[NSString stringWithFormat:@"%@ unarchived the room", name] forKey:@"body"];
		}
    }

    // topic change
	NSXMLElement *topicNode = [msg elementForName:@"subject"];
    if (topicNode) {
		XMPPJID *fromArg = [args objectForKey:@"from"];
        NSString *topic = [topicNode stringValue];
		if (fromArg) {
            // If the topic doesn't come from a user, we're assuming it comes from the API
            // Append "(API)" after the name to avoid confusion
			[args setObject:@"info" forKey:@"messageType"];
            if ([Helpers isUserJid:fromArg]) {
                NSString *name = [app getDisplayName:fromArg abbreviated:NO];
                if (topic.length > 0) {
                    [args setObject:[NSString stringWithFormat:@"%@ changed the topic to: %@", name, topic] forKey:@"body"];
                } else {
                    [args setObject:[NSString stringWithFormat:@"%@ cleared the topic.", name] forKey:@"body"];
                }
            } else {
                if (topic.length > 0) {
                    [args setObject:[NSString stringWithFormat:@"The topic was changed: %@", topic] forKey:@"body"];
                } else {
                    [args setObject:[NSString stringWithFormat:@"The topic was cleared."] forKey:@"body"];
                }
            }
		}
        // Update current state if this is not a history message
        if (![args objectForKey:@"isHistory"]) {
            [room setTopic:topic];
        }
    }

    return args;
}

+ (NSString *)getChatBlockJavascriptForBody:(NSString *)body type:(NSString *)type args:(NSMutableDictionary *)args {
    HipChatApp *app = HipChatApp.instance;
	HipChatUser *currentUser = app.currentUser;

	NSString *argsClassName = [args objectForKey:@"className"];
	NSString *senderClassName = @"them";
	XMPPJID *from = [args objectForKey:@"from"];
	if (from) {
		if ([from.bare isEqualToString:currentUser.jid.bare] ||
			[from.bare isEqualToString:currentUser.nicknameJid.bare]) {
			senderClassName = @"me";
		}
	}

	NSString *className = [NSString stringWithFormat:@"%@ %@",
						   senderClassName,
						   (argsClassName ? argsClassName : @"")];
	if ([args objectForKey:@"monospace"]) {
		className = [NSString stringWithFormat:@"%@ monospace", className];
	}

    NSInteger currentBlockId = (NSInteger)[[args objectForKey:@"currentBlockId"] integerValue];
    currentBlockId++;
	NSString *blockId = [NSString stringWithFormat:@"block%ld", (long)currentBlockId];

	[args setObject:className forKey:@"className"];
	[args setObject:blockId forKey:@"blockId"];
    [args setObject:[NSNumber numberWithInteger:currentBlockId] forKey:@"currentBlockId"];

	NSString *html = @"";
	if ([type isEqualToString:@"chat"] ||
		[type isEqualToString:@"image"] ||
		[type isEqualToString:@"link"] ||
		[type isEqualToString:@"twitter_status"] ||
		[type isEqualToString:@"twitter_user"] ||
		[type isEqualToString:@"video"] ||
		[type isEqualToString:@"youtube_video"]) {
        html = [ChatHelpers getChatHtmlForBody:body args:args];
    } else if ([type isEqualToString:@"chat_state"]) {
        html = [ChatHelpers getChatStateHtmlForBody:body args:args];
    } else if ([type isEqualToString:@"file"]) {
        html = [ChatHelpers getFileHtmlForBody:body args:args];
    } else if ([type isEqualToString:@"info"]) {
        html = [ChatHelpers getInfoHtmlForBody:body args:args];
    } else if ([type isEqualToString:@"system"]) {
        html = [ChatHelpers getSystemHtmlForBody:body args:args];
    } else if ([type isEqualToString:@"welcome"]) {
        html = [ChatHelpers getWelcomeHtmlForBody:body args:args];
    } else if (body.length > 0) {
        html = [ChatHelpers getChatHtmlForBody:body args:args];
		DDLogError(@"Unknown chat text type: %@", type);
	}

	// We can send newlines in the string directly, escape them as \n
	html = [html stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
	html = [html stringByReplacingOccurrencesOfRegex:@"[\n\r]+" withString:@"\\\\n"];

	BOOL insertOnTop = [[args objectForKey:@"insertOnTop"] boolValue];
    BOOL isCode = [args objectForKey:@"code"] != nil && [[args objectForKey:@"code"] boolValue];
	NSString *javascript = [NSString stringWithFormat:@"%@addChatBlock(\"%@\", %@)%@;", (isCode ? @" prettyPrint(\"\", " : @""), html, (insertOnTop ? @"true" : @"false"), (isCode ? @")" : @"")];
    return javascript;
}

+ (NSString *)getChatDividerJavascript {
    return @"addChatDivider();";
}

+ (NSString *)getChatLineJavascriptForBody:(NSString *)body args:(NSMutableDictionary *)args {
    NSString *className = @"msgText";
	if ([args objectForKey:@"monospace"]) {
		className = @"msgText monospace";
	}

    NSInteger currentBlockId = (NSInteger)[[args objectForKey:@"currentBlockId"] integerValue];
	NSString *blockId = [NSString stringWithFormat:@"block%ld", (long)currentBlockId];
	NSString *messageId = [args objectForKey:@"messageId"];
	if (!messageId) {
		messageId = @"";
	}
	body = [[NSMutableString stringWithString:body] stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
	NSString *javascript = [NSString stringWithFormat:@"addChatLine(\"%@\", \"%@\", \"%@\", \"%@\")", blockId, className, body, messageId];
    return javascript;
}

+ (NSString *)getDateDividerJavascript:(NSDate *)date insertOnTop:(BOOL)insertOnTop {
    return [NSString stringWithFormat:@"addDateDivider('%@', %@);",
            [Helpers formatDate:date],
            (insertOnTop ? @"true" : @"false")];
}

+ (NSTimeInterval)getMessageTimeForArgs:(NSDictionary *)args {
    NSTimeInterval messageTime = [[args objectForKey:@"time"] timeIntervalSince1970];

	if (!messageTime) {
		messageTime = [[NSDate date] timeIntervalSince1970];
	}

    return messageTime;
}

+ (NSString *)getPreviewDataJavascriptForType:(NSString *)type args:(NSMutableDictionary *)args {
    NSString *html = @"";
	NSString *className = @"";

	if ([type isEqualToString:@"image"]) {
        html = [ChatHelpers getImageHtmlWithArgs:args];
    } else if ([type isEqualToString:@"link"]) {
        className = @"linkBlock";
        html = [ChatHelpers getLinkHtmlWithArgs:args];
    } else if ([type isEqualToString:@"twitter_status"] || [type isEqualToString:@"twitter_user"]) {
        className = @"linkBlock";
        html = [ChatHelpers getTwitterHtmlWithArgs:args];
    } else if ([type isEqualToString:@"video"] || [type isEqualToString:@"youtube_video"]) {
        className = @"linkBlock";
        html = [ChatHelpers getVideoHtmlWithArgs:args];
	}

	if ([html length] > 0) {
		html = [html stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
		html = [html stringByReplacingOccurrencesOfRegex:@"[\n\r]" withString:@"\\\\n"];
		NSString *javascript = [NSString stringWithFormat:@"addPreviewData(\"%@\", \"%@\", \"%@\");", [args objectForKey:@"messageId"], html, className];
        return javascript;
	}

    return nil;
}

+ (NSString *)getReasonForDeparture:(NSString *)status {
	if (!status || [status length] == 0) return nil;

	if ([status isEqualToString:@"hc-conflict"]) {
		return @"logged in from a different location";
	} else if ([status isEqualToString:@"hc-disconnect"]) {
		return @"user disconnected";
	} else if ([status isEqualToString:@"hc-leave"]) {
		return nil;
	} else if ([status isEqualToString:@"hc-not-allowed"]) {
		return @"guest access disabled";
	} else if ([status isEqualToString:@"hc-timeout"]) {
		return @"lost connection";
	}

	return status;
}

///////////////////////////////////////////////////////////////////////
#pragma mark Chat block HTML
///////////////////////////////////////////////////////////////////////

+ (NSString *)getChatHtmlForBody:(NSString *)body args:(NSMutableDictionary *)args {
	// Use <pre> tags for monospace pastes
	NSString *blockElement = ([args objectForKey:@"monospace"] ? @"pre" : @"p");
	NSString *msgId = [args objectForKey:@"messageId"];
	if (!msgId) {
		msgId = @"";
	}

    NSString *blockElementClass;
    if ([args objectForKey:@"code"]) {
        blockElementClass = @"msgText prettyprint";
    } else {
        blockElementClass = @"msgText";
    }

	NSString *html = [NSString stringWithFormat:
					  @"<div class='chatBlock %@'>"
                      "<table width='100%%' cellspacing='0' cellpadding='0'><tr>"
                      "<td class='nameBlock'><p><a href='action:mention:%@'>%@</a></p></td>"
                      "<td id='%@' class='messageBlock'><p class='timeBlock'>%@</p><%@ id='%@' class='%@'>%@</%@></td>"
                      "</tr></table>"
					  "</div>",
					  [args objectForKey:@"className"],
					  [args objectForKey:@"from"],
					  [args objectForKey:@"displayName"],
					  [args objectForKey:@"blockId"],
					  [Helpers formatTime:[args objectForKey:@"time"]],
					  blockElement,
					  msgId,
                      blockElementClass,
					  body,
					  blockElement];

	return html;
}

+ (NSString *)getChatStateHtmlForBody:(NSString *)body args:(NSMutableDictionary *)args {
	NSString *html = [NSString stringWithFormat:
					  @"<div class='infoBlock chatBlock %@' name='state_message'>"
                      "<table width='100%%' cellspacing='0' cellpadding='0'><tr>"
                      "<td class='nameBlock'></td>"
                      "<td class='messageBlock'><p class='timeBlock'>%@</p><p>%@</p></td>"
                      "</tr></table>"
					  "</div>",
					  [args objectForKey:@"className"],
					  [Helpers formatTime:[args objectForKey:@"time"]],
					  body];

	return html;
}

+ (NSString *)getFileHtmlForBody:(NSString *)body args:(NSMutableDictionary *)args {
	NSXMLElement *fileData = [args objectForKey:@"fileData"];
	NSString *filePath = [Helpers getNodeStringValue:fileData forName:@"name"];
	NSString *fileDesc = [Helpers getNodeStringValue:fileData forName:@"desc"];
	NSString *thumbPath = [Helpers getNodeStringValue:fileData forName:@"thumb"];
	NSInteger size = [[Helpers getNodeStringValue:fileData forName:@"size"] intValue];
	NSString *bucket = [Helpers getNodeStringValue:fileData forName:@"bucket"];

	NSString *fileUrl = [Helpers getNodeStringValue:fileData forName:@"file_url"];
    if (!fileUrl || fileUrl.length == 0) {
        fileUrl = [Helpers fileUrlWithBucket:bucket andPath:filePath];
    }

	NSString *thumbHtml = @"";
	if ([thumbPath length] > 0) {
		NSString *thumbUrl = [Helpers getNodeStringValue:fileData forName:@"file_url"];
        if (!thumbUrl || thumbUrl.length == 0) {
            thumbUrl = [Helpers fileUrlWithBucket:bucket andPath:thumbPath];
        }
        thumbHtml = [NSString stringWithFormat:@"<div class='preview'>%@</div>",
                     [self getImagePreviewWithName:@"file" url:fileUrl thumbURL:thumbUrl]];
	}

	NSString *descHtml = @"";
	if (fileDesc && fileDesc.length > 0) {
		descHtml = [NSString stringWithFormat:@"<p class='msgText'>%@</p>",
					[ChatHelpers generateBody:fileDesc type:@"file" args:args]];
	}

	// Get the file name by stripping off the path and the random characters
	// File names are formatted like <dir>/<dir>/<random chars>/<filename>.<ext>
	NSString *fileName = [Helpers getFileNameFromPath:filePath];
	NSString *iconSrc = [NSString stringWithFormat:@"http://%@/img/filetype_icons/%@", HipChatApp.instance.webHost, [Helpers getFileTypeIcon:fileName]];

	NSString *html = [NSString stringWithFormat:
					  @"<div class='chatBlock fileBlock %@'>"
					  "<table width='100%%' cellspacing='0' cellpadding='0'><tr>"
					  "<td class='nameBlock'><p><a href='action:mention:%@'>%@</a></p></td>"
					  "<td id='%@' class='messageBlock'>"
					  "<p class='timeBlock'>%@</p>"
					  "<p><img onload='if (scrollAtBottom()) { scrollToBottom(); }' class='icon' src='%@' />"
					  "<a name='file' href='%@'>%@</a>"
					  "&ensp;<span class='fileSize'>%@</span></p>"
					  "%@"
					  "%@"
					  "</td>"
					  "</tr></table>"
					  "</div>",
					  [args objectForKey:@"className"],
					  [args objectForKey:@"from"],
					  [args objectForKey:@"displayName"],
					  [args objectForKey:@"blockId"],
					  [Helpers formatTime:[args objectForKey:@"time"]],
					  iconSrc,
					  fileUrl,
					  [Helpers escape:fileName],
					  [Helpers getFileSizeString:size],
					  thumbHtml,
					  descHtml];

	return html;
}

+ (NSString *)getInfoHtmlForBody:(NSString *)body args:(NSMutableDictionary *)args {
	// Add presence class if the type == "presence"
	XMPPJID *from = [args objectForKey:@"from"];
	NSString *fromKey = (from ? from.bare : @"");
	NSString *presenceName = ([[args objectForKey:@"type"] isEqualToString:@"presence"] ?
							  [NSString stringWithFormat:@" name='presence_%@'", fromKey] :
							  @"");

	NSString *html = [NSString stringWithFormat:
					  @"<div class='infoBlock chatBlock %@' %@>"
                      "<table width='100%%' cellspacing='0' cellpadding='0'><tr>"
#if TARGET_OS_IPHONE
                      "<td class='messageBlock'><p class='timeBlock'>%@</p><p class='msgText'>%@</p></td>"
#else
                      "<td class='nameBlock'><p></p></td>"
                      "<td class='messageBlock'><p class='timeBlock'>%@</p><p class='msgText'>%@</p></td>"
#endif
                      "</tr></table>"
					  "</div>",
					  [args objectForKey:@"className"],
					  presenceName,
					  [Helpers formatTime:[args objectForKey:@"time"]],
					  body];

	return html;
}

+ (NSString *)getSystemHtmlForBody:(NSString *)body args:(NSMutableDictionary *)args {
	NSString *color = [args objectForKey:@"color"];
	if (!color) {
		color = @"yellow";
	}
	NSString *html = [NSString stringWithFormat:
					  @"<div class='systemMessage chatBlock %@'>"
                      "<table width='100%%' cellspacing='0' cellpadding='0'><tr>"
                      "<td class='nameBlock'><p>%@</p></td>"
                      "<td class='messageBlock'><p class='timeBlock'>%@</p><p>%@</p></td>"
                      "</tr></table>"
					  "</div>",
					  color,
					  [args objectForKey:@"displayName"],
					  [Helpers formatTime:[args objectForKey:@"time"]],
					  body];

	return html;
}

+ (NSString *)getWelcomeHtmlForBody:(NSString *)body args:(NSMutableDictionary *)args {
	return @"<div class='welcomeMessage chatBlock'>"
                "<table width='100%%' cellspacing='0' cellpadding='0'><tr>"
                    "<td class='nameBlock'><p>Welcome!</p></td>"
                    "<td class='messageBlock'><p>You joined the room</p></td>"
                "</tr></table>"
            "</div>";
}

///////////////////////////////////////////////////////////////////////
#pragma mark Preview Data HTML
///////////////////////////////////////////////////////////////////////

+ (NSString *)getImageHtmlWithArgs:(NSMutableDictionary *)args {
	NSXMLElement *metadata = [[args objectForKey:@"metadatas"] objectAtIndex:0];

	NSString *name = [Helpers escape:[Helpers getNodeStringValue:metadata forName:@"name"]];

	NSString *url = [[NSMutableString stringWithString:[Helpers getNodeStringValue:metadata forName:@"url"]]
					 stringByReplacingOccurrencesOfString:@"'" withString:@"&quot;"];

	NSString *thumbUrl = [[NSMutableString stringWithString:[Helpers getNodeStringValue:metadata forName:@"image"]]
						  stringByReplacingOccurrencesOfString:@"'" withString:@"&quot;"];

//    var htmlStrings:Array = [];
//    if (name && name.length > 0) {
//        // Use inline-block so the toggle
//        htmlStrings.push('<p style="display: inline-block;">'+name+'</p>');
//    }
//    htmlStrings.push(getImagePreview("link", url, thumbUrl));
//    
    
	NSString *html = [NSString stringWithFormat:
                      @"%@%@",
                      (name && name.length > 0 ? [NSString stringWithFormat:@"<p style='display: inline-block'>%@</p>", name] : @""),
                      [self getImagePreviewWithName:@"link" url:url thumbURL:thumbUrl]];

	return html;
}

+ (NSString *)getImagePreviewWithName:(NSString *)name url:(NSString *)url thumbURL:(NSString *)thumbURL {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *imageStyle = @"";
    NSString *hiddenStyle = @"style='display: none;'";
    NSString *toggleArrow = [NSString stringWithFormat:@"<img src='%@' />", imageToggleArrowDown];
    if ([thumbURL rangeOfRegex:@".gif$"].location != NSNotFound && [prefs boolForKey:@"hideGifs"]) {
        imageStyle = @"style='display: none;'";
        hiddenStyle = @"";
        toggleArrow = [NSString stringWithFormat:@"<img src='%@' />", imageToggleArrowUp];
    }

    return [NSString stringWithFormat:@"<a class='hide' onclick='toggleImage(this);'>%@</a>"
            "<div class='image'>"
            "   <a name='%@' href='%@'><img %@ onload='if (scrollAtBottom()) { scrollToBottom(); }' class='thumbnail' src='%@' /></a>"
            "   <p %@ class='hiddenImage'><a class='imageToggle' onclick='toggleImage(this);'>Image hidden</a></p>"
            "</div>",
            toggleArrow,
            name,
            url,
            imageStyle,
            thumbURL,
            hiddenStyle];
}

+ (NSString *)getLinkHtmlWithArgs:(NSMutableDictionary *)args {
    NSArray *metadatas = [args objectForKey:@"metadatas"];

    // Concatenate all the link preview blocks
    NSMutableArray *previews = [NSMutableArray arrayWithObjects: nil];
    for (NSXMLElement *linkData in metadatas) {
        if ([[Helpers getNodeStringValue:linkData forName:@"type"] isEqualToString:@"link"]) {
            [previews addObject:[ChatHelpers getLinkPreviewHTML:linkData]];
        }
    }

    return [previews componentsJoinedByString:@""];
}

+ (NSString *)getLinkPreviewHTML:(NSXMLElement *)linkData {
	NSString *favicon = [Helpers getNodeStringValue:linkData forName:@"favicon_url"];

    NSString *headerTextString = [Helpers escape:[Helpers getNodeStringValue:linkData forName:@"header_text"]];
	NSMutableString *headerText = (headerTextString ? [NSMutableString stringWithString:headerTextString] : nil);

    NSString *linkUrlString = [Helpers getNodeStringValue:linkData forName:@"full_url"];
	NSMutableString *linkUrl = (linkUrlString ? [NSMutableString stringWithString:linkUrlString] : nil);

    NSString *linkTextString = [Helpers getNodeStringValue:linkData forName:@"link_text"];
	NSMutableString *linkText = (linkTextString ? [NSMutableString stringWithString:[Helpers escape:linkTextString]] : nil);

	NSString *desc = [Helpers getNodeStringValue:linkData forName:@"desc"];
	if (desc) {
		desc = [Helpers escapeAndLinkify:desc];
		desc = [desc stringByReplacingOccurrencesOfRegex:@"(\n)" withString:@"<br>"];
	}

	NSString *iconSrc = ((favicon && [favicon length] > 0) ?
						 favicon :
						 [NSString stringWithFormat:@"http://%@/img/filetype_icons/html.png", HipChatApp.instance.webHost]);
	NSString *titleHtml = ((headerText && [headerText length] > 0) ?
						   [NSString stringWithFormat:@"<span class='linkTitle'>%@</span>", headerText] :
						   @"");
	NSString *linkHtml = ((linkUrl && [linkUrl length] > 0) ?
						  [NSString stringWithFormat:@" <a href='%@'>%@</a>",
						   linkUrl,
						   ((linkText && [linkText length] > 0) ? linkText : linkUrl)] :
						  @"");
	NSString *descHtml = ((desc && [desc length] > 0) ?
						  [NSString stringWithFormat:@"<p class='linkDesc'>%@</p>", desc] :
						  @"");

	return [NSString stringWithFormat:
			@"<div class='linkPreview'>"
            "<img onload='if (scrollAtBottom()) { scrollToBottom(); }' class='icon' src='%@' />"
            "<p>"
            "%@%@%@"
            "<p>"
            "%@"
            "</div>",
            iconSrc,
            titleHtml,
            (titleHtml.length > 0 && linkHtml.length > 0 ? @" - " : @""),
            linkHtml,
            descHtml];
}

+ (NSString *)getTwitterHtmlWithArgs:(NSMutableDictionary *)args {
	NSXMLElement *metadata = [[args objectForKey:@"metadatas"] objectAtIndex:0];
	NSString *title = [Helpers getNodeStringValue:metadata forName:@"text"];
	if (title) {
		title = [Helpers escapeAndLinkify:title];
	}
	NSString *thumbUrl = [Helpers getNodeStringValue:metadata forName:@"profile_image_url"];
	NSString *name = [Helpers getNodeStringValue:metadata forName:@"name"];
	NSString *type = [Helpers getNodeStringValue:metadata forName:@"type"];
	NSString *author = [Helpers getNodeStringValue:metadata forName:@"screen_name"];
	NSString *authorUrl = [NSString stringWithFormat:@"http://twitter.com/%@", author];
	//NSString *url = [Helpers getNodeStringValue:metadata forName:@"url"];
	NSString *source = [Helpers getNodeStringValue:metadata forName:@"source"];
	source = [source stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];

	NSString *typeSpecificHtml = @"";

	if ([type isEqualToString:@"twitter_status"]) {
		typeSpecificHtml = [NSString stringWithFormat:@"<p>%@</p>"
							"<p class='linkDesc'>&ndash; %@ (@<a href='%@'>%@</a>) via %@</p>",
							title,
							name,
							authorUrl,
							author,
							source];
	} else if ([type isEqualToString:@"twitter_user"]) {
		NSInteger numFollowers = (int)[[Helpers getNodeStringValue:metadata forName:@"followers"] doubleValue];

		typeSpecificHtml = [NSString stringWithFormat:@"<p>%@ (@<a href='%@'>%@</a>)</p>"
							"<p class='linkDesc'>%@ followers</p>",
							name,
							authorUrl,
							author,
							[Helpers formatNumber:numFollowers]];
	}

	NSString *html = [NSString stringWithFormat:
					  @"<a class='linkImage' name='link' href='%@'><img height='48' width='48' onload='if (scrollAtBottom()) { scrollToBottom(); }' src='%@' /></a>"
					  "%@",
					  authorUrl,
					  thumbUrl,
					  typeSpecificHtml];

	return html;
}

+ (NSString *)getVideoHtmlWithArgs:(NSMutableDictionary *)args {
	NSXMLElement *metadata = [[args objectForKey:@"metadatas"] objectAtIndex:0];
	NSString *title = [Helpers getNodeStringValue:metadata forName:@"title"];
	NSString *url = [Helpers getNodeStringValue:metadata forName:@"url"];
	NSString *thumbUrl = [Helpers getNodeStringValue:metadata forName:@"thumb"];
	//NSString *type = [Helpers getNodeStringValue:metadata forPath:@"type"];
	NSInteger numViews = (int)[[Helpers getNodeStringValue:metadata forName:@"views"] doubleValue];

	NSString *desc = [NSString stringWithFormat:@"%@ views<br />%@",
					  [Helpers formatNumber:numViews],
					  [Helpers getNodeStringValue:metadata forName:@"author"]];

	NSString *html = [NSString stringWithFormat:
					  @"<a class='linkImage' name='link' href='%@'><img onload='if (scrollAtBottom()) { scrollToBottom(); }' src='%@' /></a>"
					  "<p class='linkTitle'>%@</p>"
					  "<p class='linkDesc'>%@</p>",
					  url,
					  thumbUrl,
					  title,
					  desc];

	return html;
}

///////////////////////////////////////////////////////////////////////
#pragma mark Initial HTML
///////////////////////////////////////////////////////////////////////

static NSString *_initialHTML;

static NSString *_initialHTMLFormat = @"<html>"
"<head>"
"<script type='text/javascript'>%1$@</script>"
"<style>%2$@"
// Prevent font scaling when switching orientations
"html { -webkit-text-size-adjust: none; }"
"* { margin: 0; padding: 0; word-wrap: break-word; }"
#if TARGET_OS_IPHONE
"p { font-family: Helvetica,sans-serif; }"
"p, div, .nameBlock p, .messageBlock p, .messageBlock pre { font-size: 13px; line-height: 20px; }"
"p.timeBlock { Helvetica,sans-serif; font-size: 11px; }"
".dateDivider { font-family: Helvetica,sans-serif; font-size: 11px; }"
#else
"p, div { font-family: Lucida Grande,arial,sans-serif; }"
"p, div, .nameBlock p, .messageBlock p, .messageBlock pre { font-size: 100%%; line-height: 160%%; }"
// Place p.timeBlock after .messageBlock p so it overrides the font-size
"p.timeBlock { font-size: 90%%; font-family: Lucida Grande,arial,sans-serif; }"
".dateDivider { font-family: Helvetica,sans-serif; font-size: 90%%; line-height: 200%%; }"
#endif
"li { margin-left: 35px }"
"a { color: #3B73AF; }"
"table { table-layout: fixed }"
".historyLink { width: 100%%; text-align: center; background-color: #F0F0F0; padding: 5px 0; display: none; }"
".chatBlock { border-top: 1px solid #E1E1E1; border-collapse: collapse; }"
".chatBlock p { margin: 5px 10px 0 10px; }"
".chatBlock p:first-child, .chatBlock p:nth-child(2) { margin-top: 0; }"
".chatBlock td { padding: 3px 0; }"
".fileBlock .icon { margin: 2px 7px 4px 0; }"
".fileBlock .fileSize { color: #666; }"
".linkBlock .icon { height: 16px; width: 16px; margin: 2px 5px 4px 10px; vertical-align: middle; float:left; }"
".linkBlock .linkPreview { margin-top: 5px; }"
".linkBlock .linkPreview:first-child { margin-top: 0; }"
".linkBlock .linkPreview p { margin: 0; margin-left: 30px; }"
".me { background-color: #F3F7FB; border-top: 1px solid #BED6EB; }"
".them { background-color: #fff; }"
".messageBlock img { vertical-align: middle; }"
".messageBlock { border-left: 1px solid #e1e1e1; }"
".me .messageBlock { vertical-align: middle; margin-bottom: 4px; border-left: 1px solid #BED6EB; }"
".messageBlock p.monospace { font-family: monospace }"
".messageBlock p.msgText { clear: left; }"
".messageBlock p.msgText:nth-child(2) { margin-top: 1px; }"
".messageBlock p.msgTextError { clear: left; color: #ABABAB; padding-left: 21px; background: url('icon_yield.png') no-repeat left center; }"
".messageBlock p.msgTextError a { color: #9999EE; }"
".messageBlock pre { font-family: monospace; margin: 5px 10px 0 10px; white-space: pre-wrap; tab-interval: 10px; }"
".messageBlock pre:first-child, .messageBlock pre:nth-child(2) { margin-top: 0; }"
".messageBlock pre * { font-family: monospace; }"
".messageBlock p.monospace * { font-family: monospace }"
#if TARGET_OS_IPHONE
".nameBlock { width: 100px; background-color: #f7f7f7; border-top: 1px solid #ffffff; border-right: 1px solid #ffffff; vertical-align: top; font-weight: bold; text-align: right; }"
".nameBlock p { color: #636363; width: 85px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; }"
#else
".nameBlock { width: 130px; background-color: #f7f7f7; border-top: 1px solid #ffffff; border-right: 1px solid #ffffff; vertical-align: top; font-weight: bold; text-align: right; }"
".nameBlock p { color: #636363; width: 110px; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; }"
#endif
".nameBlock a { color: #636363; text-decoration: none; }"
".me .nameBlock p { color: #40556A; }"
".me .nameBlock { background-color: #E4ECF6; border-top: 1px solid #F3F7FB; border-right: 1px solid #F3F7FB; }"
// Add padding to make up for the missing borders on name/message blocks
".infoBlock p { color: #777; padding-left: 2px; }"
".infoBlock .messageBlock { border-left: none; }"
".infoBlock .nameBlock { background-color: inherit; border: none; }"
// Time blocks are different on the iPhone client - they are floated 'p' blocks instead of seprate table cells
// Use p.timeBlock so we override the default info block p style
"p.timeBlock { white-space: nowrap; color: #999; text-align: right; vertical-align: top; float: right; margin-top: 0; }"
".systemMessage, .systemMessage.yellow { background-color: #FFFBE8; border-top: 1px solid #ECDCBD; }"
".systemMessage .nameBlock, .systemMessage.yellow .nameBlock { color: #75510F; background-color: #FDECC6; border-top: 1px solid #FFFAEA; border-right: 1px solid #FFFAEA; }"
".systemMessage .messageBlock, .systemMessage.yellow .messageBlock { margin-bottom: 4px; border-left: 1px solid #E6D0AA; }"
".systemMessage.red { background-color: #F9E6E6; border-top: 1px solid #E2AFAF; }"
".systemMessage.red .messageBlock { border-left: 1px solid #E2AFAF; }"
".systemMessage.red .nameBlock { background-color: #F4D0D0; border-top: 1px solid #F8DEDE; border-right: 1px solid #F8DEDE; }"
".systemMessage.gray { background-color: #F4F4F4; border-top: 1px solid #D4D4D4; }"
".systemMessage.gray .messageBlock { border-left: 1px solid #C8C8C8; }"
".systemMessage.gray .nameBlock { background-color: #E1E1E1; border-top: 1px solid #F4F4F4; border-right: 1px solid #EFEFEF; }"
".systemMessage.purple { background-color: #F2F4FD; border-top: 1px solid #D2C4F7; }"
".systemMessage.purple .messageBlock { border-left: 1px solid #D2C4F7; }"
".systemMessage.purple .nameBlock { background-color: #E4DCFA; border-top: 1px solid #EEE8FC; border-right: 1px solid #EEE8FC; }"
".systemMessage.green { background-color: #F5F8E7; border-top: 1px solid #BDE18C; }"
".systemMessage.green .messageBlock { border-left: 1px solid #BDE18C; }"
".systemMessage.green .nameBlock { background-color: #DFF3CD; border-top: 1px solid #FFFAEA; border-right: 1px solid #FFFAEA; }"
".welcomeMessage { background-color: #F5F8E7; border-top: 1px solid #BDE18C; }"
".welcomeMessage .nameBlock { color: #75510F; background-color: #DFF3CD; border-top: 1px solid #FFFAEA; border-right: 1px solid #FFFAEA; }"
".welcomeMessage .messageBlock { margin-bottom: 4px; border-left: 1px solid #BDE18C; }"
#if TARGET_OS_IPHONE
"#chat_state { padding: 3px 5px 3px 112px; display: none; }"
#else
"#chat_state { padding: 3px 5px 4px 130px; display: none; }"
#endif
".tooltip { position: absolute; display: none; color: #fff; }"
".atTag { display: inline-block; padding: 0 3px; border: 1px solid #B4B4B4; background-color: #EFEFEF; -webkit-border-radius: 4px; }"
".atTagMe { color: #fff; border: 1px solid #4783BF; background: #4282C1 url(http://www.hipchat.com/img/at_bg.png) repeat-x; }"
".hexPreview { -webkit-border-radius: 3px; text-decoration: none; border: 1px solid #999; padding: 0 5px; width: 15px; }"
".image { margin: 5px 10px; }"
#if TARGET_OS_IPHONE
".image img { max-width: 175px; max-height: 175px; }"
#else
".image img { max-width: 300px; max-height: 250px; }"
#endif
"p.hiddenImage { font-style: italic; margin-left: 0; }"
"p.hiddenImage a { text-decoration: none; color: #999; }"
"p.hiddenImage a:hover { text-decoration: underline; }"
".dateDivider { background: #F4F4F4 url(http://www.hipchat.com/img/sidebar_header_bg.png) repeat-x scroll 0 100%%; color: #333; padding: 0;"
"				font-weight: bold; text-align: center; border-top: 1px solid #CECECE; }"
".chatDivider { background-color: #AFB8D2; color: #fff; height: 2px; border-top: 1px solid #5F7DA7; }"
".preview { padding: 3px 0 0 0; background: url(http://www.hipchat.com/img/chat/preview_bg.png) repeat-x; }"
".preview td { padding: 0; vertical-align: top; }"
".preview .hide, .fileBlock .hide { padding: 0 4px 1px 4px; float: right; margin: -2px 10px 0 0; cursor: pointer; }"
".linkImage { float: left; margin: 5px 10px; }"
".linkTitle { font-weight: bold; }"
".linkDesc { color: #888; }"
".scrollAnchor { display: block; position: relative; top: -42px; }"
".tooltip { position: absolute; z-index: 10; padding: 3px; background-color: #333; border: 1px solid #000; opacity: 0.9; display: none; border-radius: 6px }"
".tooltip a { color: #fff; text-decoration: none; display: inline-block; padding: 0 5px; }"
".unconfirmedIcon { float: left; padding-right: 5px; }"
".truncated { max-height:%3$@; text-overflow:clip; overflow:hidden; padding-left: 10px; }"
".untruncated { padding-left: 10px; }"
".showHideLinkContainer { margin-top: -15px; padding-top:18px; padding-left: 10px; padding-right: 10px; background: url(http://www.hipchat.com/img/chat/truncated_bg.png) repeat-x; font-family: monospace; }"
".messageBlock pre .showHideLinkContainer { margin-left: -10px; margin-right:-10px; }"
"</style>"
"</head>"
"<body onClick='setTimeout(handleBodyClick, 0);'>"
"<script type='text/javascript'>"
"window.onerror = function(error, url, line) {"
"    osxConnector.logJavascriptError_('Line '+line+': '+error);"
"};"
"var imagePreviewOpened = 'http://www.hipchat.com/img/chat/arrow_drawer_down.png';"
"var imagePreviewClosed = 'http://www.hipchat.com/img/chat/arrow_drawer.png';"
"function addAnchor(anchorName) {"
"	var newAnchor = document.createElement('a');"
"	newAnchor.name = anchorName;"
"	newAnchor.id = anchorName;"
"	newAnchor.className = 'scrollAnchor';"
"	var chatText = document.getElementById('chat_text');"
"	chatText.insertBefore(newAnchor, chatText.firstChild);"
"}"
"function addChatBlock(body, insertOnTop) {"
"	var newDiv = document.createElement('div');"
"	var chatText = document.getElementById('chat_text');"
"	newDiv.innerHTML = body;"
"   addLinkHandlers(newDiv);"
"	if (insertOnTop && chatText.firstChild) {"
#if !TARGET_OS_IPHONE
"		newDiv.className = 'previousHistory';"
"		newDiv.style.display = 'none';"
#endif
"		chatText.insertBefore(newDiv, chatText.firstChild);"
"	} else {"
"		chatText.appendChild(newDiv);"
"	}"
"   return newDiv;"
"}"
"function addChatDivider() {"
"   removeChatDivider();"
"	var newDiv = document.createElement('div');"
"	newDiv.className = 'chatDivider';"
"	document.getElementById('chat_text').appendChild(newDiv);"
"}"
"function addChatLine(blockId, className, body, messageId) {"
"	var currentBlock = document.getElementById(blockId);"
"   if (!currentBlock || typeof currentBlock == 'undefined') {"
"       return;"
"   }"
"	var newLine = document.createElement('p');"
"	newLine.id = messageId;"
"	newLine.className = className;"
"	newLine.innerHTML = body;"
"   addLinkHandlers(newLine);"
"	currentBlock.appendChild(newLine);"
"}"
"function addChatStateMessage(name) {"
"	var stateMessage = document.getElementById('chat_state');"
"	stateMessage.innerHTML = '<p>'+name+' is typing...</p>';"
"	stateMessage.style.display = 'block';"
"}"
"function addDateDivider(dateString, insertOnTop) {"
"	var newDiv = document.createElement('div');"
"	newDiv.className = 'dateDivider';"
"	newDiv.innerHTML = dateString;"
"	chatText = document.getElementById('chat_text');"
"	if (insertOnTop) {"
"		newDiv.className += ' previousHistory';"
"		newDiv.style.display = 'none';"
"		chatText.insertBefore(newDiv, chatText.firstChild);"
"	} else {"
"		chatText.appendChild(newDiv);"
"	}"
"}"
"function addLinkHandlers(node) {"
"	var links = node.getElementsByTagName('A');"
"	for (var i = 0; i < links.length; i++) {"
"		var link = links[i];"
"		if (link.name == 'toggle_paste') {"
"			link.addEventListener('click', handleTogglePasteClick, false);"
"		}"
"	}"
"}"
"function addPreviewData(messageId, html, className) {"
"	var previewNode = document.createElement('div');"
"	var messageNode = document.getElementById(messageId);"
"	if (!messageNode || typeof messageNode == 'undefined') {"
"		return;"
"	}"
"	previewNode.className = 'preview';"
"	previewNode.innerHTML = html;"
"	var links = previewNode.getElementsByTagName('A');"
"	for (var i = 0; i < links.length; i++) {"
"		var link = links[i];"
"		if (link.name == 'toggle_paste') {"
"			link.addEventListener('click', handleTogglePasteClick, false);"
"		}"
"	}"
"	if (className) {"
"		var node = messageNode.parentNode;"
"		while (node != null) {"
"			if (node.className.indexOf('chatBlock') >= 0) {"
"				node.className += ' '+className;"
"				break;"
"			}"
"			node = node.parentNode;"
"		}"
"	}"
"	messageNode.parentNode.insertBefore(previewNode, messageNode.nextSibling);"
"}"
"function clearChat() {"
"	var chatText = document.getElementById('chat_text');"
"	chatText.innerHTML = '';"
"}"
"function doScroll() {"
"	window.scrollTo(0, document.body.scrollHeight);	"
"}"
"function getMessageForBlock(blockId) {"
"	var blockNode = document.getElementById(blockId);"
"	if (!currentBlock) {"
"		return '';"
"	} else {"
"		return currentBlock.lastChild.innerHTML;"
"	}"
"}"
"function getSelectedHTML() {"
"   var sel = window.getSelection();"
"   if (sel.rangeCount) {"
"       var container = document.createElement('div');"
"       for (var i = 0, len = sel.rangeCount; i < len; ++i) {"
"           container.appendChild(sel.getRangeAt(i).cloneContents());"
"       }"
"       return container.innerHTML;"
"   } else {"
"       return '';"
"   }"
"}"
"function handleBodyClick() {"
"   var sel = getSelectedHTML();"
"   window.osxConnector.chatBodyClicked_(sel);"
"}"
"function handleTogglePasteClick() {"
"	var element = this.parentNode.parentNode.getElementsByTagName('div')[0];"
"   if (element.className.indexOf('untruncated') < 0) {"
"       element.className = element.className.replace('truncated', 'untruncated');"
"       this.innerHTML = 'Hide full text';"
"   } else {"
"       element.className = element.className.replace('untruncated', 'truncated');"
"       this.innerHTML = 'Show full text';"
"   }"
"}"
"function hideChatStateMessage() {"
"	var stateMessage = document.getElementById('chat_state');"
"	stateMessage.style.display = 'none';"
"}"
"function lastChatNodeContainsMessage(msg) {"
"	var chatText = document.getElementById('chat_text');"
"	if (chatText.childNodes.length > 0) {"
"		var lastMessage = chatText.childNodes[chatText.childNodes.length-1].childNodes[0].innerHTML;"
"		return lastMessage.indexOf(msg) != -1;"
"	}"
"	return false;"
"}"
"function markMessageConfirmed(messageId) {"
"	var messageNode = document.getElementById(messageId);"
"	if (!messageNode || typeof messageNode == 'undefined') {"
"		return;"
"	}"
"	messageNode.className = 'msgText';"
"}"
"function markMessageUnconfirmed(messageId) {"
"	var messageNode = document.getElementById(messageId);"
"	if (!messageNode || typeof messageNode == 'undefined') {"
"		return;"
"	}"
#if TARGET_OS_IPHONE
"	messageNode.addEventListener('click', function() {"
"       showUnconfirmedTooltip(this, this.id);"
"   });"
#else
"	messageNode.addEventListener('mouseover', function() {"
"       showUnconfirmedTooltip(this, this.id);"
"   });"
#endif
"	messageNode.className = 'msgTextError';"
"}"
"function removeChatDivider() {"
"	var dividerNodes = document.getElementsByClassName('chatDivider');"
"	for (var i = 0; i < dividerNodes.length; i++) {"
"		dividerNodes[i].parentNode.removeChild(dividerNodes[i]);"
"	}"
"}"
"function removeLastPresence(bareJid) {"
"	var presName = 'presence_'+bareJid;"
"	var chatText = document.getElementById('chat_text');"
"	for (var i = chatText.childNodes.length-1; i > 0; i--) {"
"		var node = chatText.childNodes[i].childNodes[0];"
"       if (node.nodeType != Node.ELEMENT_NODE) {"
"           return false;"
"       }"
"		var name = node.getAttribute('name');"
"		if (!(/presence_/.test(name))) {"
"			return false;"
"		}"
"		"
"		if (name == presName) {"
"			chatText.removeChild(chatText.childNodes[i]);"
"			return true;"
"		}"
"	}"
"}"
"function replaceMessageById(messageId, html) {"
"	var messageNode = document.getElementById(messageId);"
"	if (!messageNode || typeof messageNode == 'undefined') {"
"		return;"
"	} else {"
"		messageNode.innerHTML = html;"
"	}"
"}"
"function scrollByPixels(numPixels) {"
"	setTimeout(function() { window.scrollTo(0, window.pageYOffset + numPixels); }, 200);"
"}"
"function scrollAtBottom() {"
"	return window.pageYOffset >= (document.body.scrollHeight - window.innerHeight - 200);"
"}"
"function scrollToBottom() {"
"	setTimeout(function() { doScroll(); }, 200);"
"}"
"function showHistory() {"
"	var historyNodes = document.getElementsByClassName('previousHistory');"
"	for (var i = 0; i < historyNodes.length; i++) {"
"		historyNodes[i].style.display = 'block';"
"	}"
"}"
"function showMentionTooltip(node) {"
//"   showTooltip(node, window.osxConnector.getNameForMention_(node.innerHTML));"
"   node.title = window.osxConnector.getNameForMention_(node.innerHTML);"
"}"
"function showTooltip(obj, html) {"
"	var tt = document.getElementById('tooltip');"
"	var curleft = curtop = 0;"
"	do {"
"		curleft += obj.offsetLeft;"
"		curtop += obj.offsetTop;"
"	} while (obj = obj.offsetParent);"
"	tt.style.top = curtop-25;"
"	tt.style.left = curleft;"
"	tt.style.display = 'block';"
"	setTimeout(function() { tt.style.display = 'none'; }, 4000);"
"   tt.innerHTML = html"
"}"
"function showUnconfirmedTooltip(obj, messageId) {"
"   if (obj.className != 'msgTextError') {"
"       return;"
"   }"
#if TARGET_OS_IPHONE
"	var html = \"<a href='action:resend:\"+messageId+\"'>This message may not have been sent successfully. Tap here to attempt resend.</a>\";"
#else
"	var html = \"<a href='action:resend:\"+messageId+\"'>This message may not have been sent successfully. Click here to attempt resend.</a>\";"
#endif
"   showTooltip(obj, html);"
"}"
"function toggleImage(link) {"
"   var blockNode = (link.className == 'hide' ? link.parentNode : link.parentNode.parentNode.parentNode);"
"   var image = blockNode.getElementsByClassName('thumbnail')[0];"
"   var hiddenText = blockNode.getElementsByClassName('hiddenImage')[0];"
"   var toggleImg = blockNode.getElementsByClassName('hide')[0].getElementsByTagName('img')[0];"
"   image.style.display = (image.style.display == 'none' ? '' : 'none');"
"   hiddenText.style.display = (image.style.display == 'none' ? '' : 'none');"
"   toggleImg.src = (image.style.display == 'none' ?"
"                    imagePreviewClosed :"
"                    imagePreviewOpened);"
"}"
"</script>"
"	<div id='tooltip' onclick='this.style.display = \"none\"' class='tooltip'>"
"		<a href='action:resend'>This message may not have been sent successfully. Tap to attempt to resend.</a>"
"	</div>"
"	<div id='load_history_button' style='padding: 9px 0 8px 0; background-color: #dedede; text-align: center; display: none;'>"
"		<form action='action:load_history' method='POST'><input type='submit' value='Load more history' /></form>"
"	</div>"
"	<div id='loading_spinner' style='padding: 5px 0; background-color: #dedede; text-align: center; display: none;'>"
"		<img src='http://www.hipchat.com/img/chat/spinner.gif' />"
"	</div>"
"	<div id='view_history' class='historyLink'></div>"
"	<div id='unhide_history' class='historyLink'>"
"	<span style='color: #3B73AF; font-size: 14px;'>&#x25be;</span>&nbsp;&nbsp;"
"	<a id='unhide_history_link' href='actionscript:;'>Show chat history</a>"
"	</div>"
"	<div id='chat_text'></div>"
"	<div id='chat_state' class='chatBlock infoBlock'></div>"
"</body>"
"</html>";

+ (NSString *)initialHTML {
    if(!_initialHTML) {
        // Load prettify resources
        NSError *error = nil;
        NSString *prettifyCssPath = [[NSBundle mainBundle] pathForResource: @"prettify" ofType: @"css"];
        NSString *prettifyJsPath = [[NSBundle mainBundle] pathForResource: @"prettify" ofType: @"js"];
        NSString *prettifyJs = [NSString stringWithContentsOfFile:prettifyJsPath encoding:NSUTF8StringEncoding error:&error];
        NSString *prettifyCss = [NSString stringWithContentsOfFile:prettifyCssPath encoding:NSUTF8StringEncoding error:&error];
#if TARGET_OS_IPHONE
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            _initialHTML = [NSString stringWithFormat:_initialHTMLFormat, prettifyJs, prettifyCss, @"8.7em"];
        } else {
            _initialHTML = [NSString stringWithFormat:_initialHTMLFormat, prettifyJs, prettifyCss, @"4.5em"];
        }
#else
        _initialHTML = [NSString stringWithFormat:_initialHTMLFormat, prettifyJs, prettifyCss, @"8.7em"];
        #endif
    }
    return _initialHTML;
}

@end

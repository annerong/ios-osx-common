//
//  Linkify.m
//  hipchat
//
//  Created by Christopher Rivers on 9/30/10.
//  Copyright 2010 HipChat. All rights reserved.
//

#import "Linkify.h"
#import "Emoticon.h"
#import "RegexKitLite.h"
#import "DDLog.h"

static NSMutableDictionary *addedEmoticons = nil;
static NSMutableArray *defaultEmoticons = nil;
static NSMutableArray *parenEmoticons = nil;

static NSString *RE_EMAIL_PATTERN = @"(?<=\\s|\\A)[\\w.-]+\\+*[\\w.-]+@(?:(?:[\\w-]+\\.)+[A-Za-z]{2,6}|(?:\\d{1,3}\\.){3}\\d{1,3})";
static NSString *RE_TLD = @"(?:aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|fi|fj|fk|fm|fo|fr|ga|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)";
// characters allowed in URL center
static NSString *RE_URL_MIDCHAR = @"(?:"
                                "[^\\s()<>\"]+"    // non-space, non-()
                                "|"            // or
                                "\\(([^\"<>\\s]+)\\)" // non-space in parens
                                ")";
static NSString *RE_URL_ENDCHAR = @"(?:"
                                "\\((\\S*)\\)"                       // 0 or more non-space in parens
                                "|"                                  // or
                                "[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]" // none of these
                                ")";
static NSString *RE_URL_SCHEME = @"(?:\\w[\\w-]{1,}):/{1,3}"; 	// protocol

static NSString *RE_URL_ENDING = nil;
static NSString *RE_FULL_URL = nil;
static NSString *RE_OTHER_URL = nil;

@implementation Linkify

static NSString *emoticonBasePath = @"http://www.hipchat.com/img/emoticons";

// initialize is a message that gets sent to each class once, before it's used
// We don't explicitly call this method, but it should finish before we ever actually use the class
+ (void)initialize {
	addedEmoticons = [[NSMutableDictionary alloc] init];

	defaultEmoticons = [NSMutableArray arrayWithObjects:
                        [NSDictionary dictionaryWithObjectsAndKeys:@":-?D", @"regex",
                         @":D", @"shortcut",
                         @":D", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"bigsmile.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@":-?o", @"regex",
                         @":o", @"shortcut",
                         @":o", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"gasp.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@":-?Z", @"regex",
                         @":Z", @"shortcut",
                         @":Z", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"sealed.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@":-?p", @"regex",
                         @":p", @"shortcut",
                         @":p", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"tongue.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@";-?p", @"regex",
                         @";p", @"shortcut",
                         @";p", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"winktongue.gif", @"image", nil],

                        // Frown is a subset of angry, so do angry first, then frown
                        [NSDictionary dictionaryWithObjectsAndKeys:@"&gt;:-\\(", @"regex",
                         @">:-(", @"shortcut",
                         @"&gt;:-(", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"angry.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@":-?\\(", @"regex",
                         @":(", @"shortcut",
                         @":(", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"frown.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@"8-?\\)", @"regex",
                         @"8)", @"shortcut",
                         @"8)", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"cool.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@":'\\(", @"regex",
                         @":'(", @"shortcut",
                         @":&apos;(", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"cry.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@":#", @"regex",
                         @":#", @"shortcut",
                         @":#", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"footinmouth.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@"\\(embarrassed\\)", @"regex",
                         @"(embarrassed)", @"shortcut",
                         @"(embarrassed)", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"embarrassed.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@":'\\)", @"regex",
                         @":')", @"shortcut",
                         @":&apos;)", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"happytear.gif", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@"O:\\)", @"regex",
                         @"O:)", @"shortcut",
                         @"O:)", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:22], @"height",
                         @"innocent.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@":-\\*", @"regex",
                         @":-*", @"shortcut",
                         @":-*", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"kiss.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@":\\$", @"regex",
                         @":$", @"shortcut",
                         @":$", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"moneymouth.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@"\\(oops\\)", @"regex",
                         @"(oops)", @"shortcut",
                         @"(oops)", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"oops.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@"(:\\\\|:/)", @"regex",
                         @":\\", @"shortcut",
                         @":\\\\", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"slant.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@"=\\)|:-?\\)", @"regex",
                         @":)", @"shortcut",
                         @":)", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"smile.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@":-?\\|", @"regex",
                         @":-|", @"shortcut",
                         @":-|", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"straightface.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@"\\(thumbsdown\\)", @"regex",
                         @"(thumbsdown)", @"shortcut",
                         @"(thumbsdown)", @"htmlShortcut",
                         [NSNumber numberWithInt:16], @"width",
                         [NSNumber numberWithInt:16], @"height",
                         @"thumbs_down.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@"\\(thumbsup\\)", @"regex",
                         @"(thumbsup)", @"shortcut",
                         @"(thumbsup)", @"htmlShortcut",
                         [NSNumber numberWithInt:16], @"width",
                         [NSNumber numberWithInt:16], @"height",
                         @"thumbs_up.png", @"image", nil],
                        [NSDictionary dictionaryWithObjectsAndKeys:@";-?\\)", @"regex",
                         @";)", @"shortcut",
                         @";)", @"htmlShortcut",
                         [NSNumber numberWithInt:18], @"width",
                         [NSNumber numberWithInt:18], @"height",
                         @"wink.png", @"image", nil],
                        nil
				];
    parenEmoticons = [NSMutableArray array];
	
    RE_URL_ENDING = [NSString stringWithFormat:@"(?:%@*%@)?", RE_URL_MIDCHAR, RE_URL_ENDCHAR];
	RE_FULL_URL = [NSString stringWithFormat:@"%@%@", RE_URL_SCHEME, RE_URL_ENDING];
	RE_OTHER_URL = [NSString stringWithFormat:@"\\w[\\w_-]*(?:\\.\\w[\\w_-]*)*\\.%@(?:\\/%@|\\b)", RE_TLD, RE_URL_ENDING];
}

+ (void)addEmoticon:(Emoticon *)emoticon {
	// Don't add emoticons more than once
	if ([addedEmoticons objectForKey:emoticon.shortcut]) {
		return;
	}
	[addedEmoticons setObject:[NSNumber numberWithBool:YES] forKey:emoticon.shortcut];
	[parenEmoticons addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                              [NSString stringWithFormat:@"\\((?i:%@)\\)", emoticon.shortcut], @"regex",
                              [NSString stringWithFormat:@"(%@)", emoticon.shortcut], @"shortcut",
                              emoticon.width, @"width",
                              emoticon.height, @"height",
                              emoticon.path, @"image",
                              nil]];
}

+ (void)addEmoticonWithFilename:(NSString *)filename shortcut:(NSString *)shortcut height:(NSInteger)height width:(NSInteger)width {
	// Don't add emoticons more than once
	if ([addedEmoticons objectForKey:shortcut]) {
		return;
	}
	[addedEmoticons setObject:[NSNumber numberWithBool:YES] forKey:shortcut];
	[parenEmoticons addObject:[NSDictionary dictionaryWithObjectsAndKeys:
						  [NSString stringWithFormat:@"\\((?i:%@)\\)", shortcut], @"regex",
						  [NSString stringWithFormat:@"(%@)", shortcut], @"shortcut",
						  [NSNumber numberWithLong:width], @"width",
						  [NSNumber numberWithLong:height], @"height",
						  filename, @"image", 
						  nil]];
}

+ (NSArray *)getDefaultEmoticons {
    return defaultEmoticons;
}

+ (NSString *)linkify:(NSString *)text withEmotions:(BOOL)emoticonify andTruncate:(NSInteger)truncateLength matches:(NSMutableArray *)matches {
	text = [Linkify matchAndReplaceWithPattern:RE_EMAIL_PATTERN input:text isURL:NO addHTTP:NO truncateLength:truncateLength matches:matches];
	text = [Linkify matchAndReplaceWithPattern:RE_FULL_URL input:text isURL:YES addHTTP:NO truncateLength:truncateLength matches:matches];
	text = [Linkify matchAndReplaceWithPattern:RE_OTHER_URL input:text isURL:YES addHTTP:YES truncateLength:truncateLength matches:matches];

	if (emoticonify) {
		text = [Linkify emoticonText:text];
	}

	return text;
}

/**
 * Internal helper function for linkification
 */
+ (NSString *)matchAndReplaceWithPattern:(NSString *)pattern
							 input:(NSString *)input
							 isURL:(BOOL)isURL
						   addHTTP:(BOOL)addHTTP
					truncateLength:(NSInteger)truncateLength
						   matches:(NSMutableArray *)matchedLinks {

	NSInteger offset = 0;
	NSInteger endTagPos = 0;

	// Max number of links to linkify in a single chunk of text
	NSInteger maxIter = 20;
	NSInteger curIter = 0;
	NSRange matchRange;
	NSRange currentRange = NSMakeRange(offset, [input length]);
	do {
		matchRange = [input rangeOfRegex:pattern inRange:currentRange];
		if (matchRange.location == NSNotFound) {
			break;
		}

		curIter++;
		if (curIter > maxIter) {
			break;
		}

		// If we find an opening anchor tag, advance to its end and continue looking
		NSString *substr = [input substringWithRange:NSMakeRange(offset, (matchRange.location-offset))];
		if ([substr isMatchedByRegex:@"<[aA]"]) {
			NSRange closeAnchorRange = [[input substringFromIndex:offset] rangeOfRegex:@"</[aA]>"];

			if (closeAnchorRange.location == NSNotFound) {
				return input;
			}

			endTagPos = offset + closeAnchorRange.location;
			offset = endTagPos + 4;
			currentRange.location = offset;
			currentRange.length = (input.length - offset);

			continue;
		}

		// Do the replacement of url text with anchor tag
		NSString *address = [input substringWithRange:matchRange];

        // Since we escape before linkifying, we need to make sure that the matched link
        // doesn't actually end with an escaped character (&, <, or >)
        // If it does, move the match backwards so as not to include the escaped character
        // Then, keep moving the match back until it ends with a valid character (defined by RE_URL_ENDCHAR)
        NSUInteger nextCharacterIndex = (matchRange.location+matchRange.length);
        NSRange escapedCharMatchRange = [address rangeOfRegex:@"&(gt|lt|amp)$"];
        if (escapedCharMatchRange.location != NSNotFound && input.length > nextCharacterIndex && [input characterAtIndex:nextCharacterIndex] == ';') {
            NSString *validEndingRegex = [NSString stringWithFormat:@"%@$", RE_URL_ENDCHAR];
            matchRange = NSMakeRange(matchRange.location, matchRange.length-escapedCharMatchRange.length);
            address = [input substringWithRange:matchRange];
            while (![address isMatchedByRegex:validEndingRegex] && matchRange.length > 0) {
                matchRange = NSMakeRange(matchRange.location, matchRange.length-1);
                address = [input substringWithRange:matchRange];
            }
        }


		NSString *replacement;
		NSString *escapedAddr = [address stringByReplacingOccurrencesOfRegex:@"\"" withString:@"%%22"];
		if (addHTTP) {
			escapedAddr = [NSString stringWithFormat:@"http://%@", escapedAddr];
		}
		NSString *displayAddr = ((truncateLength && address.length > truncateLength) ?
								 [NSString stringWithFormat:@"%@...", [address substringToIndex:truncateLength]] :
								 address);
        // Add manual <wbr> tags here so that long links don't get pushed down a line in chat due to the
        // timestamp text (which is floated right). The built-in CSS based word-break tries to avoid breaking
        // the link on characters like "/" and "." so add wbr around them
        displayAddr = [displayAddr stringByReplacingOccurrencesOfRegex:@"([/\\.])" withString:@"<wbr>$1"];

		if (!isURL) {
			replacement = [NSString stringWithFormat:@"<a href=\"mailto:%@\">%@</a>", escapedAddr, displayAddr];
		} else {
			replacement = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>", escapedAddr, displayAddr];
		}

		if (matchedLinks) {
			[matchedLinks addObject:address];
		}

		NSInteger matchEndPos = matchRange.location+matchRange.length;
		input = [NSString stringWithFormat:@"%@%@%@", 
				 [input substringWithRange:NSMakeRange(0, matchRange.location)],
				 replacement,
				 [input substringWithRange:NSMakeRange(matchEndPos, (input.length - matchEndPos))]];

		offset = matchRange.location + replacement.length;
		currentRange.location = offset;
		currentRange.length = (input.length - currentRange.location);
	} while (matchRange.location != NSNotFound);

	return input;
}

///**
// * Replace text emoticons with images
// **/
+ (NSString *)emoticonText:(NSString *)text {
	for (NSDictionary *emoticonData in defaultEmoticons) {
		NSString *regex = [emoticonData objectForKey:@"regex"];
		NSString *img = [emoticonData objectForKey:@"image"];

		NSString *replacement = [NSString stringWithFormat:@"<img name='emoticon' title='%@' alt='%@' height='%@' width='%@' src='%@/%@' />",
								 [emoticonData objectForKey:@"htmlShortcut"],
								 [emoticonData objectForKey:@"htmlShortcut"],
								 [emoticonData objectForKey:@"height"],
								 [emoticonData objectForKey:@"width"],
                                 emoticonBasePath,
								 img];
        // Avoid emoticonifying when the emoticon is just part of a longer string
        // Check for quote character too so emoticonifying something like O:) doesn't later also try
        // to emoticon the :) in the title/alt attributes
		NSString *pattern = [NSString stringWithFormat:@"(?<!\\w|')%@(?!\\w|')", regex];
		text = [text stringByReplacingOccurrencesOfRegex:pattern
                                              withString:replacement
                                                 options:RKLCaseless
                                                   range:NSMakeRange(0, text.length)
                                                   error:nil];
	}

    // If there's no parentheses, we're done (only parenthetical emoticons left)
    if ([text rangeOfString:@"("].location == NSNotFound) {
        return text;
    }

	for (NSDictionary *emoticonData in [NSArray arrayWithArray:parenEmoticons]) {
		NSString *regex = [emoticonData objectForKey:@"regex"];
		NSString *img = [emoticonData objectForKey:@"image"];

		NSString *replacement = [NSString stringWithFormat:@"<img name='emoticon' title='%@' alt='%@' height='%@' width='%@' src='%@/%@' />",
								 [emoticonData objectForKey:@"shortcut"],
								 [emoticonData objectForKey:@"shortcut"],
								 [emoticonData objectForKey:@"height"],
								 [emoticonData objectForKey:@"width"],
                                 emoticonBasePath,
								 img];
		text = [text stringByReplacingOccurrencesOfRegex:regex withString:replacement];
	}

	return text;
}

+ (void)setEmoticonBasePath:(NSString *)path {
    if (emoticonBasePath) {
        emoticonBasePath = nil;
    }
    emoticonBasePath = [NSString stringWithString:path];
}

+ (NSString *)emoticonsBasePath {
    return emoticonBasePath;
}

+ (NSArray *)parenEmoticons {
    return parenEmoticons;
}

@end

//
// Created by Doug Keen on 11/14/13.
//


#import "ThreadSafeSet.h"


@implementation ThreadSafeSet {

}

@synthesize collectionQueue = _queue;
@synthesize internalSet = _set;

- (instancetype)initWithQueueName:(const char *)queueName {
    self = [super init];
    if (self) {
        _queue = dispatch_queue_create(queueName, DISPATCH_QUEUE_CONCURRENT);
        _set = [NSMutableSet set];
    }

    return self;
}

- (instancetype)initWithQueueName:(const char *)queueName andSet:(NSSet *)set {
    self = [super init];
    if (self) {
        _queue = dispatch_queue_create(queueName, DISPATCH_QUEUE_CONCURRENT);
        _set = [NSMutableSet setWithSet:set];
    }

    return self;
}


- (NSSet *)immutableCopy {
    __block NSSet *copy;
    dispatch_sync(_queue, ^{
        copy = [NSSet setWithSet:_set];
    });
    return copy;
}

- (void)addObject:(id)object {
    dispatch_barrier_async(_queue, ^{
        [_set addObject:object];
    });
}

- (void)removeObject:(id)object {
    dispatch_barrier_async(_queue, ^{
        [_set removeObject:object];
    });
}

- (NSUInteger)count {
    __block NSUInteger count;
    dispatch_sync(_queue, ^{
        if (_set) {
            count = [_set count];
        } else {
            count = 0;
        }
    });
    return count;
}

- (id)member:(id)object {
    __block id member;
    dispatch_sync(_queue, ^{
        member = [_set member:object];
    });
    return member;
}

- (void)intersectSet:(NSSet *)otherSet {
    dispatch_barrier_async(_queue, ^{
        [_set intersectSet:otherSet];
    });
}

- (void)minusSet:(NSSet *)otherSet {
    dispatch_barrier_async(_queue, ^{
        [_set minusSet:otherSet];
    });
}

- (void)removeAllObjects {
    dispatch_barrier_async(_queue, ^{
        [_set removeAllObjects];
    });
}

- (void)unionSet:(NSSet *)otherSet {
    dispatch_barrier_async(_queue, ^{
        [_set unionSet:otherSet];
    });
}

- (void)filterUsingPredicate:(NSPredicate *)predicate {
    dispatch_barrier_async(_queue, ^{
        [_set filterUsingPredicate:predicate];
    });
}

- (NSArray *)allObjects {
    __block NSArray *array;
    dispatch_sync(_queue, ^{
        array = [_set allObjects];
    });
    return array;
}

- (BOOL)containsObject:(id)anObject {
    __block BOOL b;
    dispatch_sync(_queue, ^{
        b = [_set containsObject:anObject];
    });
    return b;
}

@end
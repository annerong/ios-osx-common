//
// Created by Doug Keen on 11/14/13.
//


#import <Foundation/Foundation.h>


@interface ThreadSafeDictionary : NSObject

@property(readonly) dispatch_queue_t collectionQueue;

@property(readonly) NSMutableDictionary *internalDictionary;

- (instancetype)initWithQueueName:(const char *)queueName;

- (instancetype)initWithQueueName:(const char *)queueName andDictionary:(NSDictionary *)dictionary;

- (NSDictionary *)immutableCopy;

- (void)removeObjectForKey:(id)aKey;

- (void)setObject:(id)anObject forKey:(id <NSCopying>)aKey;

- (void)removeAllObjects;

- (NSUInteger)count;

- (id)objectForKey:(id)aKey;

- (NSArray *)allKeys;

- (NSArray *)allValues;

- (NSSet *)keysOfEntriesPassingTest:(BOOL (^)(id key, id obj, BOOL *stop))predicate;

- (NSString *)JSONRepresentation;

- (void)enumerateKeysAndObjectsUsingBlock:(void (^)(id key, id obj, BOOL *stop))block;
@end
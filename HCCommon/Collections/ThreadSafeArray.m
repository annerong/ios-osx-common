//
// Created by Doug Keen on 11/14/13.
//


#import "ThreadSafeArray.h"


@implementation ThreadSafeArray {
}

@synthesize collectionQueue = _queue;
@synthesize internalArray = _array;

- (instancetype)initWithQueueName:(const char *)queueName {
    self = [super init];
    if (self) {
        _queue = dispatch_queue_create(queueName, DISPATCH_QUEUE_CONCURRENT);
        _array = [NSMutableArray array];
    }

    return self;
}

- (instancetype)initWithQueueName:(const char *)queueName andArray:(NSArray *)array {
    self = [super init];
    if (self) {
        _queue = dispatch_queue_create(queueName, DISPATCH_QUEUE_CONCURRENT);
        _array = [NSMutableArray arrayWithArray:array];
    }

    return self;
}


- (NSArray *)immutableCopy {
    __block NSArray *copy;
    dispatch_sync(_queue, ^{
        copy = [NSArray arrayWithArray:_array];
    });
    return copy;
}

- (void)addObject:(id)anObject {
    dispatch_barrier_async(_queue, ^{
        [_array addObject:anObject];
    });
}

- (void)insertObject:(id)anObject atIndex:(NSUInteger)index {
    dispatch_barrier_async(_queue, ^{
        [_array insertObject:anObject atIndex:index];
    });
}

- (void)removeObjectAtIndex:(NSUInteger)index {
    dispatch_barrier_async(_queue, ^{
        [_array removeObjectAtIndex:index];
    });
}

- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject {
    dispatch_barrier_async(_queue, ^{
        [_array replaceObjectAtIndex:index withObject:anObject];
    });
}

- (void)removeAllObjects {
    dispatch_barrier_async(_queue, ^{
        [_array removeAllObjects];
    });
}

- (void)removeObject:(id)anObject {
    dispatch_barrier_async(_queue, ^{
        [_array removeObject:anObject];
    });
}

- (NSUInteger)count {
    __block NSUInteger count;
    dispatch_sync(_queue, ^{
        if (_array) {
            count = [_array count];
        } else {
            count = 0;
        }
    });
    return count;
}

- (id)objectAtIndex:(NSUInteger)index {
    __block id obj;
    dispatch_sync(_queue, ^{
        obj = [_array objectAtIndex:index];
    });
    return obj;
}

- (NSUInteger)indexOfObject:(id)anObject {
    __block NSUInteger index;
    dispatch_sync(_queue, ^{
        index = [_array indexOfObject:anObject];
    });
    return index;
}

- (NSUInteger)indexOfObjectPassingTest:(BOOL (^)(id obj, NSUInteger idx, BOOL *stop))predicate {
    __block NSUInteger index;
    dispatch_sync(_queue, ^{
        index = [_array indexOfObjectPassingTest:predicate];
    });
    return index;
}

- (id)firstObject {
    __block id obj;
    dispatch_sync(_queue, ^{
        obj = [_array firstObject];
    });
    return obj;
}

- (id)lastObject {
    __block id obj;
    dispatch_sync(_queue, ^{
        obj = [_array lastObject];
    });
    return obj;
}

- (void)removeLastObject {
    dispatch_barrier_async(_queue, ^{
        [_array removeLastObject];
    });
}

- (void)push:(id)anObject {
    [self addObject:anObject];
}

- (id)pop {
    __block id obj;
    dispatch_barrier_sync(_queue, ^{
        obj = [_array lastObject];
        [_array removeLastObject];
    });
    return obj;
}

- (NSArray *)sortedArrayUsingComparator:(NSComparator)cmptr {
    __block id array;
    dispatch_barrier_sync(_queue, ^{
        array = [_array sortedArrayUsingComparator:cmptr];
    });
    return array;
}

- (NSArray *)sortedArrayWithOptions:(NSSortOptions)opts usingComparator:(NSComparator)cmptr {
    __block id array;
    dispatch_barrier_sync(_queue, ^{
        array = [_array sortedArrayWithOptions:opts usingComparator:cmptr];
    });
    return array;
}

- (NSArray *)sortedArrayUsingDescriptors:(NSArray *)sortDescriptors {
    __block id array;
    dispatch_barrier_sync(_queue, ^{
        array = [_array sortedArrayUsingDescriptors:sortDescriptors];
    });
    return array;
}

@end
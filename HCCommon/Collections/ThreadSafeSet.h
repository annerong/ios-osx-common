//
// Created by Doug Keen on 11/14/13.
//


#import <Foundation/Foundation.h>


@interface ThreadSafeSet : NSObject

@property (readonly) dispatch_queue_t collectionQueue;

@property (readonly) NSMutableSet *internalSet;

- (instancetype)initWithQueueName:(const char *)queueName;

- (instancetype)initWithQueueName:(const char *)queueName andSet:(NSSet *)set;

- (NSSet *)immutableCopy;

- (void)addObject:(id)object;

- (void)removeObject:(id)object;

- (NSUInteger)count;

- (id)member:(id)object;

- (void)intersectSet:(NSSet *)otherSet;

- (void)minusSet:(NSSet *)otherSet;

- (void)removeAllObjects;

- (void)unionSet:(NSSet *)otherSet;

- (void)filterUsingPredicate:(NSPredicate *)predicate;

- (NSArray *)allObjects;

- (BOOL)containsObject:(id)anObject;
@end
//
// Created by Doug Keen on 11/14/13.
//


#import "ThreadSafeDictionary.h"
#import "NSObject+SBJson.h"


@implementation ThreadSafeDictionary {

}

@synthesize collectionQueue = _queue;
@synthesize internalDictionary = _dict;

- (instancetype)initWithQueueName:(const char *)queueName {
    self = [super init];
    if (self) {
        _queue = dispatch_queue_create(queueName, DISPATCH_QUEUE_CONCURRENT);
        _dict = [NSMutableDictionary dictionary];
    }

    return self;
}

- (instancetype)initWithQueueName:(const char *)queueName andDictionary:(NSDictionary *)dictionary {
    self = [super init];
    if (self) {
        _queue = dispatch_queue_create(queueName, DISPATCH_QUEUE_CONCURRENT);
        _dict = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    }

    return self;
}

- (NSDictionary *)immutableCopy {
    __block NSDictionary *copy;
    dispatch_sync(_queue, ^{
        copy = [NSDictionary dictionaryWithDictionary:_dict];
    });
    return copy;
}

- (void)removeObjectForKey:(id)aKey {
    dispatch_barrier_async(_queue, ^{
        [_dict removeObjectForKey:aKey];
    });
}

- (void)setObject:(id)anObject forKey:(id <NSCopying>)aKey {
    dispatch_barrier_async(_queue, ^{
        [_dict setObject:anObject forKey:aKey];
    });
}

- (void)removeAllObjects {
    dispatch_barrier_async(_queue, ^{
        [_dict removeAllObjects];
    });
}

- (NSUInteger)count {
    __block NSUInteger count;
    dispatch_sync(_queue, ^{
        if (_dict) {
            count = [_dict count];
        } else {
            count = 0;
        }
    });
    return count;
}

- (id)objectForKey:(id)aKey {
    __block id obj;
    dispatch_sync(_queue, ^{
        obj = [_dict objectForKey:aKey];
    });
    return obj;
}

- (NSArray *)allKeys {
    __block NSArray *keys;
    dispatch_sync(_queue, ^{
        keys = [_dict allKeys];
    });
    return keys;
}

- (NSArray *)allValues {
    __block NSArray *vals;
    dispatch_sync(_queue, ^{
        vals = [_dict allValues];
    });
    return vals;
}

- (NSSet *)keysOfEntriesPassingTest:(BOOL (^)(id key, id obj, BOOL *stop))predicate {
    __block NSSet *keys;
    dispatch_sync(_queue, ^{
        keys = [_dict keysOfEntriesPassingTest:predicate];
    });
    return keys;
}

- (NSString *)JSONRepresentation {
    __block NSString *json;
    dispatch_sync(_queue, ^{
        json = [_dict JSONRepresentation];
    });
    return json;
}

@end
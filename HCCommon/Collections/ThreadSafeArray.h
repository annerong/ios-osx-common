//
// Created by Doug Keen on 11/14/13.
//


#import <Foundation/Foundation.h>


@interface ThreadSafeArray : NSObject

@property (readonly) dispatch_queue_t collectionQueue;

@property (readonly) NSMutableArray *internalArray;

- (instancetype)initWithQueueName:(const char *)queueName;

- (instancetype)initWithQueueName:(const char *)queueName andArray:(NSArray *)array;

- (NSArray *)immutableCopy;

- (void)addObject:(id)anObject;

- (void)insertObject:(id)anObject atIndex:(NSUInteger)index1;

- (void)removeObjectAtIndex:(NSUInteger)index1;

- (void)replaceObjectAtIndex:(NSUInteger)index1 withObject:(id)anObject;

- (void)removeAllObjects;

- (void)removeObject:(id)anObject;

- (NSUInteger)count;

- (id)objectAtIndex:(NSUInteger)index1;

- (NSUInteger)indexOfObject:(id)anObject;

- (NSUInteger)indexOfObjectPassingTest:(BOOL (^)(id obj, NSUInteger idx, BOOL *stop))predicate;

- (id)firstObject;

- (id)lastObject;

- (void)removeLastObject;

- (void)push:(id)anObject;

- (id)pop;

- (NSArray *)sortedArrayUsingComparator:(NSComparator)cmptr;

- (NSArray *)sortedArrayWithOptions:(NSSortOptions)opts usingComparator:(NSComparator)cmptr;

- (NSArray *)sortedArrayUsingDescriptors:(NSArray *)sortDescriptors;
@end
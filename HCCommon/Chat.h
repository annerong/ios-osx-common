//
//  Chat.h
//  HCCommon
//
//  Created by Christopher Rivers on 10/30/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Room.h"

@class NSXMLElement;
@class Room;
@class User;
@class XMPPJID;
@class XMPPMessage;
@class XMPPPresence;

@interface Chat : NSObject <RoomDelegate> {
	NSMutableDictionary *departureTimers;
    ThreadSafeArray *historyMessages;
    NSMutableDictionary *historyReplacements;
    NSMutableDictionary *lastMessages;
    NSMutableDictionary *markedUnconfirmed;
    ThreadSafeArray *preloadEvents;
    NSMutableDictionary *unconfirmedMessages;
    ThreadSafeArray *unsentMessages;

	NSTimeInterval chatDividerTime;
    NSInteger currentBlockId;
    NSTimer *fullyLoadedTimer;
	NSString *historyRequestId;
	NSTimeInterval lastActive;
	XMPPJID *lastMessageFrom;
    XMPPMessage *lastMessageReceived;
    NSTimeInterval oldestMessageTime;
    NSTimer *typingMessageTimer;

    BOOL firstLoading;
    BOOL initialized;
    BOOL requestedHistory;
    BOOL waitingToLoadHistory;
	BOOL welcomeIsShown;

    id delegate;
}

- (Chat *)initWithJid:(XMPPJID *)initJid delegate:(id)initDelegate;
- (void)close;
- (void)deinitialize;

- (void)addChatDivider;
- (void)executePreloadEvents;
- (void)loadHistory;
- (void)removeChatDivider;
- (BOOL)requestPreviousHistoryWithCount:(NSInteger)count;
- (void)resendUnconfirmedMessage:(NSString *)messageId;
- (void)sendMessage:(NSString *)text;
- (void)sendFileUploadWithData:(NSData *)data
                   contentType:(NSString *)contentType
                      fileName:(NSString *)fileName
                          desc:(NSString *)desc;
- (void)scrollChat;
- (void)scrollChatByPixels:(NSInteger)pixels;

@property (readwrite, strong) NSMutableArray *autocompleteList;
@property (readwrite, strong) NSMutableArray *autocompleteMatches;
@property (readwrite, assign) NSTimeInterval backgroundEnterTime;
@property (readwrite, strong) NSString *currentAnchor;
@property (readwrite, assign) BOOL historyLoaded;
@property (readwrite, assign) BOOL executingPreload;
@property (readwrite, assign) BOOL finishedExecutingPreload;
@property (readwrite, strong) XMPPMessage *initialMessage;
@property (readwrite, strong) XMPPJID *jid;
@property (readwrite, strong) Room *room;
@property (readwrite, strong) User *user;
@property (nonatomic, retain) id delegate;

@end

@protocol ChatDelegate

@required

- (void)chatFinishedLoadingHistory:(Chat *)chat;
- (void)chatFinishedExecutingPreload:(Chat *)chat;
- (void)chat:(Chat *)chat didAddJavascript:(NSString *)javascript;

@optional

- (void)chatDidAddText:(Chat *)chat;
- (void)chatDidUpdateAutocomplete:(Chat *)chat;
- (void)chatFailedToRequestHistory:(Chat *)chat;
- (void)chatFailedToUploadFile:(Chat *)chat;
- (void)chatFinishedUploadingFile:(Chat *)chat;
- (void)chatLoadedFullHistory:(Chat *)chat;
- (BOOL)chatShouldAddText:(Chat *)chat;
- (BOOL)chatShouldInsertTextAtTop:(Chat *)chat;
- (BOOL)chatShouldLoadHistory:(Chat *)chat;
- (void)chatWillLoadHistory:(Chat *)chat;
- (void)chatWillLoadHistoryJavascript:(Chat *)chat;
- (void)chatWillRefresh:(Chat *)chat;
- (void)chat:(Chat *)chat didReceiveMessage:(XMPPMessage *)message;
- (void)chat:(Chat *)chat didReceivePresence:(XMPPPresence *)presence;
- (void)chat:(Chat *)chat wasDestroyedWithNode:(NSXMLElement *)destroyNode;
- (NSInteger)numberOfHistoryStanzasToRequestForChat:(Chat *)chat;

@end


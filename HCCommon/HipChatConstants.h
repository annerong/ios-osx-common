//
//  HipChatConstants.h
//  HCCommon
//
//  Created by Christopher Rivers on 10/23/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const HipChatFileAddedNotification;
FOUNDATION_EXPORT NSString *const HipChatFlashNotification;
FOUNDATION_EXPORT NSString *const HipChatFocusChatNotification;
FOUNDATION_EXPORT NSString *const HipChatFocusNextChatNotification;
FOUNDATION_EXPORT NSString *const HipChatFocusPreviousChatNotification;
FOUNDATION_EXPORT NSString *const HipChatFocusLobbyNotification;
FOUNDATION_EXPORT NSString *const HipChatFocusSettingsNotification;
FOUNDATION_EXPORT NSString *const HipChatInviteNotification;
FOUNDATION_EXPORT NSString *const HipChatJoinChatNotification;
FOUNDATION_EXPORT NSString *const HipChatLeaveChatNotification;
FOUNDATION_EXPORT NSString *const HipChatLinkAddedNotification;
FOUNDATION_EXPORT NSString *const HipChatShowSearchResultsNotification;
FOUNDATION_EXPORT NSString *const HipChatCloseSearchResultsNotification;
FOUNDATION_EXPORT NSString *const HipChatPresenceChangedNotification;

FOUNDATION_EXPORT NSString *const OWNER_AFFILIATION;
FOUNDATION_EXPORT NSString *const ADMIN_AFFILIATION;
FOUNDATION_EXPORT NSString *const MEMBER_AFFILIATION;

FOUNDATION_EXPORT NSString *const MODERATOR_ROLE;
FOUNDATION_EXPORT NSString *const MEMBER_ROLE;
FOUNDATION_EXPORT NSString *const VISITOR_ROLE;

FOUNDATION_EXPORT NSString *const PERM_CREATE_ROOMS;
FOUNDATION_EXPORT NSString *const PERM_SHARE_FILES;
FOUNDATION_EXPORT NSString *const PERM_EDIT_HISTORY;
FOUNDATION_EXPORT NSString *const PERM_EDIT_PROFILE;
FOUNDATION_EXPORT NSString *const PERM_GUEST_ACCESS;
FOUNDATION_EXPORT NSString *const PERM_PRIVATE_CHAT;
FOUNDATION_EXPORT NSString *const PERM_SEND_INVITES;

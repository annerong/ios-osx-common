//
//  HipChatXMPPAuthentication.m
//  HCCommon
//
//  Created by Christopher Rivers on 10/4/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "HipChatXMPPAuthentication.h"
#import "HipChatApp.h"
#import "XMPP.h"
#import "XMPPStream.h"
#import "XMPPLogging.h"
#import "XMPPInternal.h"
#import "NSData+XMPP.h"
#import "NSXMLElement+XMPP.h"

#if ! __has_feature(objc_arc)
#warning This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

// Log levels: off, error, warn, info, verbose
#if DEBUG
  static const int xmppLogLevel = XMPP_LOG_LEVEL_INFO; // | XMPP_LOG_FLAG_TRACE;
#else
  static const int xmppLogLevel = XMPP_LOG_LEVEL_WARN;
#endif

@implementation HipChatXMPPAuthentication {
  #if __has_feature(objc_arc_weak)
	__weak XMPPStream *xmppStream;
  #else
	__unsafe_unretained XMPPStream *xmppStream;
  #endif

	NSString *email;
	NSString *password;
	NSString *resource;
}

+ (NSString *)mechanismName {
	return @"HIPCHAT";
}

//
- (id)initWithStream:(XMPPStream *)stream
            password:(NSString *)inPassword {
    @throw [NSException exceptionWithName:@"HipChatException"
                                   reason:@"HipChat auth must use initWithStream:email:password:resource:"
                                 userInfo:nil];
}

- (id)initWithStream:(XMPPStream *)stream
               email:(NSString *)inEmail
            password:(NSString *)inPassword
            resource:(NSString *)inResource {
	if (self = [super init]) {
		xmppStream = stream;
		email = inEmail;
		password = inPassword;
		resource = inResource;
	}
	return self;
}

- (BOOL)start:(NSError **)errPtr {
	XMPPLogTrace();

	// <auth xmlns="http://hipchat.com">Base-64-Info</auth>

    NSString *payload = [NSString stringWithFormat:@"%C%@%C%@%C%@",
                         (unsigned short)0,
                         email,
                         (unsigned short)0,
                         password,
                         (unsigned short)0,
                         resource];
    NSString *base64 = [[payload dataUsingEncoding:NSUTF8StringEncoding] base64Encoded];

	NSString *versionNumber = [[NSBundle mainBundle]
                               objectForInfoDictionaryKey:@"CFBundleVersion"];

    // Include node (client type) and ver (version) in auth node
    // We also send this data in the caps node, but we want to be able to
    // require a minimum version when signing in (without having to go through auth
    // handshake beforehand)
    NSXMLElement *auth = [NSXMLElement elementWithName:@"auth" xmlns:@"http://hipchat.com"];
    [auth setStringValue:base64];
#if TARGET_OS_IPHONE
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [auth addAttributeWithName:@"node" stringValue:@"http://hipchat.com/client/ios/ipad"];
    } else {
        [auth addAttributeWithName:@"node" stringValue:@"http://hipchat.com/client/ios/iphone"];
    }
#else
    [auth addAttributeWithName:@"node" stringValue:@"http://hipchat.com/client/mac"];
#endif
	[auth addAttributeWithName:@"ver" stringValue:versionNumber];
	[xmppStream sendAuthElement:auth];

	return YES;
}

- (BOOL)shouldResendOpeningNegotiationAfterSuccessfulAuthentication {
    return NO;
}

- (XMPPHandleAuthResponse)handleAuth:(NSXMLElement *)authResponse {
	XMPPLogTrace();

	// We're expecting a success response.
	// If we get anything else we can safely assume it's the equivalent of a failure response.
	if ([[authResponse name] isEqualToString:@"success"]) {
        xmppStream.myJID = [XMPPJID jidWithString:[authResponse attributeStringValueForName:@"jid"]];

        // Set host names (muc/web/api/chat) with the attributes from the success response if applicable
        [HipChatApp.instance setHostsWithAuth:authResponse];

		return XMPP_AUTH_SUCCESS;
	} else {
		return XMPP_AUTH_FAIL;
	}
}

@end

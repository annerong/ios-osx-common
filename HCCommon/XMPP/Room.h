//
//  Room.h
//  hipchat
//
//  Created by Christopher Rivers on 10/7/10.
//  Copyright 2010 HipChat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "HCCachedData.h"
#import "HipChatApp.h"

@class GCDMulticastDelegate;
@class NSXMLElement;
@class RoomOccupant;
@class XMPPJID;

@protocol RoomDelegate;

@interface Room : NSObject <HCCachedObject> {
    // Additional properties
	NSArray *admins;
    NSMutableArray *gadgets;
    NSString *guestMentionRegex;
    BOOL isCreated;
    BOOL listeningToConn;
    NSArray *membersToAdd;
    NSString *mentionRegex;
	GCDMulticastDelegate <RoomDelegate> *multicastDelegate;
	NSMutableDictionary *occupants;
    NSString *_privacy;
    BOOL sendInvitesOnCreate;
}

- (void)fillWithJsonData:(NSDictionary *)jsonData;
- (void)updateWithInvite:(NSXMLElement *)invite;
- (void)updateWithJid:(XMPPJID *)jid
             name:(NSString *)name
            topic:(NSString *)topic
          privacy:(NSString *)privacy;
- (void)updateWithItem:(NSXMLElement *)item;

- (void)addDelegate:(id)delegate;
- (void)removeDelegate:(id)delegate;

- (void)changePrivacy:(NSString *)newPrivacy callback:(IQCallback)callback;
- (void)destroyWithReason:(NSString *)reason callback:(IQCallback)callback;
- (void)fetchMembersWithCallback:(IQCallback)callback;
- (RoomOccupant *)getOccupant:(XMPPJID *)occJid;
- (void)invite:(XMPPJID *)jid reason:(NSString *)reason;
- (void)inviteMultiple:(NSArray *)jids reason:(NSString *)reason;
- (BOOL)isAdmin:(XMPPJID *)jid;
- (BOOL)isPrivate;
- (BOOL)isPublic;
- (void)join;
- (void)joinWithHistoryCount:(NSInteger)count;
- (void)leave;
- (void)removeUser:(XMPPJID *)jid callback:(IQCallback)callback;
- (void)removeMultipleUsers:(NSArray *)jids callback:(IQCallback)callback;
- (void)rename:(NSString *)newName callback:(IQCallback)callback;
- (void)setArchived:(BOOL)archived callback:(IQCallback)callback;
- (void)setGuestAccess:(BOOL)enabled callback:(IQCallback)callback;
- (void)setMembersToAdd:(NSArray *)members sendInvites:(BOOL)sendInvites;
- (void)setNewTopic:(NSString *)newTopic;

@property (readonly) NSArray *admins;
@property (readonly) NSArray *gadgets;
@property NSString *guestUrl;
@property BOOL historyLoaded;
@property (readonly) NSString *icon;
@property BOOL isArchived;
@property XMPPJID *jid;
@property (readonly) NSArray *membersToAdd;
@property (readonly) NSString *mentionRegex;
@property (readonly) GCDMulticastDelegate <RoomDelegate> *multicastDelegate;
@property NSString *name;
@property BOOL needsRemoteCreation;
@property NSNumber *numParticipants;
@property (readonly) NSMutableDictionary *occupants;
@property XMPPJID *owner;
@property NSString *privacy;
@property NSString *roomId;
@property BOOL rosterLoaded;
@property NSString *topic;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@protocol RoomDelegate
@optional

- (void)room:(Room *)room occupantAdded:(XMPPJID *)jid;
- (void)room:(Room *)room occupantRemoved:(XMPPJID *)jid;
- (void)roomRosterLoaded:(Room *)room;
- (void)roomHistoryLoaded:(Room *)room;
- (void)room:(Room *)room topicChanged:(NSString *)topic;

@end
//
//  Profile.m
//  HCCommon
//
//  Created by Christopher Rivers on 11/16/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "Profile.h"

#import "Helpers.h"
#import "HipChatApp.h"
#import "XMPPFramework.h"

@implementation Profile

@synthesize jid;

- (id)initWithIq:(XMPPIQ *)iq {
    self = [super init];
    if (self) {
        jid = iq.from;

        NSXMLElement *query = [iq elementForName:@"query" xmlns:@"http://hipchat.com/protocol/profile"];
        self.email = [[query elementForName:@"email"] stringValue];
        self.name = [[query elementForName:@"name"] stringValue];
        self.mentionName = [[query elementForName:@"mention_name"] stringValue];
        self.photoURLLarge = [[query elementForName:@"photo_large"] stringValue];
        self.photoURLSmall = [[query elementForName:@"photo_small"] stringValue];
        self.title = [[query elementForName:@"title"] stringValue];
    }

    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"ProfileData {jid: %@, email: %@, name: %@, mentionName: %@, photoLarge: %@, photoSmall: %@, title: %@}",
            jid, self.email, self.name, self.mentionName, self.photoURLLarge, self.photoURLSmall, self.title];
}

@end

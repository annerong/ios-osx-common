//
//  Profile.h
//  HCCommon
//
//  Created by Christopher Rivers on 11/16/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class XMPPIQ;
@class XMPPJID;

@interface Profile : NSObject {
    XMPPJID *jid;
}

- (id)initWithIq:(XMPPIQ *)iq;

@property (readonly) XMPPJID *jid;
@property (readwrite, retain) NSString *email;
@property (readwrite, retain) NSString *name;
@property (readwrite, retain) NSString *mentionName;
@property (readwrite, retain) NSString *photoURLLarge;
@property (readwrite, retain) NSString *photoURLSmall;
@property (readwrite, retain) NSString *title;

@end

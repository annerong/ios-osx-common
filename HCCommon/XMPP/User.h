//
//  User.h
//  HCCommon
//
//  Created by Christopher Rivers on 2/11/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HCCachedData.h"

@class NSXMLElement;
@class XMPPJID;
@class XMPPPresence;

@interface User : NSObject <HCCachedObject>

- (void)fillWithRosterItem:(NSXMLElement *)item;
- (void)fillWithJsonData:(NSDictionary *)jsonData;

- (void)clearLastPresence;

- (void)updateWithPresence:(XMPPPresence *)presence;
- (void)updateWithRosterItem:(NSXMLElement *)item;

@property XMPPJID *jid;
@property NSString *name;
@property NSString *displayName;
@property XMPPPresence *lastPresence;
@property NSString *mentionName;
@property NSString *mobile;
@property (readonly) NSString *regexMentionName;

@end

//
//  RoomList.h
//  HipChat
//
//  Created by Garret Heaton on 10/1/10.
//  Copyright 2010 HipChat, Inc. All rights reserved.
//

#import "HCCachedData.h"
#import "HipChatApp.h"

@class Room;
@class GCDMulticastDelegate;
@class NSXMLElement;
@class XMPPMessage;
@class XMPPJID;
@class XMPPStream;
@protocol RoomListDelegate;

@interface RoomList : HCCachedData {
	GCDMulticastDelegate <RoomListDelegate> *multicastDelegate;
	XMPPStream *xmppStream;
    BOOL isPopulating;
    NSString *discoId;
}

- (id)initWithStream:(XMPPStream *)xmppStream;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Delegation
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)addDelegate:(id)delegate delegateQueue:(dispatch_queue_t)delegateQueue;
- (void)removeDelegateImmediate:(id)delegate;
- (void)removeDelegate:(id)delegate;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Population functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)handleDiscoItem:(NSXMLElement *)item;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Public functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (Room *)addRoomWithInvite:(XMPPMessage *)invite;
- (Room *)createNewRoomWithJid:(XMPPJID *)jid
                          name:(NSString *)name
                         topic:(NSString *)topic
                       privacy:(NSString *)privacy;
- (void)destroyRoom:(XMPPJID *)roomJid withReason:(NSString *)reason callback:(IQCallback)callback;
- (void)fetchItems:(NSString *)mucHost;
- (void)removeRoom:(XMPPJID *)roomJid;
- (Room *)roomForJid:(XMPPJID *)jid;
- (Room *)roomForName:(NSString *)name;

@property (nonatomic, readonly) XMPPStream *xmppStream;
@property (nonatomic, readonly) BOOL refreshedFromServer;
@property (readwrite, copy) NSString *discoId;

@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark RoomListDelegate functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@protocol RoomListDelegate
@optional

/**
 * Sent when a disco response is received
 **/
- (void)roomListLoaded:(RoomList *)sender;

/**
 * Sent when the room list is changed or cleared (e.g. on sign out)
 **/
- (void)roomListCleared:(RoomList *)sender;

/**
 * Sent when a room is added to the list
 **/
- (void)roomList:(RoomList *)sender addedRoom:(Room *)room;

/**
 * Sent when a room is deleted from the list
 **/
- (void)roomList:(RoomList *)sender deletedRoom:(XMPPJID *)jid;

@end

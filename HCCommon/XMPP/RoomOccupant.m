//
//  RoomOccupant.m
//  hipchat
//
//  Created by Christopher Rivers on 3/7/11.
//  Copyright 2011 HipChat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RegexKitLite.h"
#import "RoomOccupant.h"
#import "XMPP.h"

@implementation RoomOccupant

@synthesize affiliation;
@synthesize jid;
@synthesize nickname;
@dynamic lastPresence;
@synthesize role;

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark KVO method for lastPresence
///////////////////////////////////////////////////////////////////////

- (void)setLastPresence:(XMPPPresence *)newLastPresence {
    if (![newLastPresence isEqual:lastPresence]) {
        [self willChangeValueForKey:@"lastPresence"];
        lastPresence = newLastPresence;
        [self didChangeValueForKey:@"lastPresence"];
        [[NSNotificationCenter defaultCenter] postNotificationName:HipChatPresenceChangedNotification object:self];
    }
}

- (XMPPPresence *)lastPresence {
    return lastPresence;
}

- (NSString *)regexMentionName {
    // Replace regex reserved characters \, *, +, ?, ^, -
    return [self.mentionName stringByReplacingOccurrencesOfRegex:@"([\\\\*+?\\^\\-])" withString:@"\\$1"];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper methods
///////////////////////////////////////////////////////////////////////

- (BOOL)isAdmin {
    return ([self.affiliation isEqualToString:@"admin"] || [self.affiliation isEqualToString:@"owner"]);
}

- (void)updateWithPresence:(XMPPPresence *)presence {
	NSXMLElement *userNode = [presence elementForName:@"x" xmlns:@"http://jabber.org/protocol/muc#user"];
	if (!userNode) {
		return;
	}

	NSXMLElement *itemNode = [userNode elementForName:@"item"];
	self.affiliation = [itemNode attributeStringValueForName:@"affiliation"];
	self.jid = [XMPPJID jidWithString:[itemNode attributeStringValueForName:@"jid"]];
	self.nickname = [presence.from resource];
	self.lastPresence = presence;
	self.role = [itemNode attributeStringValueForName:@"role"];
    NSString *mentionName = [itemNode attributeStringValueForName:@"mention_name"];
    if (mentionName) {
        self.mentionName = mentionName;
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Logging description (%@)
///////////////////////////////////////////////////////////////////////

- (NSString *)description {
    return [NSString stringWithFormat:@"RoomOccupant -- jid: %@ -- nickname: %@ -- role: %@ -- affiliation: %@ -- lastPresence: %@",
            self.jid, self.nickname, self.role, self.affiliation, self.lastPresence];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Creation static methods
///////////////////////////////////////////////////////////////////////

+ (RoomOccupant *)createWithPresence:(XMPPPresence *)presence {
	RoomOccupant *occ = [[RoomOccupant alloc] init];
    [occ updateWithPresence:presence];

	return occ;
}

@end

//
//  ChatStateMessage.h
//  hipchat
//
//  Created by Christopher Rivers on 5/14/12.
//  Copyright (c) 2012 Plaxo. All rights reserved.
//

#import "XMPPMessage.h"

#define ACTIVE_CHAT_STATE @"active"
#define COMPOSING_CHAT_STATE @"composing"
#define GONE_CHAT_STATE @"gone"
#define INACTIVE_CHAT_STATE @"inactive"
#define NS_CHAT_STATES @"http://jabber.org/protocol/chatstates"

@interface ChatStateMessage : XMPPMessage

// TODO: Have automatic parsing for chat state nodes?
// Currently we're just using this class as a place to define the consts above

+ (ChatStateMessage *)chatStateMessageWithState:(NSString *)state;

@end

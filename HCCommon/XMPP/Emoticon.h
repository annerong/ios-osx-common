//
//  Emoticon.h
//  hipchat
//
//  Created by Christopher Rivers on 8/22/12.
//
//

#import "HCCachedData.h"

@class NSXMLElement;

@interface Emoticon : NSObject <HCCachedObject>

- (void)fillWithJsonData:(NSDictionary *)jsonData;

- (void)updateWithItem:(NSXMLElement *)item;

@property (nonatomic, retain) NSString *path;
@property (nonatomic, retain) NSString *shortcut;
@property (nonatomic, retain) NSNumber *width;
@property (nonatomic, retain) NSNumber *height;
@property (nonatomic, retain) NSString *audio_path;
@property (nonatomic, retain) NSString *emoticon_id;

@end

//
//  Roster.h
//  HCCommon
//
//  Created by Christopher Rivers on 2/11/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HCCachedData.h"

@class User;
@protocol RosterDelegate;

@interface Roster : HCCachedData {
    GCDMulticastDelegate <RosterDelegate> *multicastDelegate;

    NSMutableArray *earlyPresences;

    BOOL readyForPresence;
    BOOL requestedRoster;
    BOOL presenceFetchScheduled;

    NSMutableSet *pendingPresenceFetchJids;

    NSString *presenceRequestId;
    NSString *rosterRequestId;
}

- (id)initWithStream:(XMPPStream *)conn;

- (void)addDelegate:(id)delegate delegateQueue:(dispatch_queue_t)delegateQueue;
- (void)removeDelegateImmediate:(id)delegate;
- (void)removeDelegate:(id)delegate;

- (void)fetchWithVersion:(NSString *)version;
- (void)requestPresences;

- (void)requestPresenceForJid:(NSString *)jid;
- (void)requestPresencesForJidsImmediate:(NSSet *)jids;

- (User *)userForBareJid:(NSString *)bareJid;
- (User *)userForJid:(XMPPJID *)jid;

@property (nonatomic, copy) NSString *presenceRequestId;

@end

@protocol RosterDelegate
@optional

/**
 * Sent when the initial roster load response is received
 **/
- (void)rosterFinishedLoading:(Roster *)sender;

/**
 * Sent when the initial roster load response is received
 **/
- (void)presenceFinishedLoading:(Roster *)sender;

/**
 * Sent when the roster is changed or cleared (e.g. on sign out)
 **/
- (void)rosterCleared:(Roster *)sender;

/**
 * Sent when a user is added to the list
 **/
- (void)roster:(Roster *)sender addedUser:(User *)user;

/**
 * Sent when a user is deleted from the list
 **/
- (void)roster:(Roster *)sender removedUser:(XMPPJID *)jid;

@end

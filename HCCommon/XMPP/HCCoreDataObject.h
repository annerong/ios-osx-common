//
//  HCCoreDataObject.h
//  hipchat
//
//  Created by Christopher Rivers on 8/22/12.
//
//

@class NSManagedObjectModel;
@class NSPersistentStoreCoordinator;
@class NSManagedObjectContext;

@interface HCCoreDataObject : NSObject {
@private

    NSManagedObjectModel *managedObjectModel;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
    NSManagedObjectContext *managedObjectContext;

@protected

	dispatch_queue_t storageQueue;
}

- (NSManagedObjectModel *)managedObjectModel;
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator;
- (NSManagedObjectContext *)managedObjectContext;

- (void)executeBlock:(dispatch_block_t)block;
- (void)scheduleBlock:(dispatch_block_t)block;

- (void)clearData;

///////////////////////////////////////////////////////////////////////
#pragma mark Subclass methods (must be overridden)
///////////////////////////////////////////////////////////////////////

- (NSString *)getStoreFileName;
- (NSString *)getDataModelName;



@end
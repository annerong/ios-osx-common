//
//  HCCoreData.m
//  hipchat
//
//  Created by Christopher Rivers on 8/22/12.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <objc/runtime.h>

#import "HipChatApp.h"
#import "HCCoreDataObject.h"
#import "DDLog.h"

@implementation HCCoreDataObject

- (id)init {
    self = [super init];
    if (self) {
        storageQueue = dispatch_queue_create(class_getName([self class]), NULL);
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Core Data Setup
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSString *)persistentStoreDirectory {
#if TARGET_OS_IPHONE

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *result = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;

#else

	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : NSTemporaryDirectory();

	NSString *result = [basePath stringByAppendingPathComponent:@"hipchat"];

#endif

	NSFileManager *fileManager = [NSFileManager defaultManager];

	if(![fileManager fileExistsAtPath:result]) {
		[fileManager createDirectoryAtPath:result withIntermediateDirectories:YES attributes:nil error:nil];
	}

    return result;
}

- (NSManagedObjectModel *)managedObjectModel {
    __block NSManagedObjectModel *result = nil;

	dispatch_block_t block = ^{ @autoreleasepool {
        if (managedObjectModel) {
            result = managedObjectModel;
            return;
        }

        NSString *path = [[NSBundle mainBundle] pathForResource:[self getDataModelName] ofType:@"momd"];
        if (path) {
            // If path is nil, then NSURL or NSManagedObjectModel will throw an exception
            NSURL *url = [NSURL fileURLWithPath:path];
            managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:url];
        }

        result = managedObjectModel;
    }};

    if (dispatch_get_current_queue() == storageQueue) {
		block();
	} else {
		dispatch_sync(storageQueue, block);
    }

    return result;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    __block NSPersistentStoreCoordinator *result = nil;

	dispatch_block_t block = ^{ @autoreleasepool {
    
        if (persistentStoreCoordinator) {
            result = persistentStoreCoordinator;
            return;
        }

        NSManagedObjectModel *mom = [self managedObjectModel];

        persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];

        NSString *docsPath = [self persistentStoreDirectory];
        NSString *storePath = [docsPath stringByAppendingPathComponent:[self getStoreFileName]];
        if (storePath) {
            // If storePath is nil, then NSURL will throw an exception
            NSURL *storeUrl = [NSURL fileURLWithPath:storePath];

            NSError *error = nil;
            NSPersistentStore *persistentStore;
            persistentStore = [persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                                       configuration:nil
                                                                                 URL:storeUrl
                                                                             options:nil
                                                                               error:&error];
            if(!persistentStore) {
                NSLog(@"=====================================================================================");
                NSLog(@"Error creating persistent store:\n%@", error);
    #if TARGET_OS_IPHONE
                NSLog(@"Changed core data model recently? Quick Fix: Delete the app from device and reinstall.");
    #else
                NSLog(@"Quick Fix: Delete the database: %@", storePath);
    #endif
                NSLog(@"=====================================================================================");

                [self clearData];
                persistentStore = [persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                                           configuration:nil
                                                                                     URL:storeUrl
                                                                                 options:nil
                                                                                   error:&error];
            }
        }

        result = persistentStoreCoordinator;
    }};

    if (dispatch_get_current_queue() == storageQueue)
		block();
	else
		dispatch_sync(storageQueue, block);

    return result;
}

- (NSManagedObjectContext *)managedObjectContext {
    NSAssert(dispatch_get_current_queue() == storageQueue, @"Invoked on incorrect queue");

	if (managedObjectContext) {
		return managedObjectContext;
	}

	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (coordinator) {
		managedObjectContext = [[NSManagedObjectContext alloc] init];
		[managedObjectContext setPersistentStoreCoordinator:coordinator];
	}

	return managedObjectContext;
}

- (void)executeBlock:(dispatch_block_t)block {
    // This method is used to invoke methods using the manageObjectContext from OTHER queues
    // in order to ensure that the managedObjectContext only gets accessed on storageQueue
	NSAssert(dispatch_get_current_queue() != storageQueue, @"Invoked on incorrect queue");

	dispatch_sync(storageQueue, ^{ @autoreleasepool {
		block();
	}});
}

- (void)scheduleBlock:(dispatch_block_t)block {
    // This method is used to invoke methods using the manageObjectContext from OTHER queues
    // in order to ensure that the managedObjectContext only gets accessed on storageQueue
	NSAssert(dispatch_get_current_queue() != storageQueue, @"Invoked on incorrect queue");

	dispatch_async(storageQueue, ^{ @autoreleasepool {
		block();
	}});
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Data control functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)clearData {
    DDLogInfo(@"Clearing all stored data. MODEL: %@ -- FILE: %@", [self getDataModelName], [self getStoreFileName]);
    NSString *docsPath = [self persistentStoreDirectory];
	NSString *storePath = [docsPath stringByAppendingPathComponent:[self getStoreFileName]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:storePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:storePath error:nil];
    }

    if (persistentStoreCoordinator) {
        persistentStoreCoordinator = nil;
    }
    if (managedObjectContext) {
        managedObjectContext = nil;
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark Subclass methods (must be overridden)
///////////////////////////////////////////////////////////////////////

- (NSString *)getStoreFileName {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}

- (NSString *)getDataModelName {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}



@end

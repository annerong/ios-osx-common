//
//  ChatStateMessage.m
//  hipchat
//
//  Created by Christopher Rivers on 5/14/12.
//  Copyright (c) 2012 Plaxo. All rights reserved.
//

#import "ChatStateMessage.h"
#import "XMPP.h"

@implementation ChatStateMessage

+ (ChatStateMessage *)chatStateMessageWithState:(NSString *)state {
    ChatStateMessage *stateMessage = [[ChatStateMessage alloc] initWithType:@"chat"];
    NSXMLElement *stateNode = [NSXMLElement elementWithName:state
                                                      xmlns:NS_CHAT_STATES];
    [stateMessage addChild:stateNode];
    return stateMessage;
}

@end

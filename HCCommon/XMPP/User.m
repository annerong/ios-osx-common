//
//  User.m
//  HCCommon
//
//  Created by Christopher Rivers on 2/11/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "User.h"

#import "RegexKitLite.h"
#import "Roster.h"
#import "XMPPFramework.h"

@implementation User

- (void)fillWithJsonData:(NSDictionary *)jsonData {
    self.jid = [XMPPJID jidWithString:[jsonData objectForKey:@"jid"]];
    self.name = [jsonData objectForKey:@"name"];
    self.displayName = [jsonData objectForKey:@"displayName"];
    self.mentionName = [jsonData objectForKey:@"mentionName"];
    self.mobile = [jsonData objectForKey:@"mobile"];
}

- (void)fillWithRosterItem:(NSXMLElement *)item {
    NSString *jidStr = [item attributeStringValueForName:@"jid"];
    XMPPJID *jid = [XMPPJID jidWithString:jidStr];

    if (jid == nil)	{
        DDLogError(@"User: invalid item (missing or invalid jid): %@", item);
        return;
    }

    self.jid = jid;
    [self updateWithRosterItem:item];
}

- (void)clearLastPresence {
    self.lastPresence = nil;
}

- (NSString *)key {
    return self.jid.bare;
}

- (NSString *)regexMentionName {
    // Replace regex reserved characters \, *, +, ?, ^, -
    return [self.mentionName stringByReplacingOccurrencesOfRegex:@"([\\\\*+?\\^\\-])" withString:@"\\$1"];
}

- (void)updateWithPresence:(XMPPPresence *)presence {
    self.lastPresence = presence;
}

- (void)setLastPresence:(XMPPPresence *)lastPresence {
    if (![lastPresence isEqual:_lastPresence]) {
        _lastPresence = lastPresence;
        [[NSNotificationCenter defaultCenter] postNotificationName:HipChatPresenceChangedNotification object:self];
    }
}

- (void)updateWithRosterItem:(NSXMLElement *)item {
    self.name = [item attributeStringValueForName:@"name"];
    self.displayName = [item attributeStringValueForName:@"display_name"];
    self.mentionName = [item attributeStringValueForName:@"mention_name"];
    self.mobile = [item attributeStringValueForName:@"mobile"];
}

- (NSDictionary *)proxyForJson {
    return [NSDictionary dictionaryWithObjectsAndKeys:
            self.jid.bare, @"jid",
            self.name, @"name",
            // Ignore display name for now - we don't use it
//            (self.displayName ? self.displayName : @""), @"displayName",
            self.mentionName, @"mentionName",
            self.mobile, @"mobile",
            nil];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<User: JID: %@ -- NAME: %@>", self.jid, self.name];
}

@end

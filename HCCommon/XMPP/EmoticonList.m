//
//  EmoticonList.m
//  hipchat
//
//  Created by Christopher Rivers on 8/22/12.
//
//

#import "HipChatApp.h"
#import "Emoticon.h"
#import "EmoticonList.h"
#import "Linkify.h"
#import "ThreadSafeDictionary.h"

@implementation EmoticonList


- (EmoticonList *)init {
    if (self = [super init]) {
        _objects = [[ThreadSafeDictionary alloc] initWithQueueName:"com.hipchat.emoticonList"];
        multicastDelegate = (GCDMulticastDelegate <EmoticonListDelegate> *) [[GCDMulticastDelegate alloc] init];
    }
    return self;
}

- (void)clearData {
    [super clearData];
    [self runBlock:^{
        [multicastDelegate emoticonListCleared:self];
    }];
}

- (void)fetchEmoticons {
    if ([self itemCount] == 0) {
        [self loadFromStore];
        if ([self itemCount] != 0) {
            [multicastDelegate emoticonListLoaded:self];
        }
    }

    NSString *uuid = [NSString stringWithFormat:@"emoticon_%@", [XMPPStream generateUUID]];
    XMPPIQ *iq = [XMPPIQ iqWithType:@"get" elementID:uuid];

    NSXMLElement *queryNode = [NSXMLElement elementWithName:@"query" xmlns:@"http://hipchat.com/protocol/emoticons"];

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *version = [prefs stringForKey:@"emoticonsVersion"];
    if (version) {
        [queryNode addAttributeWithName:@"ver" stringValue:version];
    }
    [iq addChild:queryNode];

    [HipChatApp.instance sendIQ:iq callback:^(XMPPIQ *responseIq) {
        [self handleEmoticonsFetch:responseIq];
    }];
}

- (Emoticon *)emoticonForShortcut:(NSString *)shortcut {
    if (shortcut == nil) {
        return nil;
    }
    return [self objectForKey:shortcut];
}

- (void)handleEmoticonsFetch:(XMPPIQ *)response {
    DDLogInfo(@"Got response for get_emoticons");

    NSXMLElement *query = [response elementForName:@"query" xmlns:@"http://hipchat.com/protocol/emoticons"];

    NSString *pathPrefix = [[query elementForName:@"path_prefix"] stringValue];

    // If we got a path prefix, save it to local prefs
    // Otherwise, load the last prefix we stored in prefs
    if (pathPrefix && pathPrefix.length > 0) {
        [[NSUserDefaults standardUserDefaults] setObject:pathPrefix forKey:@"emoticonPathPrefix"];
    } else {
        pathPrefix = [[NSUserDefaults standardUserDefaults] stringForKey:@"emoticonPathPrefix"];
    }

    if (pathPrefix.length > 0) {
        [Linkify setEmoticonBasePath:pathPrefix];
    }

    NSArray *items = [query elementsForName:@"item"];
    if ([items count] == 0) {
        [self loadFromStore];
    } else {
        [self updateEmoticonsWithQuery:query];
    }
    [multicastDelegate emoticonListLoaded:self];
}

- (void)handleEmoticonPush:(XMPPIQ *)iq {
    NSXMLElement *query = [iq elementForName:@"query" xmlns:@"http://hipchat.com/protocol/emoticons"];
    [self updateEmoticonsWithQuery:query];
}

- (BOOL)loadFromStore {
    DDLogInfo(@"Loading emoticons from persistent store and adding to Linkify");
    if ([super loadFromStore]) {
        for (id key in [self getData]) {
            Emoticon *emoticon = [self objectForKey:key];
            [Linkify addEmoticon:emoticon];
        }
        return YES;
    }
    return NO;
}

- (void)updateEmoticonsWithQuery:(NSXMLElement *)query {
    NSArray *items = [query elementsForName:@"item"];

    // Set or unset version, depending on the response we got
    NSString *version = [query attributeStringValueForName:@"ver"];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if (version) {
        [prefs setObject:version forKey:@"emoticonsVersion"];
    } else {
        [prefs removeObjectForKey:@"emoticonsVersion"];
    }

    if ([prefs synchronize]) {
        DDLogInfo(@"Saved emoticon version to prefs: %@", version);
    } else {
        DDLogError(@"Failed to save emoticon version to prefs: %@", version);
    }

    for (NSXMLElement *item in items) {
        // Save emoticon in core data for fetch on login
        NSString *shortcut = [[item elementForName:@"shortcut"] stringValue];
        Emoticon *emoticon = [self getOrCreateObjectForKey:shortcut];
        [emoticon updateWithItem:item];

        // Add emoticon to linkify cache
        [Linkify addEmoticon:emoticon];
    }
    [self saveToStore];
}

- (void)removeEmoticon:(NSString *)shortcut {
    [self removeObjectForKey:shortcut];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Delegation
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)addDelegate:(id)delegate {
    [self runBlock:^{
        [multicastDelegate addDelegate:delegate delegateQueue:dispatch_get_main_queue()];
    }];
}

- (void)removeDelegate:(id)delegate {
    [self runBlock:^{
        [multicastDelegate removeDelegate:delegate];
    }];
}

- (void)runBlock:(dispatch_block_t)block {
    if (dispatch_get_current_queue() == HipChatApp.hipchatQueue)
        block();
    else
        dispatch_async(HipChatApp.hipchatQueue, block);
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Subclass methods (must be overridden)
///////////////////////////////////////////////////////////////////////

- (NSString *)storeName {
    return @"emoticons";
}

- (id <HCCachedObject>)generateObjectWithData:(NSDictionary *)data {
    Emoticon *emoticon = [self getOrCreateObjectForKey:[data objectForKey:@"shortcut"]];
    [emoticon fillWithJsonData:data];
    return emoticon;
}

- (Class)objectClass {
    return [Emoticon class];
}

@end

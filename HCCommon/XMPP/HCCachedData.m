//
//  HCCachedData.m
//  HipChat
//
//  Created by Christopher Rivers on 3/19/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "HCCachedData.h"
#import "HipChatApp.h"

#import "SBJson.h"
#import "ThreadSafeDictionary.h"

@implementation HCCachedData

- (id)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (id <HCCachedObject>)generateObjectWithData:(NSDictionary *)data {
    [NSException raise:@"ImplementationException" format:@"Subclass must implement generateObjectWithData"];
    return nil;
}

- (Class)objectClass {
    [NSException raise:@"ImplementationException" format:@"Subclass must implement objectClass"];
    return nil;
}

- (id <HCCachedObject>)getOrCreateObjectForKey:(NSString *)key {
    if (!key) {
        return nil;
    }

    __block id <HCCachedObject> obj;
    dispatch_barrier_sync(_objects.collectionQueue, ^{
        obj = [_objects.internalDictionary objectForKey:key];
        if (!obj) {
            obj = [[[self objectClass] alloc] init];
            [_objects.internalDictionary setObject:obj forKey:key];
        }
    });
    return obj;
}

- (void)clearData {
    [_objects removeAllObjects];
    [self saveToStore];
}

/**
* Return a thread safe copy of the objects dict
*/
- (NSDictionary *)getData {
    return [_objects immutableCopy];
}

- (NSUInteger)itemCount {
    return [_objects count];
}

- (id)objectForKey:(id)key {
    return [_objects objectForKey:key];
}

- (void)setObject:(id)obj forKey:(id <NSCopying>)key {
    [_objects setObject:obj forKey:key];
}

- (void)removeObjectForKey:(id)key {
    [_objects removeObjectForKey:key];
}

- (NSSet *)keysOfEntriesPassingTest:(BOOL (^)(id key, id obj, BOOL *stop))predicate {
    return [_objects keysOfEntriesPassingTest:predicate];
}


- (NSString *)storeDirectory {
#if TARGET_OS_IPHONE
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *baseDir = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *storeDirectory = [[baseDir stringByAppendingPathComponent:@"HipChat"] stringByAppendingPathComponent:[HipChatApp.instance getSignInEmail]];
#else
	NSString *appName = [[NSProcessInfo processInfo] processName];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
	NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : NSTemporaryDirectory();
    HipChatApp *app = HipChatApp.instance;
	NSString *storeDirectory = [[basePath stringByAppendingPathComponent:app.chatHost] stringByAppendingPathComponent:appName];
#endif
    if (![[NSFileManager defaultManager] fileExistsAtPath:storeDirectory]) {
        NSError *err = nil;
        if (![[NSFileManager defaultManager] createDirectoryAtPath:storeDirectory
                                       withIntermediateDirectories:YES
                                                        attributes:nil
                                                             error:&err]) {
            DDLogError(@"Cached data: Error creating storeDirectory: %@", err);
        }
    }
    return storeDirectory;
}

- (NSString *)storeFilePath {
    NSString *fileName = [NSString stringWithFormat:@"%@-store.json", self.storeName];
    NSString *filePath = [[self storeDirectory] stringByAppendingPathComponent:fileName];

    return filePath;
}

- (NSString *)storeName {
    [NSException raise:@"ImplementationException" format:@"Store name must be defined on the subclasss"];
    return nil;
}

- (BOOL)loadFromStore {
    NSString *filePath = [self storeFilePath];
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        DDLogInfo(@"No cached data store found at %@", filePath);
        return NO;
    }

    NSError *err;
    NSString *jsonString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF16StringEncoding error:&err];
    if (err) {
        DDLogError(@"Failed to load cached data from store: %@", [err localizedDescription]);
        return NO;
    }

    id jsonValue = [jsonString JSONValue];
    if (![jsonValue isKindOfClass:[NSDictionary class]]) {
        DDLogError(@"JSON data from cached data store didn't load as dictionary. Failed to load cached data.");
        return NO;
    }

    NSString *key = nil;
    for (key in (NSDictionary *) jsonValue) {
        NSDictionary *jsonData = [jsonValue objectForKey:key];
        id <HCCachedObject> obj = [self generateObjectWithData:jsonData];
        [_objects setObject:obj forKey:obj.key];
    }
    DDLogInfo(@"Loaded cached data from file store: %@", filePath);
    return YES;
}

- (void)saveToStore {
    NSString *filePath = [self storeFilePath];
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        DDLogInfo(@"Cached data: Creating new store file: %@", filePath);
        [[NSFileManager defaultManager] createFileAtPath:filePath contents:nil attributes:nil];
    }
    NSError *err;
    [[_objects JSONRepresentation] writeToFile:filePath
                                    atomically:YES
                                      encoding:NSUTF16StringEncoding
                                         error:&err];
    if (err) {
        DDLogError(@"Error saving cached data to local store: %@", [err localizedDescription]);
    } else {
        DDLogInfo(@"Saved cached data to store: %@", filePath);
    }
}

@end

//
//  HipChatXMPPAuthentication.h
//  HCCommon
//
//  Created by Christopher Rivers on 10/4/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPSASLAuthentication.h"
#import "XMPPStream.h"

@interface HipChatXMPPAuthentication : NSObject <XMPPSASLAuthentication>

// This class implements the XMPPSASLAuthentication protocol.
// 
// See XMPPSASLAuthentication.h for more information.
- (id)initWithStream:(XMPPStream *)stream
               email:(NSString *)inEmail
            password:(NSString *)inPassword
            resource:(NSString *)inResource;

@end

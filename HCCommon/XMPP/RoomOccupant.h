//
//  RoomOccupant.h
//  hipchat
//
//  Created by Christopher Rivers on 3/7/11.
//  Copyright 2011 HipChat. All rights reserved.
//

@class XMPPPresence;
@class XMPPJID;

@interface RoomOccupant : NSObject {
	XMPPPresence *lastPresence;
}

+ (RoomOccupant *)createWithPresence:(XMPPPresence *)presence;

- (void)updateWithPresence:(XMPPPresence *)presence;

@property NSString *affiliation;
@property (readonly) BOOL isAdmin;
@property XMPPJID *jid;
@property NSString *nickname;
@property XMPPPresence *lastPresence;
@property (readonly) NSString *regexMentionName;
@property NSString *role;
@property NSString *mentionName;

@end

//
//  Roster.m
//  HCCommon
//
//  Created by Christopher Rivers on 2/11/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import "Roster.h"

#import "HipChatApp.h"
#import "HipChatUser.h"
#import "User.h"
#import "ThreadSafeDictionary.h"

@implementation Roster

@synthesize presenceRequestId = presenceRequestId;

- (id)initWithStream:(XMPPStream *)conn {
    self = [super init];
    if (self) {
        _objects = [[ThreadSafeDictionary alloc] initWithQueueName:"com.hipchat.roster"];
        [conn addDelegate:self delegateQueue:HipChatApp.hipchatQueue];
        multicastDelegate = (GCDMulticastDelegate <RosterDelegate> *) [[GCDMulticastDelegate alloc] init];

        readyForPresence = NO;
        earlyPresences = [NSMutableArray array];

        pendingPresenceFetchJids = [NSMutableSet set];
    }
    return self;
}

- (id <HCCachedObject>)generateObjectWithData:(NSDictionary *)data {
    XMPPJID *jid = [XMPPJID jidWithString:[data objectForKey:@"jid"]];
    User *user = [self getOrCreateObjectForKey:jid.bare];
    [user fillWithJsonData:data];
    return user;
}

- (Class)objectClass {
    return [User class];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark XMPPStreamDelegate methods
///////////////////////////////////////////////////////////////////////

- (void)addDelegate:(id)delegate delegateQueue:(dispatch_queue_t)delegateQueue {
    [self postDelegateBlock:^{
        [multicastDelegate addDelegate:delegate delegateQueue:delegateQueue];
    }];
}

- (void)removeDelegateImmediate:(id)delegate {
    [multicastDelegate removeDelegate:delegate];
}

- (void)removeDelegate:(id)delegate {
    [self postDelegateBlock:^{
        [self removeDelegateImmediate:delegate];
    }];
}

- (void)postDelegateBlock:(dispatch_block_t)block {
    if (dispatch_get_current_queue() == HipChatApp.hipchatQueue)
        block();
    else
        dispatch_async(HipChatApp.hipchatQueue, block);
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark XMPPStreamDelegate methods
///////////////////////////////////////////////////////////////////////

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error {
    // Only clear resources (not users) on disconnect

    requestedRoster = NO;
    readyForPresence = NO;

    for (User *user in [[self getData] allValues]) {
        [user clearLastPresence];
    }

    [earlyPresences removeAllObjects];
}

- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq {
    NSString *iqId = [iq attributeStringValueForName:@"id"];
    BOOL isResponseToRequest = [iqId isEqualToString:rosterRequestId];
    // For initial population, store retrieved user list in a dictionary
    NSMutableDictionary *newUsersDictionary;
    BOOL triggeredEvent = NO;
    NSArray *startingJids = [self getData].allKeys;

    NSXMLElement *query = [iq elementForName:@"query" xmlns:@"jabber:iq:roster"];
    // Check if we're empty, and the last IQ didn't return any info
    if (!query && [self itemCount] == 0 && isResponseToRequest) {
        // Try again, without version (which should return full roster for a complete refresh)
        requestedRoster = NO;
        [self fetchWithVersion:nil];
        return YES;
    } else if (query) {
        NSArray *items = [query elementsForName:@"item"];

        if (isResponseToRequest) {
            newUsersDictionary = [NSMutableDictionary dictionaryWithCapacity:[items count]];
        }

        for (NSXMLElement *item in items) {
            User *user = [self handleRosterItem:item withNotifications:!isResponseToRequest];
            if (isResponseToRequest) {
                [newUsersDictionary setObject:user forKey:[user key]];
            }
        }

        [self saveToStore];
        NSString *newVersion = [query attributeStringValueForName:@"ver"];
        if (newVersion && newVersion.length > 0) {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [prefs setObject:newVersion forKey:@"rosterVersion"];
        }
    }

    if (isResponseToRequest && [startingJids count] && newUsersDictionary) {
        // Synchronize new list and old list, send notifications
        NSArray *newJids = newUsersDictionary.allKeys;

        NSMutableArray *jidsToDelete = [NSMutableArray arrayWithArray:startingJids];
        [jidsToDelete removeObjectsInArray:newJids];

        NSMutableArray *addedJids = [NSMutableArray arrayWithArray:newJids];
        [addedJids removeObjectsInArray:startingJids];

        for (NSString *jidToDelete in jidsToDelete) {
            [self removeObjectForKey:jidToDelete];
            [self postDelegateBlock:^{
                [multicastDelegate roster:self removedUser:[XMPPJID jidWithString:jidToDelete]];
            }];
            triggeredEvent = YES;
        }
        for (NSString *addedJid in addedJids) {
            // User already added as part of handleRosterItem
            [self postDelegateBlock:^{
                [multicastDelegate roster:self addedUser:[self objectForKey:addedJid]];
            }];
            triggeredEvent = YES;
        }
    }
    triggeredEvent = triggeredEvent & [self triggerLoadedEventsForIqId:iqId];

    return triggeredEvent;
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence {
    if (![self hasRoster] || !readyForPresence) {
        DDLogInfo(@"Received presence before roster was loaded - queueing for later");
        [earlyPresences addObject:presence];
        return;
    }

    [self handlePresence:presence];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Public helper functions
///////////////////////////////////////////////////////////////////////

- (void)clearData {
    dispatch_block_t block = ^{
        [super clearData];
        rosterRequestId = nil;
        presenceRequestId = nil;
        requestedRoster = NO;
        readyForPresence = NO;
    };

    if (dispatch_get_current_queue() == HipChatApp.hipchatQueue) {
        block();
    } else {
        dispatch_async(HipChatApp.hipchatQueue, block);
    }
}

- (void)fetchWithVersion:(NSString *)version {
    if (requestedRoster) {
        // We've already requested the roster from the server.
        return;
    }

    if (![self hasRoster]) {
        [self loadFromStore];
        if ([self hasRoster]) {
            [self postDelegateBlock:^{
                [multicastDelegate rosterFinishedLoading:self];
            }];
        }
    }

    // <iq type="get">
    //   <query xmlns="jabber:iq:roster"/>
    // </iq>
    NSXMLElement *query = [NSXMLElement elementWithName:@"query" xmlns:@"jabber:iq:roster"];

    NSXMLElement *iq = [NSXMLElement elementWithName:@"iq"];
    [iq addAttributeWithName:@"type" stringValue:@"get"];
    [iq addChild:query];

    XMPPStream *conn = HipChatApp.instance.conn;
    rosterRequestId = [conn generateUUID];
    [iq addAttributeWithName:@"id" stringValue:rosterRequestId];
    if (version) {
        [query addAttributeWithName:@"ver" stringValue:version];
    }

    [conn sendElement:iq];
    requestedRoster = YES;
}

- (void)requestPresences {
    XMPPStream *conn = HipChatApp.instance.conn;

    //<iq type="get" id="1">
    //  <query xmlns="http://hipchat.com/protocol/presence" />
    //</iq>
    NSXMLElement *query = [NSXMLElement elementWithName:@"query" xmlns:@"http://hipchat.com/protocol/presence"];

    presenceRequestId = [conn generateUUID];
    NSXMLElement *iq = [NSXMLElement elementWithName:@"iq"];
    [iq addAttributeWithName:@"type" stringValue:@"get"];
    [iq addAttributeWithName:@"id" stringValue:presenceRequestId];
    [iq addChild:query];

    [conn sendElement:iq];
}

- (void)requestPresenceForJid:(NSString *)jid {
    /* By delaying presence requests, we can ensure requests are somewhat "batched",
    rather than blasting the server with a ton of one-off requests */
    @synchronized (pendingPresenceFetchJids) {
        DDLogInfo(@"Delaying presence fetch for %@", jid);
        [pendingPresenceFetchJids addObject:jid];
        if (!presenceFetchScheduled) {
            presenceFetchScheduled = true;
            DDLogInfo(@"Scheduling presence fetch in 1.5 seconds");
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.5 * NSEC_PER_SEC),
                    dispatch_get_current_queue(),
                    ^{
                        [self doPendingPresencesRequest];
                    });
        }
    }
}

- (void)doPendingPresencesRequest {
    @synchronized (pendingPresenceFetchJids) {
        presenceFetchScheduled = false;
        [self requestPresencesForJidsImmediate:pendingPresenceFetchJids];
        [pendingPresenceFetchJids removeAllObjects];
    }
}

- (void)requestPresencesForJidsImmediate:(NSSet *)jids {
    // Remove any jids that got presences during the delay
    NSSet *filteredJids = [jids filteredSetUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        return ![HipChatApp.instance jidHasPresence:evaluatedObject];
    }]];
    if ([filteredJids count]) {
        XMPPStream *conn = HipChatApp.instance.conn;

        //<iq type="get" id="1">
        //  <query xmlns="http://hipchat.com/protocol/presence" />
        //</iq>
        NSXMLElement *query = [NSXMLElement elementWithName:@"query" xmlns:@"http://hipchat.com/protocol/presence"];

        presenceRequestId = [conn generateUUID];
        NSXMLElement *iq = [NSXMLElement elementWithName:@"iq"];
        [iq addAttributeWithName:@"type" stringValue:@"get"];
        [iq addAttributeWithName:@"id" stringValue:presenceRequestId];
        [iq addChild:query];

        for (NSString *jid in filteredJids) {
            [query addChild:[NSXMLElement elementWithName:@"jid" stringValue:jid]];
        }

        DDLogInfo(@"Requesting presences for %@", [[filteredJids allObjects] componentsJoinedByString:@", "]);
        [conn sendElement:iq];
    }
}

- (NSString *)storeName {
    return @"roster";
}

- (User *)userForBareJid:(NSString *)bareJid {
    return [self objectForKey:bareJid];
}

- (User *)userForJid:(XMPPJID *)jid {
    return [self objectForKey:jid.bare];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Private helper methods
///////////////////////////////////////////////////////////////////////

- (BOOL)hasRoster {
    return [self itemCount] > 0;
}

- (void)handlePresence:(XMPPPresence *)presence {
    // Ignore non-user presences (not from same domain as connection's user)
    // This makes it so the roster doesn't end up handling MUC presences
    if (![presence.from.domain isEqualToString:HipChatApp.instance.currentUser.jid.domain]) {
        return;
    }

    XMPPJID *from = presence.from;
    User *user = [self objectForKey:from.bare];
    if (user) {
        [user updateWithPresence:presence];
    } else {
        DDLogError(@"Received presence for jid not in roster: %@", from);
    }
}

- (User *)handleRosterItem:(NSXMLElement *)item withNotifications:(BOOL)sendNotifications {
    NSString *jidStr = [item attributeStringValueForName:@"jid"];
    XMPPJID *jid = [XMPPJID jidWithString:jidStr];
    User *user = [self objectForKey:jid.bare];
    if (user) {
        NSString *subscriptionType = [item attributeStringValueForName:@"subscription"];
        if ([subscriptionType isEqualToString:@"remove"]) {
            [self removeObjectForKey:jid.bare];
            if (sendNotifications) {
                [self postDelegateBlock:^{
                    [multicastDelegate roster:self removedUser:jid];
                }];
            }
        } else {
            [user updateWithRosterItem:item];
            // Kind of a hack, we use lastPresence to trigger roster updates
            // since that's what RosterEntry listens for
            user.lastPresence = user.lastPresence;
        }
    } else {
        user = [self getOrCreateObjectForKey:jid.bare];
        [user fillWithRosterItem:item];
        [self setObject:user forKey:user.jid.bare];
        if (sendNotifications) {
            [self postDelegateBlock:^{
                [multicastDelegate roster:self addedUser:user];
            }];
        }
    }
    return user;
}

- (BOOL)loadFromStore {
    if ([super loadFromStore]) {
        return YES;
    }
    return NO;
}

- (BOOL)triggerLoadedEventsForPresenceRequest {
    DDLogInfo(@"Received presence response - triggering presenceFinishedLoading event");
    presenceRequestId = nil;
    [self postDelegateBlock:^{
        [multicastDelegate presenceFinishedLoading:self];
    }];
    return YES;
}

- (void)processEarlyPresences {
    // Also set readyForPresences here and clear out early presencess
    // If our existing cached roster is up-to-date, we'll never get into the
    // "if (query)" check above (which is where early presences are normally cleared out)
    readyForPresence = YES;
    DDLogInfo(@"Going through %ld early presences", (long) [earlyPresences count]);
    for (XMPPPresence *presence in earlyPresences) {
        [self handlePresence:presence];
    }
    [earlyPresences removeAllObjects];
}

- (BOOL)triggerLoadedEventsForRosterRequest {
    if (![self hasRoster]) {
        DDLogInfo(@"Received roster IQ response but hasRoster is FALSE - loading from store...");
        [self loadFromStore];
    }

    if (![self hasRoster]) {
        DDLogInfo(@"hasRoster is FALSE - cannot trigger rosterFinishedLoading");
        return NO;
    }

    [self processEarlyPresences];

    DDLogInfo(@"Received roster response - triggering rosterFinishedLoading event");
    rosterRequestId = nil;
    [self postDelegateBlock:^{
        [multicastDelegate rosterFinishedLoading:self];
    }];
    return YES;
}

- (BOOL)triggerLoadedEventsForIqId:(NSString *)iqId {
    if ([iqId isEqualToString:presenceRequestId]) {
        return [self triggerLoadedEventsForPresenceRequest];
    } else if ([iqId isEqualToString:rosterRequestId]) {
        return [self triggerLoadedEventsForRosterRequest];
    }

    return NO;
}

@end

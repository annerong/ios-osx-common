//
//  Emoticon.m
//  hipchat
//
//  Created by Christopher Rivers on 8/22/12.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Emoticon.h"
#import "XMPPFramework.h"

@implementation Emoticon

- (void)fillWithJsonData:(NSDictionary *)jsonData {
    self.emoticon_id = [jsonData objectForKey:@"id"];
    self.path = [jsonData objectForKey:@"path"];
    self.shortcut = [jsonData objectForKey:@"shortcut"];
    self.width = [NSNumber numberWithInt:[[jsonData objectForKey:@"width"] intValue]];
    self.height = [NSNumber numberWithInt:[[jsonData objectForKey:@"height"] intValue]];
    self.audio_path = [jsonData objectForKey:@"audio_path"];
}

- (void)updateWithItem:(NSXMLElement *)item {
	NSString *shortcutStr =  [[item elementForName:@"shortcut"] stringValue];

	if (shortcutStr == nil)	{
		DDLogError(@"Emoticon: invalid item (missing or invalid shortcut): %@", item);
		return;
	}

    //<item>
    //    <id>138</id>
    //    <path>1/yes.png</path>
    //    <shortcut>yes</shortcut>
    //    <w>25</w>
    //    <h>25</h>
    //    <audio_path>/sound.mp3</audio_path>
    //</item>
    self.emoticon_id = [[item elementForName:@"id"] stringValue];
    self.path = [[item elementForName:@"path"] stringValue];
    self.shortcut = [[item elementForName:@"shortcut"] stringValue];
    self.width = [NSNumber numberWithInt:[[[item elementForName:@"w"] stringValue] intValue]];
    self.height = [NSNumber numberWithInt:[[[item elementForName:@"h"] stringValue] intValue]];
    self.audio_path = [[item elementForName:@"audio_path"] stringValue];
}

///////////////////////////////////////////////////////////////////////
#pragma mark Protocol / required implementation methods
///////////////////////////////////////////////////////////////////////

- (NSDictionary *)proxyForJson {
    return [NSDictionary dictionaryWithObjectsAndKeys:
            self.emoticon_id, @"id",
            self.path, @"path",
            self.shortcut, @"shortcut",
            self.width, @"width",
            self.height, @"height",
            self.audio_path, @"audio_path",
            nil];
}

- (NSString *)key {
    return self.shortcut;
}


@end

//
//  RoomList.m
//  HipChat
//
//  Created by Garret Heaton on 10/1/10.
//  Copyright 2010 HipChat, Inc. All rights reserved.
//

#import "HipChatApp.h"
#import "RoomList.h"
#import "Room.h"
#import "ThreadSafeDictionary.h"

@implementation RoomList

@synthesize discoId;
@synthesize xmppStream;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Initialization / Deinitialization
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (id)initWithStream:(XMPPStream *)stream {
    if ((self = [super init])) {
        _objects = [[ThreadSafeDictionary alloc] initWithQueueName:"com.hipchat.roomList"];

        isPopulating = NO;
        multicastDelegate = (GCDMulticastDelegate <RoomListDelegate> *) [[GCDMulticastDelegate alloc] init];

        xmppStream = stream;
        [xmppStream addDelegate:self delegateQueue:HipChatApp.hipchatQueue];
    }
    return self;
}

- (void)dealloc {
    [xmppStream removeDelegate:self];
}

- (NSString *)storeName {
    return @"roomList";
}

- (id <HCCachedObject>)generateObjectWithData:(NSDictionary *)data {
    Room *room = [self getOrCreateObjectForKey:[data objectForKey:@"jid"]];
    [room fillWithJsonData:data];
    return room;
}

- (Class)objectClass {
    return [Room class];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Delegation
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)addDelegate:(id)delegate delegateQueue:(dispatch_queue_t)delegateQueue {
    [self runBlock:^{
        [multicastDelegate addDelegate:delegate delegateQueue:delegateQueue];
    }];
}

- (void)removeDelegateImmediate:(id)delegate {
    [multicastDelegate removeDelegate:delegate];
}

- (void)removeDelegate:(id)delegate {
    [self runBlock:^{
        [self removeDelegateImmediate:delegate];
    }];
}

- (void)runBlock:(dispatch_block_t)block {
    if (dispatch_get_current_queue() == HipChatApp.hipchatQueue)
        block();
    else
        dispatch_async(HipChatApp.hipchatQueue, block);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Public functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (Room *)addRoomWithInvite:(XMPPMessage *)invite {
    DDLogInfo(@"Adding room from invite data: %@", invite);
    XMPPJID *fromJid = [XMPPJID jidWithString:[[invite attributeForName:@"from"] stringValue]];
    __block Room *room = nil;

    room = [self getOrCreateObjectForKey:fromJid.bare];
    [room updateWithInvite:invite];
    [self runBlock:^{
        [multicastDelegate roomList:self addedRoom:room];
    }];

    [self saveToStore];
    return room;
}

- (Room *)createNewRoomWithJid:(XMPPJID *)jid
                          name:(NSString *)name
                         topic:(NSString *)topic
                       privacy:(NSString *)privacy {
    DDLogInfo(@"Creating new room with jid %@ (name: %@)", jid, name);
    Room *room = nil;
    room = [self getOrCreateObjectForKey:jid.bare];
    [room updateWithJid:jid
                   name:name
                  topic:topic
                privacy:privacy];
    [self runBlock:^{
        [multicastDelegate roomList:self addedRoom:room];
    }];

    [self saveToStore];
    return room;
}

- (void)clearData {
    DDLogInfo(@"Clearing all rooms from RoomList.");
    [super clearData];

    [self runBlock:^{
        [multicastDelegate roomListCleared:self];
    }];
}

- (void)destroyRoom:(XMPPJID *)roomJid withReason:(NSString *)reason callback:(IQCallback)callback {
    Room *room = [self getOrCreateObjectForKey:roomJid.bare];
    IQCallback saveCallback = ^(XMPPIQ *iq) {
        if (![iq.type isEqualToString:@"error"]) {
            [self removeRoom:roomJid saveImmediately:YES];
        }
        if (callback) {
            callback(iq);
        }
    };
    [room destroyWithReason:reason callback:saveCallback];
}

- (void)fetchItems:(NSString *)mucHost {
    // See if we can load from local store
    // If we can, send a roomListLoaded event
    [self loadFromStore];
    if ([self itemCount] > 0) {
        [self runBlock:^{
            [multicastDelegate roomListLoaded:self];
        }];
    }

    // <iq type="get">
    //   <query xmlns="http://jabber.org/protocol/disco#items" ignore_archived="true" />
    // </iq>
    XMPPIQ *iq = [XMPPIQ iqWithType:@"get"
                                 to:[XMPPJID jidWithString:mucHost]
                          elementID:[xmppStream generateUUID]];
    NSXMLElement *query = [NSXMLElement elementWithName:@"query" xmlns:@"http://jabber.org/protocol/disco#items"];
    [query addAttributeWithName:@"ignore_archived" stringValue:@"true"];
    [iq addChild:query];

    [HipChatApp.instance sendIQ:iq callback:^(XMPPIQ *iq) {
        [self handleDisco:iq];
    }];
}

- (void)removeRoom:(XMPPJID *)roomJid {
    return [self removeRoom:roomJid saveImmediately:YES];
}

- (void)removeRoom:(XMPPJID *)roomJid saveImmediately:(BOOL)saveImmediately {
    DDLogInfo(@"Removing room from room list: %@", roomJid);
    Room *room = [self roomForJid:roomJid];
    if (room) {
        [self removeObjectForKey:roomJid.bare];
        [self runBlock:^{
            [multicastDelegate roomList:self deletedRoom:roomJid];
        }];
        if (saveImmediately) {
            [self saveToStore];
        }
    }
}

- (Room *)roomForJid:(XMPPJID *)jid {
    if (jid == nil) {
        return nil;
    }

    return [self objectForKey:jid.bare];
}

- (Room *)roomForName:(NSString *)name {
    // Search for a room with the given name
    // Used for room creation / renaming
    if (name == nil) {
        return nil;
    }

    NSSet *keys = [self keysOfEntriesPassingTest:^BOOL(id key, id obj, BOOL *stop) {
        if ([[obj name] caseInsensitiveCompare:name] == NSOrderedSame) {
            *stop = YES;
            return YES;
        }
        return NO;
    }];
    if (keys.count == 0) {
        return nil;
    }
    return [self objectForKey:[keys anyObject]];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Population functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)handleDiscoItem:(NSXMLElement *)item {
    NSString *jidStr = [item attributeStringValueForName:@"jid"];
    XMPPJID *jid = [XMPPJID jidWithString:jidStr];
    Room *room = [self getOrCreateObjectForKey:jid.bare];
    [room updateWithItem:item];
}

- (void)handleDisco:(XMPPIQ *)iq {
    NSXMLElement *query = [iq elementForName:@"query" xmlns:@"http://jabber.org/protocol/disco#items"];
    NSArray *items = [query elementsForName:@"item"];
    NSMutableDictionary *jidList = [[NSMutableDictionary alloc] initWithCapacity:[items count]];

    // Mark as populating (all items will be inserts instead of insert/update)
    // if we don't currently have any rooms cached
    if ([self itemCount] == 0) {
        isPopulating = YES;
    } else {
        isPopulating = NO;
    }

    // We're populating if we have no existing rooms in our store
    // This will default all items to be inserted (not updated) in handleDiscoItem
    for (NSXMLElement *item in items) {
        [jidList setObject:[NSNumber numberWithBool:YES] forKey:[item attributeStringValueForName:@"jid"]];
        [self handleDiscoItem:item];
    }

    // Check to make sure we received each room in our current list in the latest complete list
    NSMutableArray *roomsToRemove = [NSMutableArray array];
    for (Room *room in [[self getData] allValues]) {
        if ([jidList objectForKey:room.jid.bare] == nil) {
            [roomsToRemove addObject:room.jid];
        }
    }
    // Remove any rooms in our cache that weren't returned via the full fetch
    // They were probably deleted since the last time we checked in with the server
    if (roomsToRemove.count > 0) {
        DDLogInfo(@"Found %ld rooms to remove - calling removeRoom on each...", (long) roomsToRemove.count);
    }
    for (XMPPJID *jid in roomsToRemove) {
        [self removeRoom:jid saveImmediately:NO];
    }
    [roomsToRemove removeAllObjects];

    isPopulating = NO;

    [self saveToStore];
    [multicastDelegate roomListLoaded:self];
}

- (void)handlePush:(XMPPIQ *)iq {
    NSXMLElement *query = [iq elementForName:@"query" xmlns:@"http://hipchat.com/protocol/muc#room"];
    NSArray *items = [query elementsForName:@"item"];

    for (NSXMLElement *item in items) {
        if ([[item attributeStringValueForName:@"status"] isEqualToString:@"deleted"]) {
            NSString *jidStr = [item attributeStringValueForName:@"jid"];
            DDLogInfo(@"Recieved room delete push: %@", jidStr);
            XMPPJID *jid = [XMPPJID jidWithString:jidStr];
            [self removeRoom:jid saveImmediately:YES];
        } else {
            NSString *jidStr = [item attributeStringValueForName:@"jid"];
            DDLogInfo(@"Recieved room push: %@", jidStr);
            XMPPJID *jid = [XMPPJID jidWithString:jidStr];
            BOOL exists = [self objectForKey:jid.bare];
            [self handleDiscoItem:item];
            if (!exists) {
                DDLogInfo(@"Room did not exist before push - informing delegates of added room");
                Room *room = [self getOrCreateObjectForKey:jid.bare];
                [self runBlock:^{
                    [multicastDelegate roomList:self addedRoom:room];
                }];
            }
        }
    }
    [self saveToStore];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark XMPPStream delegates
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq {
    NSXMLElement *query = [iq elementForName:@"query" xmlns:@"http://hipchat.com/protocol/muc#room"];
    if (query) {
        [self handlePush:iq];
        return YES;
    }

    return NO;
}

@end

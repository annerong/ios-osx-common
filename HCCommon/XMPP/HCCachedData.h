//
//  HCCachedData.h
//  HipChat
//
//  Created by Christopher Rivers on 3/19/13.
//  Copyright (c) 2013 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "XMPPStream.h"

@protocol HCCachedObject;
@class ThreadSafeDictionary;

@interface HCCachedData : NSObject <XMPPStreamDelegate> {
    ThreadSafeDictionary *_objects;
}

- (void)clearData;

- (NSDictionary *)getData;

- (NSUInteger)itemCount;

- (id)objectForKey:(id)key;

- (void)setObject:(id)obj forKey:(id <NSCopying>)key;

- (void)removeObjectForKey:(id)key;

- (NSSet *)keysOfEntriesPassingTest:(BOOL (^)(id key, id obj, BOOL *stop))predicate;

- (BOOL)loadFromStore;

- (void)saveToStore;

// Any sublcasses must implement these methods
@property(readonly) NSString *storeName;

- (id <HCCachedObject>)generateObjectWithData:(NSDictionary *)data;

- (Class)objectClass;

- (id <HCCachedObject>)getOrCreateObjectForKey:(NSString *)key;

@end


@protocol HCCachedObject <NSObject>

@required

- (id)fillWithJsonData:(NSDictionary *)data;

- (NSString *)key;

@end

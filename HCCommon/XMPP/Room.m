//
//  Room.m
//  hipchat
//
//  Created by Christopher Rivers on 10/7/10.
//  Copyright 2010 HipChat. All rights reserved.
//

#import "Room.h"
#import "RoomOccupant.h"
#import "RoomList.h"
#import "Helpers.h"
#import "HipChatApp.h"
#import "HipChatUser.h"
#import "User.h"
#import "XMPP.h"
#import "XMPPIQ.h"

#import "DDLog.h"

@implementation Room

@synthesize admins;
@synthesize mentionRegex;
@synthesize occupants;

@dynamic gadgets;
@dynamic privacy;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Init
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (id)init {
    self = [super init];
    if (self) {
        self.historyLoaded = NO;
        listeningToConn = NO;
        membersToAdd = nil;
        self.rosterLoaded = NO;
        sendInvitesOnCreate = NO;
        self.needsRemoteCreation = NO;
    }
    return self;
}

- (void)fillWithJsonData:(NSDictionary *)jsonData {
    self.jid = [XMPPJID jidWithString:[jsonData objectForKey:@"jid"]];
    self.name = [jsonData objectForKey:@"name"];
    self.topic = [jsonData objectForKey:@"topic"];
    self.privacy = [jsonData objectForKey:@"privacy"];
    gadgets = [jsonData objectForKey:@"gadgets"];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Creation / update methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)updateWithInvite:(NSXMLElement *)invite {
    // Create new rooms with info in invite message:
	//	<message to="2_57@chat.mayo.hipchat.com" from="2_lowecase_sad_room@conf.mayo.hipchat.com">
	//		<x xmlns="http://jabber.org/protocol/muc#user">
	//			<invite from="2_2993@chat.mayo.hipchat.com/mac"/>
	//		</x>
	//		<x xmlns="http://hipchat.com/protocol/muc#room">
	//			<name>lowecase sad room</name>
	//			<privacy>public</privacy>
	//			<topic/>
	//		</x>
	//	</message>
    XMPPJID *fromJid = [XMPPJID jidWithString:[[invite attributeForName:@"from"] stringValue]];
    
	NSXMLElement *inviteNode = [[invite elementForName:@"x" xmlns:@"http://jabber.org/protocol/muc#user"] elementForName:@"invite"];
	if (inviteNode) {
        NSXMLElement *roomNode = [invite elementForName:@"x" xmlns:@"http://hipchat.com/protocol/muc#room"];
        self.jid = fromJid;
        self.name = [[roomNode elementForName:@"name"] stringValue];
        self.privacy = [[roomNode elementForName:@"privacy"] stringValue];
        self.topic = [[roomNode elementForName:@"topic"] stringValue];
    }
}

- (void)updateWithJid:(XMPPJID *)jid
             name:(NSString *)name
            topic:(NSString *)topic
          privacy:(NSString *)privacy {
    self.jid = jid;
    self.name = name;
    self.topic = topic;
    self.privacy = privacy;
    self.needsRemoteCreation = YES;
}

- (void)updateWithItem:(NSXMLElement *)item {
	NSString *jidString = [item attributeStringValueForName:@"jid"];
	XMPPJID *jid = [XMPPJID jidWithString:jidString];

	if (jid == nil)	{
		DDLogError(@"Room: invalid item (missing or invalid jid): %@", item);
		return;
	}

    // For a disco#items response:
    // <item jid="2_empty@conf.mayo.hipchat.com" name="Empty">
    //    <x xmlns="http://hipchat.com/protocol/muc#room">
    //      <id>1169</id>
    //      <topic>foo</topic>
    //      <privacy>private</privacy>
    //      <owner>2_2025@chat.mayo.hipchat.com</owner>
    //      <num_participants>0</num_participants>
    //      <guest_url></guest_url>
    //      <is_archived></is_archived>  // Only present for archived rooms
    //    </x>
    //  </item>
    //
    // For a room push, the data will be directly under the item node
    self.jid = jid;
    self.name = [[item attributeForName:@"name"] stringValue];

    // The item will have data nodes directly under it in a room push scenario
    // In a disco#items scenario, the data will be under an x extension node
    NSXMLElement *dataNode = ([item elementForName:@"id"] ?
                              item :
                              [item elementForName:@"x" xmlns:@"http://hipchat.com/protocol/muc#room"]);
    self.roomId = [[dataNode elementForName:@"id"] stringValue];
    self.topic = [[dataNode elementForName:@"topic"] stringValue];
    self.privacy = [[dataNode elementForName:@"privacy"] stringValue];
    self.guestUrl = [[dataNode elementForName:@"guest_url"] stringValue];
    self.numParticipants = [NSNumber numberWithInteger:[[[dataNode elementForName:@"num_participants"] stringValue] intValue]];
    self.isArchived = ([[dataNode elementsForName:@"is_archived"] count] > 0);
    self.owner = [XMPPJID jidWithString:[[dataNode elementForName:@"owner"] stringValue]];

    NSArray *gadgetNodes = [dataNode elementsForName:@"gadget"];
    gadgets = [NSMutableArray arrayWithCapacity:gadgetNodes.count];
    for (NSXMLElement *gadget in gadgetNodes) {
        [gadgets addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                            [gadget attributeStringValueForName:@"url"], @"url",
                            [gadget attributeStringValueForName:@"id"], @"id",
                            nil]];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Memory / Delegate methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)dealloc {
	HipChatApp *app = HipChatApp.instance;
	// Always remove the delegation on dealloc, regardless of whether we ever added it
	// (if we never did addDelegate, this will just do nothing)
	[app.conn removeDelegate:self];
    listeningToConn = NO;
}

- (void)addDelegate:(id)delegate {
    [self postDelegateBlock:^{
        [self.multicastDelegate addDelegate:delegate delegateQueue:dispatch_get_main_queue()];
    }];
}

- (void)removeDelegate:(id)delegate {
    // Always remove immediately to avoid bad references to expiring objects
    [multicastDelegate removeDelegate:delegate];
}

- (void)postDelegateBlock:(dispatch_block_t)block {
    if (dispatch_get_current_queue() == HipChatApp.hipchatQueue)
		block();
	else
		dispatch_async(HipChatApp.hipchatQueue, block);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Property implementations
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSArray *)gadgets {
    return gadgets;
}

- (NSString *)icon {
    return [Helpers getIconNameForRoom:self lightVersion:NO];
}

- (NSArray *)membersToAdd {
    return membersToAdd;
}

- (NSString *)mentionRegex {
    if ([self isPrivate]) {
        return mentionRegex;
    } else {
        if (guestMentionRegex && guestMentionRegex.length > 0) {
            // Append the guest mention names inside the overall mention regex
            NSString *globalRegex = [HipChatApp.instance mentionRegex];
            NSString *combinedRegex = [NSString stringWithFormat:@"%@|%@)", [globalRegex substringToIndex:globalRegex.length-1], guestMentionRegex];
            return combinedRegex;
        } else {
            return [HipChatApp.instance mentionRegex];
        }
    }
}

- (GCDMulticastDelegate *)multicastDelegate {
    if (multicastDelegate) {
        return multicastDelegate;
    }

    multicastDelegate = (GCDMulticastDelegate<RoomDelegate> *)[[GCDMulticastDelegate alloc] init];
    return multicastDelegate;
}

- (NSMutableDictionary *)occupants {
    if (occupants) {
        return occupants;
    }

    occupants = [[NSMutableDictionary alloc] initWithObjectsAndKeys:nil];
    return occupants;
}

- (void)setPrivacy:(NSString *)privacy {
    [self willChangeValueForKey:@"privacy"];
    if (!_privacy) {
        _privacy = privacy;
    } else {
        // If the room is changing to public, remove any unavailable occupants
        if ([privacy isEqualToString:@"public"] && ![privacy isEqualToString:_privacy]) { @synchronized(self.occupants) {
            DDLogInfo(@"Privacy change from privacy to public - removing unavailable users");
            NSMutableArray *occsToRemove = [NSMutableArray array];
            for (id key in self.occupants) {
                RoomOccupant *occ = [self.occupants objectForKey:key];
                if ([occ.lastPresence.type isEqualToString:@"unavailable"]) {
                    [occsToRemove addObject:key];
                }
            }
            for (id key in occsToRemove) {
                [self.occupants removeObjectForKey:key];
            }
        }}

        if (self.rosterLoaded) {
            // Send out a roster loaded event (as long as it's been loaded already)
            // This will trigger a refresh of the room roster
            [self postDelegateBlock:^{
                [self.multicastDelegate roomRosterLoaded:self];
            }];
        }

        [self updateMentionRegex];
    }
    _privacy = privacy;
    [self didChangeValueForKey:@"privacy"];
}

- (NSString *)privacy {
    return _privacy;
}

- (void)setTopic:(NSString *)topic {
    _topic = topic;
    [multicastDelegate room:self topicChanged:topic];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Functionality methods
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)changePrivacy:(NSString *)newPrivacy callback:(IQCallback)callback {
    XMPPIQ *iq = [XMPPIQ iqWithType:@"set" to:self.jid elementID:[XMPPStream generateUUID]];
    NSXMLElement *queryNode = [NSXMLElement elementWithName:@"query" xmlns:@"http://hipchat.com"];
    NSXMLElement *privacyNode = [NSXMLElement elementWithName:@"privacy" stringValue:newPrivacy];
    [queryNode addChild:privacyNode];
    [iq addChild:queryNode];

	HipChatApp *app = HipChatApp.instance;
    IQCallback saveCallback = ^(XMPPIQ *iq) {
        if (![iq.type isEqualToString:@"error"]) {
            self.privacy = newPrivacy;
        }
        if (callback) {
            callback(iq);
        }
    };
    [app sendIQ:iq callback:saveCallback];
}

- (void)destroyWithReason:(NSString *)reason callback:(IQCallback)callback {
    DDLogInfo(@"Destroying room %@ -- Reason: %@", self.jid, reason);
    XMPPIQ *iq = [XMPPIQ iqWithType:@"set" to:self.jid elementID:[XMPPStream generateUUID]];
    NSXMLElement *queryNode = [NSXMLElement elementWithName:@"query" xmlns:@"http://jabber.org/protocol/muc#owner"];
    NSXMLElement *destroyNode = [NSXMLElement elementWithName:@"destroy"];
    if (reason) {
        [destroyNode addChild:[NSXMLElement elementWithName:@"reason" stringValue:reason]];
    }
    [queryNode addChild:destroyNode];
    [iq addChild:queryNode];

    HipChatApp *app = HipChatApp.instance;
    [app sendIQ:iq callback:callback];
}

- (void)fetchMembersWithCallback:(IQCallback)callback {
    DDLogInfo(@"Fetching member list for room %@", self.jid);
    XMPPIQ *iq = [XMPPIQ iqWithType:@"set" to:self.jid elementID:[XMPPStream generateUUID]];
    NSXMLElement *queryNode = [NSXMLElement elementWithName:@"query" xmlns:@"http://jabber.org/protocol/disco#items"];
    [iq addChild:queryNode];

	HipChatApp *app = HipChatApp.instance;
    [app sendIQ:iq callback:callback];
}

- (NSXMLElement *)generateCreationExtension {
    NSXMLElement *hcExt = [NSXMLElement elementWithName:@"x" xmlns:@"http://hipchat.com/protocol/muc#room"];
    [hcExt addChild:[NSXMLElement elementWithName:@"name" stringValue:self.name]];
    [hcExt addChild:[NSXMLElement elementWithName:@"topic" stringValue:self.topic]];
    [hcExt addChild:[NSXMLElement elementWithName:@"send_invites" stringValue:(sendInvitesOnCreate ? @"1" : @"0")]];
    [hcExt addChild:[NSXMLElement elementWithName:@"privacy" stringValue:self.privacy]];
    if (membersToAdd) {
        NSXMLElement *members = [NSXMLElement elementWithName:@"members"];
        for (XMPPJID *jid in membersToAdd) {
            [members addChild:[NSXMLElement elementWithName:@"jid" stringValue:jid.bare]];
        }
        [hcExt addChild:members];
    }
    return hcExt;
}

- (RoomOccupant *)getOccupant:(XMPPJID *)occJid {
	RoomOccupant *occ = [self.occupants objectForKey:[occJid bare]];
	return occ;
}

- (void)handlePresence:(XMPPPresence *)presence {
	XMPPJID *from = [XMPPJID jidWithString:[[presence attributeForName:@"from"] stringValue]];
	if (![self.jid.bare isEqualToString:from.bare]) {
		// Ignore anything that's not for this room
		return;
	}

	NSXMLElement *userNode = [presence elementForName:@"x" xmlns:@"http://jabber.org/protocol/muc#user"];
	if (!userNode) {
		return;
	}

    HipChatApp *app = [HipChatApp instance];

    NSXMLElement *item = [userNode elementForName:@"item"];
    BOOL isGuest = ([[item attributeStringValueForName:@"role"] isEqualToString:VISITOR_ROLE]);

	XMPPJID *userJid = [XMPPJID jidWithString:[[item attributeForName:@"jid"] stringValue]];
    NSString *statusCode = [[userNode elementForName:@"status"] attributeStringValueForName:@"code"];
    BOOL isKick = [statusCode isEqualToString:@"307"];
    if (isKick && [userJid.bare isEqualToString:app.currentUser.jid.bare]) {
        // Syncing close tab
        XMPPJID *actor = [XMPPJID jidWithString:[[item elementForName:@"actor"] attributeStringValueForName:@"jid"]];
        if ([[actor bare] isEqualToString:[app.currentUser jid].bare]) {
            DDLogInfo(@"Syncing room leave from another session. JID: %@", self.jid);
        } else {
            [app showFlash:[NSString stringWithFormat:@"You have been removed from the room '%@'", self.name]];
        }
        [app closeChat:self.jid];
        return;
    }

    if ([userNode elementForName:@"destroy"]) {
        HipChatApp *app = [HipChatApp instance];
        [app.roomList removeRoom:self.jid];
        return;
    }

	RoomOccupant *occupant = [self getOccupant:userJid];
    @synchronized(self.occupants) {
        // Only private rooms show users as "offline"
        // Never show kicked users or guests as offline
        BOOL isUnavailable = [presence.type isEqualToString:@"unavailable"];
        BOOL showAsUnavailable = !(self.isPublic || isKick || isGuest);
        if (occupant) {
            if (isUnavailable && !showAsUnavailable) {
                [self.occupants removeObjectForKey:[userJid bare]];
                if (self.rosterLoaded) {
                    [self postDelegateBlock:^{
                        [self.multicastDelegate room:self occupantRemoved:userJid];
                    }];
                    [self updateMentionRegex];
                }
            } else {
                [occupant updateWithPresence:presence];
            }
        } else {
            // Add an occupant to the room if we don't have one in the occupant dictionary yet
            // Only show non-offline users, or users that we want to show up as offline
            if (!isUnavailable || (isUnavailable && showAsUnavailable)) {
                [self.occupants setObject:[RoomOccupant createWithPresence:presence] forKey:[userJid bare]];
                if (self.rosterLoaded) {
                    [self postDelegateBlock:^{
                        [self.multicastDelegate room:self occupantAdded:userJid];
                    }];
                    [self updateMentionRegex];
                }
            }
        }
    }

	if ([statusCode isEqualToString:@"110"]) {
		if (self.rosterLoaded) {
            DDLogInfo(@"Room history loaded for %@", self.jid);
			self.historyLoaded = YES;
            [self postDelegateBlock:^{
                [self.multicastDelegate roomHistoryLoaded:self];
            }];
		} else {
            DDLogInfo(@"Room roster loaded for %@", self.jid);
			self.rosterLoaded = YES;
            self.needsRemoteCreation = NO;
            [self postDelegateBlock:^{
                [self.multicastDelegate roomRosterLoaded:self];
            }];
            [self updateMentionRegex];
		}
	}
}

- (void)invite:(XMPPJID *)jid reason:(NSString *)reason {
    [self inviteMultiple:[NSArray arrayWithObject:jid] reason:reason];
}

- (void)inviteMultiple:(NSArray *)jids reason:(NSString *)reason {
    XMPPMessage *message = [XMPPMessage message];
    [message addAttributeWithName:@"to" stringValue:self.jid.bare];
    [message addAttributeWithName:@"id" stringValue:[XMPPStream generateUUID]];
    NSXMLElement *mucUserNode = [NSXMLElement elementWithName:@"x" xmlns:@"http://jabber.org/protocol/muc#user"];
    for (XMPPJID *jid in jids) {
        if (![Helpers isUserJid:jid]) {
            continue;
        }
        NSXMLElement *inviteNode = [NSXMLElement elementWithName:@"invite"];
        [inviteNode addAttributeWithName:@"to" stringValue:jid.bare];
        if (reason && reason.length > 0) {
            NSXMLElement *reasonNode = [NSXMLElement elementWithName:@"reason" stringValue:reason];
            [inviteNode addChild:reasonNode];
        }
        [mucUserNode addChild:inviteNode];
    }
    [message addChild:mucUserNode];

    HipChatApp *app = HipChatApp.instance;
    [app.conn sendElement:message];
}

- (BOOL)isAdmin:(XMPPJID *)jid {
    // Check owner first
    if ([jid.bare isEqualToString:self.owner.bare]) {
        return YES;
    }

    // Check occupant affiliation
    RoomOccupant *occ = [self getOccupant:jid];
    if (!occ) {
        return NO;
    }
    return ([occ.affiliation isEqualToString:OWNER_AFFILIATION] ||
            [occ.affiliation isEqualToString:ADMIN_AFFILIATION]);
}

- (BOOL)isPrivate {
	return ([self.privacy isEqualToString:@"private"] || [self.privacy isEqualToString:@"secret"]);
}

- (BOOL)isPublic {
	return ([self.privacy isEqualToString:@"public"]);
}

- (void)join {
	[self joinWithHistoryCount:20];
}

- (void)joinWithHistoryCount:(NSInteger)count {
	self.rosterLoaded = NO;
	self.historyLoaded = NO;

	HipChatApp *app = HipChatApp.instance;
    // Remove any previous listener, just in case the connection object has changed.
    // Then, (re)start listening to the connection for presence/iq updates
    if (listeningToConn) {
        [self stopListeningToConnection];
    }
    [self listenToConnection];

	XMPPJID *nicknameJid = [XMPPJID jidWithString:self.jid.bare resource:app.currentUser.name];

    NSMutableArray *elements = [NSMutableArray array];
	NSXMLElement *x = [NSXMLElement elementWithName:@"x" xmlns:@"http://jabber.org/protocol/muc"];
	NSXMLElement *history = [NSXMLElement elementWithName:@"history"];
	[history addAttributeWithName:@"maxstanzas" stringValue:[NSString stringWithFormat:@"%ld", (long)count]];
	[x addChild:history];
    [elements addObject:x];
    if (self.needsRemoteCreation) {
        [elements addObject:[self generateCreationExtension]];
    }

	NSDictionary *args = [NSDictionary dictionaryWithObjectsAndKeys:
                          [NSDictionary dictionaryWithObjectsAndKeys:@"available", @"type", nicknameJid.full, @"to", nil], @"attributes",
                          elements, @"elements",
						  nil];
	[app sendPresenceWithArgs:args];
}

//<iq from='fluellen@shakespeare.lit/pda' id='kick1' to='harfleur@chat.shakespeare.lit' type='set'>
//  <query xmlns='http://jabber.org/protocol/muc#admin'>
//      <item nick='pistol' role='none'>
//          <reason>Avaunt, you cullion!</reason>
//      </item>
//  </query>
//</iq>
- (void)kickUser:(XMPPJID *)jid callback:(IQCallback)callback {
    [self kickMultipleUsers:[NSArray arrayWithObject:jid] callback:callback];
}

- (void)kickMultipleUsers:(NSArray *)jids callback:(IQCallback)callback {
    XMPPIQ *iq = [XMPPIQ iqWithType:@"set" to:self.jid elementID:[XMPPStream generateUUID]];
    NSXMLElement *queryNode = [NSXMLElement elementWithName:@"query" xmlns:@"http://hipchat.com"];
    for (XMPPJID *jid in jids) {
        NSXMLElement *itemNode = [NSXMLElement elementWithName:@"item"];
        [itemNode addAttributeWithName:@"jid" stringValue:jid.bare];
        [itemNode addAttributeWithName:@"role" stringValue:@"none"];
        [queryNode addChild:itemNode];
    }
    [iq addChild:queryNode];

	HipChatApp *app = HipChatApp.instance;
    [app sendIQ:iq callback:callback];
}

- (void)leave {
    [self stopListeningToConnection];
    if (!self.rosterLoaded) {
        return;
    }
    self.rosterLoaded = NO;
    self.historyLoaded = NO;

    HipChatApp *app = HipChatApp.instance;
	XMPPJID *roomJid = [XMPPJID jidWithString:self.jid.bare resource:app.currentUser.name];
	NSDictionary *args = [NSDictionary dictionaryWithObjectsAndKeys:
                          [NSDictionary dictionaryWithObjectsAndKeys:@"unavailable", @"type", roomJid.full, @"to", nil], @"attributes",
						  nil];
	[app sendPresenceWithArgs:args];
}

- (void)listenToConnection {
    HipChatApp *app = HipChatApp.instance;
	[app.conn addDelegate:self delegateQueue:HipChatApp.hipchatQueue];
    listeningToConn = YES;
}

//<iq from='crone1@shakespeare.lit/desktop' id='member2' to='coven@chat.shakespeare.lit' type='set'>
//  <query xmlns='http://jabber.org/protocol/muc#admin'>
//      <item affiliation='none' jid='hag66@shakespeare.lit'>
//          <reason>Not so worthy after all!</reason>
//      </item>
//  </query>
//</iq>
- (void)removeUser:(XMPPJID *)jid callback:(IQCallback)callback {
    [self removeMultipleUsers:[NSArray arrayWithObject:jid] callback:callback];
}

- (void)removeMultipleUsers:(NSArray *)jids callback:(IQCallback)callback {
    XMPPIQ *iq = [XMPPIQ iqWithType:@"set" to:self.jid elementID:[XMPPStream generateUUID]];
    NSXMLElement *queryNode = [NSXMLElement elementWithName:@"query" xmlns:@"http://hipchat.com"];
    for (XMPPJID *jid in jids) {
        NSXMLElement *itemNode = [NSXMLElement elementWithName:@"item"];
        [itemNode addAttributeWithName:@"jid" stringValue:jid.bare];
        [itemNode addAttributeWithName:@"affiliation" stringValue:@"none"];
        [queryNode addChild:itemNode];
    }
    [iq addChild:queryNode];

	HipChatApp *app = HipChatApp.instance;
    [app sendIQ:iq callback:callback];
}

- (void)rename:(NSString *)newName callback:(IQCallback)callback {
    XMPPIQ *iq = [XMPPIQ iqWithType:@"set" to:self.jid elementID:[XMPPStream generateUUID]];
    NSXMLElement *queryNode = [NSXMLElement elementWithName:@"query" xmlns:@"http://hipchat.com"];
    NSXMLElement *renameNode = [NSXMLElement elementWithName:@"rename" stringValue:newName];
    [queryNode addChild:renameNode];
    [iq addChild:queryNode];

	HipChatApp *app = HipChatApp.instance;
    IQCallback saveCallback = ^(XMPPIQ *iq) {
        if (![iq.type isEqualToString:@"error"]) {
            self.name = newName;
        }
        if (callback) {
            callback(iq);
        }
    };
    [app sendIQ:iq callback:saveCallback];
}

- (void)setArchived:(BOOL)archived callback:(IQCallback)callback {
    DDLogInfo(@"Setting room archived status for room %@ to: %@", self.jid, (archived ? @"archived" : @"unarchived"));
    XMPPIQ *iq = [XMPPIQ iqWithType:@"set" to:self.jid elementID:[XMPPStream generateUUID]];
    NSXMLElement *queryNode = [NSXMLElement elementWithName:@"query" xmlns:@"http://hipchat.com"];
    NSXMLElement *archiveNode = [NSXMLElement elementWithName:@"is_archived" stringValue:(archived ? @"1" : @"0")];
    [queryNode addChild:archiveNode];
    [iq addChild:queryNode];

	HipChatApp *app = HipChatApp.instance;
    IQCallback saveCallback = ^(XMPPIQ *iq) {
        if (![iq.type isEqualToString:@"error"]) {
            self.isArchived = archived;
        }
        if (callback) {
            callback(iq);
        }
    };
    [app sendIQ:iq callback:saveCallback];
}

- (void)setGuestAccess:(BOOL)enabled callback:(IQCallback)callback {
    DDLogInfo(@"Setting guest access status for room %@ to: %@", self.jid, (enabled ? @"enabled" : @"disabled"));
    XMPPIQ *iq = [XMPPIQ iqWithType:@"set" to:self.jid elementID:[XMPPStream generateUUID]];
    NSXMLElement *queryNode = [NSXMLElement elementWithName:@"query" xmlns:@"http://hipchat.com"];
    NSXMLElement *guestAccessNode = [NSXMLElement elementWithName:@"guest_access" stringValue:(enabled ? @"1" : @"0")];
    [queryNode addChild:guestAccessNode];
    [iq addChild:queryNode];

	HipChatApp *app = HipChatApp.instance;
    IQCallback saveCallback = ^(XMPPIQ *iq) {
        if ([iq.type isEqualToString:@"error"]) {
            // Just set the guest URL to what it was before so we trigger a KVO notif
            self.guestUrl = self.guestUrl;
        }
        if (callback) {
            callback(iq);
        }
    };
    [app sendIQ:iq callback:saveCallback];
}

- (void)setMembersToAdd:(NSArray *)members sendInvites:(BOOL)sendInvites {
    membersToAdd = members;
    sendInvitesOnCreate = sendInvites;
}

- (void)setNewTopic:(NSString *)newTopic {
    HipChatApp *app = HipChatApp.instance;

    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
	[message addAttributeWithName:@"to" stringValue:self.jid.bare];
	[message addAttributeWithName:@"id" stringValue:[app.conn generateUUID]];
    [message addAttributeWithName:@"type" stringValue:@"groupchat"];
    [message addAttributeWithName:@"from" stringValue:app.currentUser.nicknameJid.full];
	NSXMLElement *subject = [NSXMLElement elementWithName:@"subject" stringValue:newTopic];
	[message addChild:subject];

    [app.conn sendElement:message];
}

- (void)stopListeningToConnection {
    HipChatApp *app = [HipChatApp instance];
    [app.conn removeDelegate:self];
    listeningToConn = NO;
}

- (void)updateMentionRegex {
    if (!self.rosterLoaded) {
        return;
    }

    if (self.isPublic) {
        NSMutableArray *guestMentions = [NSMutableArray array];
        for (id key in self.occupants) {
            RoomOccupant *occ = [self.occupants objectForKey:key];
            if ([occ.role isEqualToString:VISITOR_ROLE] && occ.regexMentionName) {
                [guestMentions addObject:occ.regexMentionName];
            }
        }
        if (guestMentions.count == 0) {
             return;
        }
        guestMentionRegex = [NSString stringWithFormat:@"%@", [guestMentions componentsJoinedByString:@"|"]];
        return;
    }

    mentionRegex = @"";
    NSMutableArray *mentionNames = [NSMutableArray array];
    NSDictionary *users = [HipChatApp.instance getAllUsers];
    for (id key in self.occupants) {
        RoomOccupant *occ = [self.occupants objectForKey:key];
        User *user = [users objectForKey:key];
        if (occ && occ.mentionName) {
            [mentionNames addObject:occ.regexMentionName];
        } else if (user && user.mentionName) {
            [mentionNames addObject:user.regexMentionName];
        }
    }
    mentionRegex = [NSString stringWithFormat:@"@(%@)", [mentionNames componentsJoinedByString:@"|"]];
    DDLogInfo(@"Mention regex for %@: %@", self.jid, mentionRegex);
}

///////////////////////////////////////////////////////////////////////
#pragma mark XMPPStream Delegate Methods
///////////////////////////////////////////////////////////////////////

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error {
    self.rosterLoaded = NO;
    self.historyLoaded = NO;
    // Remove delegation when we get disconnected
    // We'll reconnect it during the join function
    // (assuming something causes us to rejoin after connection)
    [self stopListeningToConnection];
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence {
    [self handlePresence:presence];
}

///////////////////////////////////////////////////////////////////////
#pragma mark Protocol / required implementation methods
///////////////////////////////////////////////////////////////////////

- (NSDictionary *)proxyForJson {
    return [NSDictionary dictionaryWithObjectsAndKeys:
            self.jid.bare, @"jid",
            self.name, @"name",
            self.topic, @"topic",
            self.privacy, @"privacy",
            self.gadgets, @"gadgets",
            nil];
}

- (NSString *)key {
    return self.jid.bare;
}

@end

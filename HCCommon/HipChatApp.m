//
//  HipChatApp.m
//  hipchat
//
//  Created by Christopher Rivers on 9/22/10.
//  Copyright 2010 HipChat. All rights reserved.
//

#import "HipChatApp.h"

#import "ChatStateMessage.h"
#import "EmoticonList.h"
#import "Helpers.h"
#import "HipChatUser.h"
#import "Linkify.h"
#import "Profile.h"
#import "RegexKitLite.h"
#import "RoomList.h"
#import "Room.h"
#import "Roster.h"
#import "User.h"

#import "XMPPFramework.h"
#import "HipChatXMPPAuthentication.h"
#import "ApiURLConnectionDelegate.h"
#import "ThreadSafeSet.h"

static NSString *HipChatErrorDomain = @"HipChatErrorDomain";

static const NSInteger DEFAULT_REQUEST_TIMEOUT = 5;
static const NSInteger MAX_API_RETRY_COUNT = 3;

@interface HipChatApp () {
    ThreadSafeSet *jidsWithPresences;
    ThreadSafeSet *jidsPendingPresence;
}
@end

///////////////////////////////////////////////////////////////////////
#pragma mark -
///////////////////////////////////////////////////////////////////////

@implementation HipChatApp

@synthesize apiHost;
@synthesize chatHost;
@synthesize conn;
@synthesize currentJid;
@synthesize currentUser;
@synthesize deviceToken;
@synthesize doReconnect;
@synthesize emoticonList;
@synthesize isAutoJoinFinished;
@synthesize isConnecting;
@synthesize isEmoticonListLoaded;
@synthesize isFullyLoaded;
@synthesize isManualDisconnect;
@synthesize isRoomListLoaded;
@synthesize isRosterLoaded;
@synthesize isPresenceLoaded;
@synthesize lobbyJid;
@synthesize loginEmail = loginEmail;
@synthesize loginPassword = loginPassword;
@synthesize mucHost;
@synthesize mentionRegex;
@synthesize nameRegex;
@synthesize ping;
@synthesize roomList;
@synthesize roster;
@synthesize searchJid;
@synthesize webHost;

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Singleton methods
///////////////////////////////////////////////////////////////////////

static HipChatApp *sharedInstance = nil;
static dispatch_queue_t _hipchatQueue;

+ (dispatch_queue_t)hipchatQueue {
    if (!_hipchatQueue) {
        _hipchatQueue = dispatch_queue_create("com.hipchat.primaryQueue", NULL);
    }
    return _hipchatQueue;
}

+ (HipChatApp *)instance {
    if (sharedInstance) {
        return sharedInstance;
    } else {
        @throw [NSException exceptionWithName:@"HipChatException"
                                       reason:@"HipChatApp must be initialized with a subclass instance"
                                     userInfo:nil];
    }

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunreachable-code"
    return nil;
#pragma clang diagnostic pop
}

+ (HipChatApp *)getSharedInstance {
    return sharedInstance;
}

+ (void)setSharedInstance:(HipChatApp *)instance {
    sharedInstance = instance;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Thread safe collection accessors
///////////////////////////////////////////////////////////////////////
    
- (NSDictionary *)activeChats {
    return [activeChats immutableCopy];
}

- (NSArray *)activeChatJids {
    return [activeChatJids immutableCopy];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Delegation
///////////////////////////////////////////////////////////////////////

- (void)addDelegate:(id)delegate {
    [self postDelegateBlock:^{
        [multicastDelegate addDelegate:delegate delegateQueue:dispatch_get_main_queue()];
    }];
}

- (void)removeDelegateImmediate:(id)delegate {
    [multicastDelegate removeDelegate:delegate];
}

- (void)removeDelegate:(id)delegate {
    [self postDelegateBlock:^{
        [self removeDelegateImmediate:delegate];
    }];
}

- (void)postDelegateBlock:(dispatch_block_t)block {
    if (dispatch_get_current_queue() == HipChatApp.hipchatQueue)
        block();
    else
        dispatch_async(HipChatApp.hipchatQueue, block);
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Initialization / De-Initialization
///////////////////////////////////////////////////////////////////////

- (HipChatApp *)init {
    if (self = [super init]) {
        doAutoJoin = YES;
        isFirstConnect = YES;
        authTokenRequested = NO;

        // Dictionaries for lookup
        abbreviatedNames = [[ThreadSafeDictionary alloc] initWithQueueName:"com.hipchat.abbreviatedNames"];
        activeChats = [[ThreadSafeDictionary alloc] initWithQueueName:"com.hipchat.activeChats"];
        activeChatJids = [[ThreadSafeArray alloc] initWithQueueName:"com.hipchat.activeChatJids"];
        appPerms = [[NSMutableDictionary alloc] init];
        apiErrorRequestQueue = [[NSMutableArray alloc] init];
        apiRequestQueue = [[NSOperationQueue alloc] init];
        authTokenRequestQueue = [[NSMutableArray alloc] init];
        autoJoinList = [[ThreadSafeArray alloc] initWithQueueName:"com.hipchat.autoJoinList"];
        displayNames = [[ThreadSafeDictionary alloc] initWithQueueName:"com.hipchat.displayNames"];
        iqCallbacks = [[NSMutableDictionary alloc] init];
        nicknameMap = [[ThreadSafeDictionary alloc] initWithQueueName:"com.hipchat.nicknameMap"];
        profiles = [[ThreadSafeDictionary alloc] initWithQueueName:"com.hipchat.profiles"];
        unreadChats = [[ThreadSafeDictionary alloc] initWithQueueName:"com.hipchat.unreadChats"];
        unreadMessageCounts = [[ThreadSafeDictionary alloc] initWithQueueName:"com.hipchat.unreadMessageCounts"];

        // XMPP-related objects
        conn = [[XMPPStream alloc] init];
        conn.keepAliveInterval = 90;

        ping = [[XMPPPing alloc] init];
        [ping activate:conn];

        emoticonList = [[EmoticonList alloc] init];
        roomList = [[RoomList alloc] initWithStream:conn];
        roster = [[Roster alloc] initWithStream:conn];

#if !TARGET_OS_IPHONE
        reconnector = [[XMPPReconnect alloc] init];
        [reconnector addDelegate:self delegateQueue:HipChatApp.hipchatQueue];
        [reconnector activate:conn];
#endif
        [conn addDelegate:self delegateQueue:HipChatApp.hipchatQueue];
        [roster addDelegate:self delegateQueue:HipChatApp.hipchatQueue];
        [roomList addDelegate:self delegateQueue:HipChatApp.hipchatQueue];
        [ping addDelegate:self delegateQueue:HipChatApp.hipchatQueue];
        [emoticonList addDelegate:self];

        jidsWithPresences = [[ThreadSafeSet alloc] initWithQueueName:"com.hipchat.jidsWithPresences"];
        jidsPendingPresence = [[ThreadSafeSet alloc] initWithQueueName:"com.hipchat.jidsPendingPresence"];

        multicastDelegate = (GCDMulticastDelegate <HipChatAppDelegate> *) [[GCDMulticastDelegate alloc] init];
    }

    return self;
}

- (void)dealloc {
    [conn removeDelegate:self];
    [emoticonList removeDelegate:self];
    [roomList removeDelegate:self];
    [roster removeDelegate:self];
    [ping removeDelegate:self];

    [ping deactivate];
    [conn disconnect];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Connection / Disconnection
///////////////////////////////////////////////////////////////////////

// Synchronize to avoid active chats being iterated and modified at the same time
- (void)clearUserData {
    DDLogInfo(@"Clearing all user-specific data");

    // Remove cookies used for authentication in web (chat) views
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in cookieStorage.cookies) {
        if (cookie.isSessionOnly
                || [self.webHost rangeOfString:cookie.domain].location != NSNotFound
                || [cookie.name isEqualToString:@"auth-token"]
                || [cookie.name isEqualToString:@"auth-uid"]) {
            [cookieStorage deleteCookie:cookie];
        }
    }

    // Release all the user-specific / room-specific data that we currently hold on disconnect
    // Mark users/rooms as not loaded so we don't trigger updates on the table when clearing them out
    [roster clearData];
    [roomList clearData];
    [emoticonList clearData];

    [profiles removeAllObjects];

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs removeObjectForKey:@"emoticonsVersion"];
    [prefs removeObjectForKey:@"rosterVersion"];
    [prefs removeObjectForKey:@"jidToFocus"];
    [prefs synchronize];

    [autoJoinList removeAllObjects];
    
    NSDictionary *activeChatsCopy = [activeChats immutableCopy];
    for (id key in activeChatsCopy) {
        [[activeChatsCopy objectForKey:key] performSelector:@selector(close)];
    }
    [activeChats removeAllObjects];
    [activeChatJids removeAllObjects];

    [self clearUnreadCount:nil];

    currentUser = nil;

    authToken = nil;
}

- (void)connect {
    isConnecting = YES;

    DDLogInfo(@"User information set - initiating connection");

    doAutoJoin = YES;
    isManualDisconnect = NO;

    NSError *err = nil;
#if TARGET_OS_IPHONE
    // Initiate the TLS connection directly (port 5223):
    BOOL success = [conn oldSchoolSecureConnect:&err];
#else
    // Regular connection (TLS upgrade later)
    BOOL success = [conn connect:&err];
#endif

    if (!success) {
        // Don't trigger sendConnectFailed here - this can happen if we some how end up calling connect
        // during an existing connection flow (e.g. closing and reopening the app during connect)
        isConnecting = NO;
        if (err) {
            DDLogError(@"Error occurred during connect (code %ld) - %@", (long) [err code], [err localizedDescription]);
        } else {
            DDLogError(@"Unknown error occurred during connect");
        }
    } else {
        DDLogInfo(@"Connect returned success - waiting for response...");
    }
}

- (void)connectWithEmail:(NSString *)email password:(NSString *)password {
    isConnecting = YES;

    // Checking that these are the same POINTER here, not the same string value
    // Don't release/retain if they're actually the same exact string pointer already
    if (loginEmail != email) {
        loginEmail = email;
    }
    if (loginPassword != password) {
        loginPassword = password;
    }

    [[NSUserDefaults standardUserDefaults] setObject:loginEmail forKey:@"account_email"];

    DDLogInfo(@"Connecting with email %@", loginEmail);
    [self connect];
}

- (void)disconnect {
    DDLogInfo(@"Disconnecting...");

    if ([conn isConnected]) {
        [conn disconnect];
    }

    [self onDisconnect];
}

- (void)handleErrorStanza:(NSXMLElement *)stanza {
    DDLogInfo(@"Handling stanza error...");
    // Show a flash message with the value of the text node under the error
    // The text node is intended to be a human-readable string describing the error
    NSXMLElement *errorNode = [stanza elementForName:@"error"];
    NSString *errorText = [Helpers getTextForError:errorNode];

    if (errorText) {
        [self showFlash:errorText];
    }

    // We assume that if we ever get a "cancel" stanza, the chat sending that stanza should be closed
    if ([stanza attributeStringValueForName:@"from"] &&
            [[errorNode attributeStringValueForName:@"type"] isEqualToString:@"cancel"]) {
        XMPPJID *from = [XMPPJID jidWithString:[stanza attributeStringValueForName:@"from"]];
        DDLogError(@"Received 'cancel' stanza from %@ - closing chat...", from);
        [self closeChat:from isManual:NO];
    }
}

- (void)handleXMPPError:(NSXMLElement *)error {
    NSString *errorType = [[error childAtIndex:0] name];
    DDLogError(@"Got XMPP Error: %@", errorType);

    NSDictionary *errorData = [Helpers getErrorDataForError:error];
    if (errorData) {
        [self sendConnectFailed:[errorData objectForKey:@"message"] code:[[errorData objectForKey:@"code"] integerValue]];
    }
}

- (BOOL)reconnect { @synchronized(conn) {
    // Don't try to reconnect if we're not the current active app
    if ([conn isConnected] || isConnecting) {
        DDLogInfo(@"Connection is already connected or in the process of connecting. Don't need to reconnect");
        return NO;
    }

    if (conn && currentUser) {
        DDLogInfo(@"Reconnecting with existing user data...");
        [self connect];
        return YES;
    } else if ([loginEmail length] > 0 && [loginPassword length] > 0) {
        DDLogInfo(@"Reconnecting using stored email/password");
        [self connectWithEmail:loginEmail password:loginPassword];
        return YES;
    }

    DDLogError(@"Could not reconnect. No user data or email/password available.");
    return NO;
}}

- (BOOL)secureConnection:(NSError **)errPtr {
    if (!conn) {
        NSString *errMsg = @"Connection has not yet been initialized";
        NSDictionary *info = [NSDictionary dictionaryWithObject:errMsg forKey:NSLocalizedDescriptionKey];

        if (errPtr) {
            *errPtr = [NSError errorWithDomain:XMPPStreamErrorDomain code:XMPPStreamInvalidState userInfo:info];
        }
        return NO;
    }

    BOOL success = [conn secureConnection:errPtr];

    return success;
}

- (NSError *)sendConnectFailed:(NSString *)errMsg code:(NSInteger)code {
    // We return lastConnectError here so that we can maintain proper error messaging
    // between two different reconnection attempts. Otherwise, the initial error from the server
    // would be overwritten by something generic on the first reconnect attempt
    // lastConnectError will be cleared when we connect successfully again
    if (self.lastConnectError) {
        [self postDelegateBlock:^{
            [multicastDelegate hipChatAppConnectFailed:self.lastConnectError];
        }];
        return self.lastConnectError;
    }

    isConnecting = NO;

    doReconnect = (code != HipChatErrorBadEmailPassword &&
            code != HipChatErrorPolicyViolation &&
            code != HipChatErrorResetPassword &&
            code != HipChatErrorServerError &&
            code != HipChatErrorServerHostNotFound);

    DDLogInfo(@"Sending connect failed notice with code: %ld -- doReconnect: %d -- msg: %@",
    (long) code, doReconnect, errMsg);
    NSDictionary *info = [NSDictionary dictionaryWithObject:errMsg forKey:NSLocalizedDescriptionKey];
    NSError *err = [NSError errorWithDomain:HipChatErrorDomain code:code userInfo:info];
    self.lastConnectError = err;

    [self postDelegateBlock:^{
        [multicastDelegate hipChatAppConnectFailed:err];
    }];

    return err;
}

- (void)setHostsWithAuth:(NSXMLElement *)authResponse {
    NSString *api = [authResponse attributeStringValueForName:@"api_host"];
    NSString *chat = [authResponse attributeStringValueForName:@"chat_host"];
    NSString *muc = [authResponse attributeStringValueForName:@"muc_host"];
    NSString *web = [authResponse attributeStringValueForName:@"web_host"];

    if (api) {apiHost = api;}
    if (chat) {chatHost = chat;}
    if (muc) {mucHost = muc;}
    if (web) {webHost = web;}
}

- (void)setReconnectDelay:(NSInteger)delay andInterval:(NSInteger)interval {
    reconnector.reconnectDelay = delay;
    reconnector.reconnectTimerInterval = interval;
    DDLogInfo(@"Reconnect delay set to %ld. Reconnect interval set to %ld", (long) delay, (long) interval);
}

- (void)setupCookies {
    DDLogInfo(@"Setting shared cookies for auto-login functionality");

    NSArray *matches = [self.webHost arrayOfCaptureComponentsMatchedByRegex:@"([^\\.]+\\.[^\\.]+)$"];
    NSString *cookieDomain;
    if ([matches count]) {
        cookieDomain = [@"." stringByAppendingString:[[matches objectAtIndex:0] objectAtIndex:0]];
    } else {
        cookieDomain = self.webHost;
    }

    NSHTTPCookie *tokenCookie =
            [NSHTTPCookie cookieWithProperties:[NSDictionary dictionaryWithObjectsAndKeys:
                    @"auth-token", NSHTTPCookieName,
                    [authToken objectForKey:@"token"], NSHTTPCookieValue,
                    cookieDomain, NSHTTPCookieDomain,
                    @"/", NSHTTPCookiePath,
                    @"TRUE", NSHTTPCookieDiscard,
                    @"TRUE", NSHTTPCookieSecure,
                    nil]];
    NSHTTPCookie *uidCookie =
            [NSHTTPCookie cookieWithProperties:[NSDictionary dictionaryWithObjectsAndKeys:
                    @"auth-uid", NSHTTPCookieName,
                    [Helpers getJidUserId:currentUser.jid], NSHTTPCookieValue,
                    cookieDomain, NSHTTPCookieDomain,
                    @"/", NSHTTPCookiePath,
                    @"TRUE", NSHTTPCookieDiscard,
                    @"TRUE", NSHTTPCookieSecure,
                    nil]];

    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:tokenCookie];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:uidCookie];
}

- (void)signOutUser {
    isManualDisconnect = YES;
    doReconnect = NO;

    // Use this event to trigger showing the SignedOut view
    // (instead of having to wait for the socket to trigger a disconnect event)
    [self postDelegateBlock:^{
        [multicastDelegate hipChatAppWillManuallyDisconnect];
    }];

    int64_t delayInMillis = 500;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInMillis * NSEC_PER_MSEC);
    dispatch_after(popTime, HipChatApp.hipchatQueue, ^(void) {
        [HipChatApp.instance finishUserSignOut];
    });
}

- (void)signOutUserAndClearCredentials {
    // Give time for the prefs save and token removal to work before actually
    // clearing data and disconnecting
    [self clearSignInPassword];
    [self savePreferences];

    [self signOutUser];
}

- (void)finishUserSignOut {
    [self clearUserData];
    [self disconnect];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark XMPPStreamDelegate methods
///////////////////////////////////////////////////////////////////////

- (void)xmppStreamDidConnect:(XMPPStream *)sender {
    if (!conn) {
        DDLogError(@"Connection has not yet been initialized");
        [self sendConnectFailed:@"Unable to connect to HipChat" code:HipChatErrorUnknown];
        return;
    }

    isFirstConnect = NO;
    self.lastConnectError = nil;
    disconnectedAt = nil;
    if ([conn isSecure]) {
        NSError *err;
        HipChatXMPPAuthentication *auth = [[HipChatXMPPAuthentication alloc] initWithStream:conn
                                                                                      email:loginEmail
                                                                                   password:loginPassword
                                                                                   resource:resource];
        BOOL success = [conn authenticate:auth error:&err];
        if (!success) {
            DDLogError(@"Error calling authWithPassword: %@", [err localizedDescription]);
            err = [self sendConnectFailed:@"Unable to connect to HipChat" code:HipChatErrorUnknown];
            [conn disconnect];
        }
    } else {
        NSError *err;
        BOOL success = [self secureConnection:&err];
        if (!success) {
            DDLogError(@"Error calling secureConnection: %@", [err localizedDescription]);
            err = [self sendConnectFailed:@"Unable to connect to HipChat" code:HipChatErrorUnknown];
            [conn disconnect];
        }
    }
}

- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender {
    [multicastDelegate hipChatAppConnectSucceeded];

    isConnecting = NO;
    doReconnect = YES;

    [jidsWithPresences removeAllObjects];
    [jidsPendingPresence removeAllObjects];

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *storedGroupId = [NSString stringWithFormat:@"%ld", (long) [prefs integerForKey:@"storedGroupId"]];
    NSString *currentGroupId = [Helpers getJidGroupId:conn.myJID];
    if (![currentGroupId isEqualToString:storedGroupId]) {
        DDLogInfo(@"Stored group id of %@ does not match logged in group id %@ - clearing stored data",
        storedGroupId, currentGroupId);
        [self clearUserData];
    }
    [prefs setInteger:[currentGroupId integerValue] forKey:@"storedGroupId"];
    [prefs synchronize];

    isUserLoaded = NO;
    currentUser = [[HipChatUser alloc] initWithJid:conn.myJID];
    [self storeSignInEmail:loginEmail andPassword:loginPassword];

    [self requestStartup];
    [roster fetchWithVersion:[prefs stringForKey:@"rosterVersion"]];
//    [roster fetchWithVersion:@"2012-10-10T00:00:00Z"];
    [emoticonList fetchEmoticons];
    [self sendInitialPresence];
}

- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(NSXMLElement *)error {
    DDLogInfo(@"Received didNotAuthenticate error: %@", error);
    NSDictionary *errorData = [Helpers getErrorDataForError:error];
    NSString *errorText = [Helpers getTextForError:error];
    NSInteger errorCode = (errorData ? [[errorData objectForKey:@"code"] intValue] : (NSInteger) HipChatErrorBadEmailPassword);

    if (errorText) {
        [self sendConnectFailed:errorText code:errorCode];
    } else {
        [self sendConnectFailed:@"Unable to connect to the HipChat server." code:errorCode];
    }

    // Show a link to reset their password if applicable
    if (errorCode == HipChatErrorResetPassword) {
        [self showFlash:[NSString stringWithFormat:@"<a href='https://%@/account/password?required_email=%@'>Click here</a> to reset your password.",
                                                   self.webHost,
                                                   [Helpers escapeUrl:loginEmail]]
               duration:0];
    }
}

- (void)xmppStream:(XMPPStream *)sender didReceiveError:(id)error {
    if ([error isKindOfClass:[NSXMLElement class]]) {
        [self handleXMPPError:error];
    }
}

- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq {
    NSString *iqId = [iq attributeStringValueForName:@"id"];
    DDLogVerbose(@"Got IQ with ID: %@", iqId);

    // Do some generic error handling (show flash messages)
    if ([iq isErrorIQ]) {
        [self handleErrorStanza:iq];
    }

    // Check for callback
    IQCallback callback = [iqCallbacks objectForKey:iqId];
    if (callback) {
        callback(iq);
        [iqCallbacks removeObjectForKey:iqId];
        return YES;
    }

    // Break on errors (generic error handling is done before callbacks)
    if ([iq isErrorIQ]) {
        return YES;
    }

    // Auth token response
    NSXMLElement *authQuery = [iq elementForName:@"query" xmlns:@"http://hipchat.com/protocol/auth"];
    if (authQuery) {
        DDLogInfo(@"Received auth token response - updating API auth data & clearing queue");
        authTokenRequested = NO;

        NSXMLElement *token = [authQuery elementForName:@"token"];
        [self setAuthTokenWithXML:token];
        [self clearAuthTokenQueue];
        return YES;
    }

    // Update cached data on roster pushes
    NSXMLElement *rosterQuery = [iq elementForName:@"query" xmlns:@"jabber:iq:roster"];
    if (self.isRosterLoaded && rosterQuery) {
        [self handleRosterPush:iq];
        return YES;
    }

    // EmoticonList uses IQ callbacks to handle its fetch (unlike roster, which
    // listens to the connection directly)
    // Send emoticon pushes to be handled by the emoticon list here
    NSXMLElement *emoticonQuery = [iq elementForName:@"query" xmlns:@"http://hipchat.com/protocol/emoticons"];
    if (self.isEmoticonListLoaded && emoticonQuery) {
        DDLogInfo(@"Got emoticon push - update emoticon list");
        [emoticonList handleEmoticonPush:iq];
        return YES;
    }

    return NO;
}

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message {
    if ([message isErrorMessage]) {
        [self handleErrorStanza:message];
    }

    NSXMLElement *inviteNode = [[message elementForName:@"x" xmlns:@"http://jabber.org/protocol/muc#user"] elementForName:@"invite"];
    if (inviteNode) {
        [self handleInvite:message inviteNode:inviteNode];
    } else {
        [self handleMessage:message];
    }

    NSString *bareJid = [message.from bare];
    if ([Helpers isUserJid:message.from] && [self jidNeedsPresence:bareJid]) {
        [roster requestPresencesForJidsImmediate:[NSSet setWithObject:bareJid]];
        [jidsPendingPresence addObject:bareJid];
    }
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence {
    if ([presence isErrorPresence]) {
        [self handleErrorStanza:presence];
    } else {
        if ([Helpers isUserJid:presence.from]) {
            [jidsWithPresences addObject:[presence.from bare]];
            [jidsPendingPresence removeObject:[presence.from bare]];
        } else {
            NSXMLElement *userNode = [presence elementForName:@"x" xmlns:@"http://jabber.org/protocol/muc#user"];
            if (userNode) {
                NSXMLElement *item = [userNode elementForName:@"item"];
                XMPPJID *userJid = [XMPPJID jidWithString:[[item attributeForName:@"jid"] stringValue]];
                NSString *bareJid = [userJid bare];
                if (userJid && [Helpers isUserJid:userJid] && [self jidNeedsPresence:bareJid]) {
                    [roster requestPresenceForJid:bareJid];
                    [jidsPendingPresence addObject:bareJid];
                }
            }
        }
    }
}

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error {
    DDLogInfo(@"XMPP Disconnected with error: %@", error);
    [self onDisconnect];
    if (error) {
        // Only send connectFailed if we got an error (otherwise this may be a manual disconnect)
        if ([self connectsToHipChatServer] && [[error domain] isEqualToString:@"kCFStreamErrorDomainNetDB"]) {
            DDLogError(@"Host could not be resolved (getaddrinfo error code: %ld): %@", (long) error.code, error.localizedDescription);
            [self sendConnectFailed:@"HipChat Server host not found" code:HipChatErrorServerHostNotFound];
        } else if ([self connectsToHipChatServer] && [[error domain] isEqualToString:@"kCFStreamErrorDomainSSL"]) {
            DDLogError(@"Host SSL certificate failed validation: %ld): %@", (long) error.code, error.localizedDescription);
            [self sendConnectFailed:@"HipChat Server host certificate is invalid" code:HipChatErrorServerHostSSLInvalid];
        } else {
            [self sendConnectFailed:@"Failed to connect to the HipChat server" code:HipChatErrorServerUnreachable];
        }
    }
    if (isFirstConnect) {
        [self signOutUser];
    }
}


- (void)xmppStreamDidDisconnect:(XMPPStream *)sender {
    DDLogInfo(@"XMPP Did Disconnect");
    [self onDisconnect];
}

- (void)onDisconnect {
    isConnecting = NO;

    authTokenRequested = NO;
    doAutoJoin = YES;
    isAutoJoinFinished = NO;
    isAutoJoinLoaded = NO;
    isEmoticonListLoaded = NO;
    isFullyLoaded = NO;
    isRoomListLoaded = NO;
    isRosterLoaded = NO;

    // Deinitialize all the chats - they will get re-initialized if we reconnect
    // (after triggering hipChatAppFullyLoaded
    
    NSDictionary *activeChatsCopy = [activeChats immutableCopy];
    for (id key in activeChatsCopy) {
        [[activeChatsCopy objectForKey:key] performSelector:@selector(deinitialize)];
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark XMPPReconnectDelegate methods
///////////////////////////////////////////////////////////////////////

- (void)xmppReconnect:(XMPPReconnect *)sender didDetectAccidentalDisconnect:(SCNetworkReachabilityFlags)connectionFlags {
    DDLogInfo(@"Detected accidental disconnect");
    if (!disconnectedAt) {
        disconnectedAt = [NSDate date];
    }
    isManualReconnect = NO;
    [self onDisconnect];
}

- (BOOL)xmppReconnect:(XMPPReconnect *)sender shouldAttemptAutoReconnect:(SCNetworkReachabilityFlags)reachabilityFlags {
    if (isManualReconnect) {
        DDLogInfo(@"isManualReconnect set - returning YES for shouldAttemptReconnect");
        isManualReconnect = NO;
        return YES;
    }
    if (self.conn.changingHosts) {
        return YES;
    }

    if (ABS([disconnectedAt timeIntervalSinceNow]) < (reconnector.reconnectDelay - 0.2)) {
        DDLogInfo(@"Hasn't been long enough since disconnection - do NOT attempt reconnect");
        return NO;
    }
    DDLogInfo(@"AutoReconnect - Returning %@ for shouldAttemptReconnect", (doReconnect ? @"YES" : @"NO"));
    return doReconnect;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark EmoticonList Delegate methods
///////////////////////////////////////////////////////////////////////

- (void)emoticonListLoaded:(EmoticonList *)sender {
    DDLogInfo(@"Received emoticonListLoaded event - marking emoticon list loaded");
    isEmoticonListLoaded = YES;
    [self checkFullyLoaded];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark RosterDelegate methods
///////////////////////////////////////////////////////////////////////

- (void)rosterFinishedLoading:(Roster *)sender {
    NSDictionary *users = [sender getData];

    DDLogInfo(@"Roster finished loading - populating display names with %ld users", (long) [users count]);
    NSString *key = nil;
    for (key in users) {
        User *user = [users objectForKey:key];
        [self setDisplayNameForJid:user.jid withFullName:user.name displayName:user.displayName];
    }

    DDLogInfo(@"Cached users...marking roster loaded in HipChatApp");
    isRosterLoaded = YES;
    [self updateMentionRegex];
    [self checkFullyLoaded];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark RoomList Delegate methods
///////////////////////////////////////////////////////////////////////

- (void)roomListLoaded:(RoomList *)sender {
    DDLogInfo(@"Received roomListLoaded event - marking room list loaded");
    isRoomListLoaded = YES;
    [self checkFullyLoaded];
}

- (void)roomList:(RoomList *)sender deletedRoom:(XMPPJID *)jid {
    id chat = [self getActiveChat:jid];
    if (chat) {
        [self closeChat:jid];
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Chat methods
///////////////////////////////////////////////////////////////////////

- (void)addToAutoJoin:(XMPPJID *)jid {
    [self addToAutoJoin:jid atIndex:-1];
}

- (void)addToAutoJoin:(XMPPJID *)jid atIndex:(NSInteger)index {
    // Don't add items to the autoJoin list during autoJoin itself
    if (!isAutoJoinFinished) {
        return;
    }

    NSInteger currentIdx = [self indexOfAutoJoinChat:jid];
    if (index == currentIdx) {
        return;
    }

    dispatch_barrier_async(autoJoinList.collectionQueue, ^{
        if (currentIdx == NSNotFound) {
            DDLogInfo(@"Adding %@ to list of chats to auto join, index: %ld", jid.bare, (long) index);
            if (index == -1) {
                [autoJoinList.internalArray addObject:jid.bare];
            } else {
                [autoJoinList.internalArray insertObject:jid.bare atIndex:MIN(autoJoinList.internalArray.count, index)];
            }
        } else if (index != currentIdx && index != -1) {
            [autoJoinList.internalArray removeObjectAtIndex:currentIdx];
            [autoJoinList.internalArray insertObject:jid.bare atIndex:MIN(autoJoinList.internalArray.count, index)];
        }
    });
    [self savePreferences];
}

// Synchronize to avoid active chats being iterated and modified at the same time
- (void)autoJoinChats {
    if (!isAutoJoinLoaded || !isRosterLoaded) {
        DDLogInfo(@"Data not yet loaded - not auto-joining chats");
        return;
    }

    // If we a reconnecting, autoJoin will already be loaded, but the user isn't
    // fully initialized until we receive the startup IQ back from the server
    if (!currentUser.name) {
        DDLogInfo(@"User not set - not auto-joining chats");
        return;
    }

    if (!doAutoJoin) {
        DDLogInfo(@"doAutoJoin not set - not auto-joining chats");
        return;
    }

    DDLogInfo(@"All required data loaded - auto-joining chats");
    doAutoJoin = NO;

    for (NSString *key in [autoJoinList immutableCopy]) {
        XMPPJID *jid = [XMPPJID jidWithString:key];

        if ([Helpers isRoomJid:jid] && ![self getRoomInfo:jid]) {
            continue;
        } else if ([Helpers isUserJid:jid] && ![self getUserInfo:jid]) {
            continue;
        }

        [self joinChat:jid];
    }

    // Since we only auto-join chats at login,
    // determine if we have any chats that are NOT supposed to be open
    // and close them (basically a form of tab-syncing)
    NSMutableArray *chatsToClose = [NSMutableArray array];
    for (XMPPJID *jid in self.activeChatJids) {
        if (![self isAutoJoinChat:jid] && ![jid isEqualToJID:self.jidToFocus options:XMPPJIDCompareBare]) {
            [chatsToClose addObject:jid];
        }
    }
    for (XMPPJID *jid in chatsToClose) {
        DDLogInfo(@"Closing active chat not found in autojoin: %@", jid);
        [self closeChat:jid isManual:NO];
    }

    // Update all the unread counts (for the case of reconnects)
    [self postDelegateBlock:^{
        [multicastDelegate hipChatAppUnreadChangedForJid:nil];
    }];
    isAutoJoinFinished = YES;

    // Only use the NSUserDefaults jidToFocus if it hasn't been set elsewhere
    // (like from a push notif)
    if (!self.jidToFocus) {
        // If the stored jid to focus is in the auto-joined chats, set it
        // If not, clear it - the user probably closed the chat from another client
        XMPPJID *storedJidToFocus = [XMPPJID jidWithString:[[NSUserDefaults standardUserDefaults] stringForKey:@"jidToFocus"]];
        BOOL isAutoJoinChat = [self isAutoJoinChat:storedJidToFocus];
        if (!isAutoJoinChat) {
            DDLogInfo(@"Stored jidToFocus is not in autojoin chats - removing: %@", storedJidToFocus);
            self.jidToFocus = nil;
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"jidToFocus"];
        } else {
            DDLogInfo(@"Stored jidToFocus is in autojoin chats - setting as focus: %@", storedJidToFocus);
            self.jidToFocus = storedJidToFocus;
        }
    }

    // Check to see if we should autofocus a chat
    // This may be the last chat we had opened before closing the app
    // It may also be the jid that caused a push notification
    // Always make sure we've joined the chat beforehand
    if (self.jidToFocus) {
        DDLogInfo(@"Focusing jidToFocus: %@", self.jidToFocus);
        [self joinChat:self.jidToFocus withFocus:YES];
        self.jidToFocus = nil;
    }

    [self postDelegateBlock:^{
        [multicastDelegate hipChatAppAutoJoinFinished];
    }];

    // On iOS, check group size before loading rooms/presence right away
    // On OSX, just always load everything right away
#if TARGET_OS_IPHONE
    if ([roster itemCount] > 100) {
        DDLogInfo(@"Large roster cached - delaying presence fetch");
        [roomList fetchItems:mucHost];
        [roster performSelector:@selector(requestPresences) withObject:nil afterDelay:5];
    } else {
        DDLogInfo(@"Smaller roster - doing presence/room fetch immediately");
        [self loadRoomsAndPresence];
    }
#else
    [self loadRoomsAndPresence];
#endif
}

- (void)clearUnreadCount:(XMPPJID *)jid {
    DDLogInfo(@"Clearing unread count for jid: %@", jid);
    if (jid == nil) {
        [unreadMessageCounts removeAllObjects];
        [unreadChats removeAllObjects];
    } else {
        [unreadMessageCounts setObject:[NSNumber numberWithInt:0] forKey:jid.bare];
        [unreadChats setObject:[NSNumber numberWithBool:NO] forKey:jid.bare];
    }

    [self postDelegateBlock:^{
        [multicastDelegate hipChatAppUnreadChangedForJid:jid];
    }];
}

- (void)closeChat:(XMPPJID *)jid {
    [self closeChat:jid notifyDelegates:YES isManual:NO];
}

- (void)closeChat:(XMPPJID *)jid isManual:(BOOL)isManual {
    [self closeChat:jid notifyDelegates:YES isManual:isManual];
}

// Synchronize to avoid active chats being iterated and modified at the same time
- (void)closeChat:(XMPPJID *)jid notifyDelegates:(BOOL)notifyDelegates isManual:(BOOL)isManual {
    DDLogInfo(@"Closing chat %@", jid.bare);

    if ([Helpers isRoomJid:jid]) {
        Room *room = [self.roomList roomForJid:jid];
        [room leave];
    } else if ([Helpers isUserJid:jid]) {
        NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
        [message addAttributeWithName:@"to" stringValue:jid.bare];
        [message addAttributeWithName:@"type" stringValue:@"chat"];
        NSXMLElement *chatState = [NSXMLElement elementWithName:GONE_CHAT_STATE xmlns:@"http://jabber.org/protocol/chatstates"];
        [message addChild:chatState];

        [self.conn sendElement:message];
    } else if (!jid || jid == self.lobbyJid) {
        // nil jid or lobbyJid - we can safely ignore this call
        return;
    }

    [self removeFromAutoJoin:jid];
    [[self getActiveChat:jid] performSelector:@selector(close)];
    [activeChats removeObjectForKey:jid.bare];
    [activeChatJids removeObject:jid];
    [self clearUnreadCount:jid];

    NSDictionary *args = [NSDictionary dictionaryWithObjectsAndKeys:
            jid, @"jid",
            [NSNumber numberWithBool:isManual], @"isManual",
            nil];
    [self postNotifWithName:HipChatLeaveChatNotification args:args];

    if (notifyDelegates) {
        [self postDelegateBlock:^{
            [multicastDelegate hipChatAppChatsChanged];
        }];
    }
}

- (void)focusChat:(XMPPJID *)jid {
    currentJid = jid;
    if (jid == self.lobbyJid) {
        [self focusLobby];
    } else if (jid == self.searchJid) {
        [self focusSearchResults];
    } else {
        DDLogInfo(@"Focusing chat with jid: %@", jid);
        [self clearUnreadCount:jid];

        NSDictionary *args = [NSDictionary dictionaryWithObjectsAndKeys:jid, @"jid", nil];
        [self postNotifWithName:HipChatFocusChatNotification args:args];
    }

    if (jid && jid != self.searchJid) {
        [[NSUserDefaults standardUserDefaults] setValue:jid.bare forKey:@"jidToFocus"];
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"jidToFocus"];
    }
}

- (void)focusLobby {
    DDLogInfo(@"Focusing lobby...");
    self.currentJid = lobbyJid;
    [self postNotifWithName:HipChatFocusLobbyNotification args:nil];
}

- (void)focusNextChat {
    [self postNotifWithName:HipChatFocusNextChatNotification args:nil];
}

- (void)focusPreviousChat {
    [self postNotifWithName:HipChatFocusPreviousChatNotification args:nil];
}

- (void)focusSearchResults {
    [self showSearchResultsForQuery:nil jid:nil];
}

- (NSInteger)getTotalUnreadCount {
    __block NSInteger totalCount = 0;
    dispatch_sync(unreadMessageCounts.collectionQueue, ^{
        [unreadMessageCounts.internalDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            totalCount += [obj intValue];
        }];
    });

    return totalCount;
}

- (NSInteger)getUnreadCountForJid:(XMPPJID *)jid {
    id count = [unreadMessageCounts objectForKey:jid.bare];
    if (count) {
        return [count intValue];
    } else {
        return 0;
    }
}

- (BOOL)getUnreadForJid:(XMPPJID *)jid {
    id chat = [unreadChats objectForKey:jid.bare];
    return chat != nil && [chat boolValue];
}

- (void)incrementUnreadForJid:(XMPPJID *)jid {
    // If this jid is the focused chat, don't increment unread
    if ([self isAppFocused] && [currentJid.bare isEqualToString:jid.bare]) {
        return;
    }

    DDLogInfo(@"Incrementing unread count for %@", jid);
    dispatch_barrier_async(unreadMessageCounts.collectionQueue, ^{
        if ([unreadMessageCounts.internalDictionary objectForKey:jid.bare]) {
            NSInteger incVal = [[unreadMessageCounts.internalDictionary objectForKey:jid.bare] intValue];
            incVal++;
            [unreadMessageCounts.internalDictionary setObject:[NSNumber numberWithLong:incVal] forKey:jid.bare];
        } else {
            // Only increment the unread count if we actually have a CVC for this jid
            [unreadMessageCounts.internalDictionary setObject:[NSNumber numberWithLong:1] forKey:jid.bare];
        }
    });

    // Always mark chats unread when incrementing unread counts
    [self markChatUnread:jid];
}

- (NSInteger)indexOfAutoJoinChat:(XMPPJID *)jid {
    return [autoJoinList indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isEqualToString:jid.bare]) {
            *stop = YES;
            return YES;
        }
        return NO;
    }];
}

- (NSUInteger)getActiveChatIndex:(XMPPJID *)jid {
    return [activeChatJids indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        if ([[obj bare] isEqualToString:jid.bare]) {
            *stop = YES;
            return YES;
        }
        return NO;
    }];
}

- (void)enqueueActiveChatJid:(XMPPJID *)jid {
    [activeChatJids addObject:jid];
}

- (BOOL)isActiveChat:(XMPPJID *)jid {
    return ([self getActiveChatIndex:jid] != NSNotFound);
}

- (id)getActiveChat:(XMPPJID *)jid {
    return [activeChats objectForKey:jid.bare];
}

- (void)setActiveChat:(id)chat forJid:(XMPPJID *)jid {
    [activeChats setObject:chat forKey:jid.bare];
}

- (BOOL)isAutoJoinChat:(XMPPJID *)jid {
    return ([self indexOfAutoJoinChat:jid] != NSNotFound);
}

- (void)joinChat:(XMPPJID *)jid {
    return;
}

- (void)joinChat:(XMPPJID *)jid withFocus:(BOOL)doFocus {
    return;
}

- (void)joinChat:(XMPPJID *)jid withMessage:(XMPPMessage *)initialMessage {
    return;
}

- (void)markChatUnread:(XMPPJID *)jid {
    [unreadChats setObject:[NSNumber numberWithBool:YES] forKey:jid.bare];
}

- (void)removeFromAutoJoin:(XMPPJID *)jid {
    DDLogInfo(@"Removing %@ from list of chats to auto join", jid.bare);

    [autoJoinList removeObject:jid.bare];
    [self savePreferences];
}

- (void)setAutoJoin:(NSMutableArray *)newAutoJoin {
    autoJoinList = [[ThreadSafeArray alloc] initWithQueueName:"com.hipchat.autoJoinList" andArray:newAutoJoin];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark IQ Methods
///////////////////////////////////////////////////////////////////////

- (NSString *)requestHistoryForChat:(XMPPJID *)jid beforeTime:(NSTimeInterval)fetchTime count:(NSInteger)historyCount {
    DDLogInfo(@"Requesting history for chat: %@", jid);
    NSString *uuid = [conn generateUUID];
    NSXMLElement *iq = [NSXMLElement elementWithName:@"iq"];
    [iq addAttributeWithName:@"type" stringValue:@"get"];
    [iq addAttributeWithName:@"to" stringValue:jid.bare];
    [iq addAttributeWithName:@"id" stringValue:uuid];

    NSXMLElement *historyNode = [NSXMLElement elementWithName:@"query" xmlns:@"http://hipchat.com/protocol/history"];

    if (fetchTime) {
        NSString *beforeStr = [Helpers formatXMPPTimeInterval:fetchTime];
        [historyNode addAttributeWithName:@"before" stringValue:beforeStr];
    }
    if (historyCount) {
        [historyNode addAttributeWithName:@"maxstanzas" stringValue:[NSString stringWithFormat:@"%ld", (long) historyCount]];
    }
    [historyNode addAttributeWithName:@"type" stringValue:([Helpers isRoomJid:jid] ? @"groupchat" : @"chat")];

    [iq addChild:historyNode];

    [conn sendElement:iq];
    return uuid;
}

- (NSString *)requestProfileForJid:(XMPPJID *)jid callback:(IQCallback)callback {
    NSString *uuid = [NSString stringWithFormat:@"profile_%@", [conn generateUUID]];
    NSXMLElement *iq = [NSXMLElement elementWithName:@"iq"];
    [iq addAttributeWithName:@"type" stringValue:@"get"];
    [iq addAttributeWithName:@"to" stringValue:jid.bare];
    [iq addAttributeWithName:@"id" stringValue:uuid];

    NSXMLElement *queryNode = [NSXMLElement elementWithName:@"query" xmlns:@"http://hipchat.com/protocol/profile"];
    [iq addChild:queryNode];

    [conn sendElement:iq];
    if (callback) {
        [iqCallbacks setObject:[callback copy] forKey:uuid];
    }
    return uuid;
}

- (NSString *)requestStartup {
    NSString *uuid = [NSString stringWithFormat:@"startup_%@", [conn generateUUID]];
    XMPPIQ *iq = [XMPPIQ iqWithType:@"get" elementID:uuid];

    NSXMLElement *queryNode = [NSXMLElement elementWithName:@"query" xmlns:@"http://hipchat.com/protocol/startup"];
#if TARGET_OS_IPHONE
    [queryNode addAttributeWithName:@"send_auto_join_user_presences" stringValue:@"true"];
#endif
    [iq addChild:queryNode];

    // Check if we still haven't gotten a response after 5 seconds.
    // Sometimes the server drops the request and doesn't tell us about it
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, DEFAULT_REQUEST_TIMEOUT * NSEC_PER_SEC), _hipchatQueue, ^{
        if (!isAutoJoinLoaded && [conn isAuthenticated]) {
            DDLogInfo(@"Retrying startup IQ");
            [self requestStartup];
        }
    });

    [self sendIQ:iq callback:^(XMPPIQ *responseIq) {
        [self handleStartupResponse:responseIq];
    }];
    return uuid;
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Presence methods
///////////////////////////////////////////////////////////////////////
- (BOOL)jidNeedsPresence:(NSString *)jid {
    return ![jidsWithPresences member:jid] && ![jidsPendingPresence member:jid];
}

- (BOOL)jidHasPresence:(NSString *)jid {
    return [jidsWithPresences member:jid] != nil;
}

- (NSSet *)jidsWithoutPresences {
    NSMutableSet *jids = [NSMutableSet setWithArray:[[roster getData] allKeys]];
    [jids minusSet:jidsWithPresences.internalSet];
    [jids minusSet:jidsPendingPresence.internalSet];
    return jids;
}

- (void)sendInitialPresence {
    // Stub function - to be overridden in subclasses to include OS/verison data
}

// ARGS is an NSDictionary in the format:
// {
//  @"attributes": { <attribute_name>: <attribute_value>, ... }
//  @"textNodes": { <node_name>: <node_value>, ... }
//  @"elements": [ <element1>, <element2>, ... ]
// }
- (void)sendPresenceWithArgs:(NSDictionary *)args {
    NSXMLElement *presence = [NSXMLElement elementWithName:@"presence"];

    NSDictionary *attributes = [args objectForKey:@"attributes"];
    for (id key in attributes) {
        [presence addAttributeWithName:key stringValue:[attributes objectForKey:key]];
    }

    NSDictionary *textNodes = [args objectForKey:@"textNodes"];
    for (id key in textNodes) {
        NSXMLElement *newElement = [NSXMLElement elementWithName:key];
        [newElement addChild:[NSXMLElement textWithStringValue:[textNodes objectForKey:key]]];
        [presence addChild:newElement];
    }
    NSArray *elements = [args objectForKey:@"elements"];
    NSXMLElement *elem = nil;
    for (elem in elements) {
        [presence addChild:elem];
    }

    [[self conn] sendElement:presence];
}

- (void)updatePresenceWithShow:(NSString *)show status:(NSString *)status {
    [self updatePresenceWithShow:show status:status idle:0];
}

- (void)updatePresenceWithShow:(NSString *)show status:(NSString *)status idle:(NSTimeInterval)secondsIdle {
    previousPresenceShow = currentPresenceShow;
    previousPresenceStatus = currentPresenceStatus;

    currentPresenceStatus = status;
    currentPresenceShow = show;

    NSMutableDictionary *textNodes = [NSMutableDictionary dictionary];
    if (show) {
        [textNodes setObject:show forKey:@"show"];
    }
    if (status) {
        [textNodes setObject:status forKey:@"status"];
    }

    NSMutableDictionary *args = [NSMutableDictionary dictionaryWithObjectsAndKeys:textNodes, @"textNodes", nil];
    if (secondsIdle) {
        NSXMLElement *idleNode = [NSXMLElement elementWithName:@"query" xmlns:@"jabber:iq:last"];
        [idleNode addAttributeWithName:@"seconds" stringValue:[NSString stringWithFormat:@"%ld", (long) secondsIdle]];
        [args setObject:[NSArray arrayWithObject:idleNode] forKey:@"elements"];
    }
    [self sendPresenceWithArgs:args];
}

- (void)updatePresenceWithStatus:(NSString *)status {
    [self updatePresenceWithShow:currentPresenceShow status:status];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Helper Methods
///////////////////////////////////////////////////////////////////////

- (void)checkFullyLoaded {
    DDLogInfo(@"Checking fully loaded...");

    // Don't send out fully loaded multiple times
    // This might happen since we separately request the room list
    // when viewing the lobby (even though we may have loaded it
    // from cache when the app opened)
    if (isFullyLoaded) {
        DDLogInfo(@"Already fully loaded - not triggering fullyLoaded again");
        return;
    }

    if (isEmoticonListLoaded && isRoomListLoaded && isRosterLoaded && isUserLoaded) {
        DDLogInfo(@"Triggering hipChatAppFullyLoaded event - emoticons, roomList, roster, and user loaded");
        [self postDelegateBlock:^{
            [multicastDelegate hipChatAppFullyLoaded];
        }];
        isFullyLoaded = YES;
    } else {
        DDLogInfo(@"Not yet fully loaded - not triggering fullyLoaded event");
    }

    // Don't wait for room list to load in order to join rooms
    if (isAutoJoinLoaded && isRosterLoaded && isUserLoaded) {
        DDLogInfo(@"Loaded enough to run autojoin - calling autoJoinChats");
        dispatch_async(HipChatApp.hipchatQueue, ^{
            [self autoJoinChats];
        });
    }
}

- (NSMutableArray *)generateAutoJoinArg {
    NSArray *autoJoinListCopy = [autoJoinList immutableCopy];
    NSMutableArray *autoJoinArg = [NSMutableArray arrayWithCapacity:autoJoinListCopy.count];
    for (NSString *jidString in autoJoinListCopy) {
        NSString *name = [self getDisplayName:[XMPPJID jidWithString:jidString] abbreviated:NO];
        [autoJoinArg addObject:[NSDictionary dictionaryWithObjectsAndKeys:jidString, @"jid", name, @"name", nil]];
    }
    return autoJoinArg;
}

- (ActiveChatNotificationSetting)getActiveChatNotificationSetting:(XMPPJID *)jid {
    NSUserDefaults *userPrefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *activeChatNotifications = [NSMutableDictionary dictionaryWithDictionary:[userPrefs objectForKey:@"notifyForSpecificRooms"]];
    if ([[activeChatNotifications allKeys] containsObject:jid.bare]) {
        NSNumber *settingNumber = [activeChatNotifications objectForKey:jid.bare];
        ActiveChatNotificationSetting setting = (ActiveChatNotificationSetting)[settingNumber integerValue];
        return setting;
    }
    return ActiveChatNotificationOn;
}

- (NSDictionary *)getAllRooms {
    return [roomList getData];
}

- (NSDictionary *)getAllUsers {
    return [roster getData];
}

- (NSString *)getDisplayName:(XMPPJID *)jid abbreviated:(BOOL)abbreviate {
    // Don't use abbreviated names
    abbreviate = NO;

    if ([Helpers isRoomJid:jid]) {
        Room *room = [self getRoomInfo:jid];
        if ([room name]) {
            return [room name];
        } else {
            return @"Unknown";
        }
    }

    NSString *name = nil;
    if (abbreviate) {
        name = [abbreviatedNames objectForKey:jid.bare];
    } else {
        name = [displayNames objectForKey:jid.bare];
    }

    if (name) {
        return name;
    } else {
        return @"Unknown";
    }
}

- (Profile *)getProfileData:(XMPPJID *)jid {
    return [profiles objectForKey:jid.bare];
}

- (Room *)getRoomInfo:(XMPPJID *)jid {
    return [roomList roomForJid:jid];
}

- (User *)getUserInfo:(XMPPJID *)jid {
    return [roster userForJid:jid];
}

- (void)handleInvite:(XMPPMessage *)message inviteNode:(NSXMLElement *)inviteNode {
    // Handle invite messages:
    //	<message to="2_57@chat.mayo.hipchat.com" from="2_lowecase_sad_room@conf.mayo.hipchat.com">
    //		<x xmlns="http://jabber.org/protocol/muc#user">
    //			<invite from="2_2993@chat.mayo.hipchat.com/mac"/>
    //		</x>
    //		<x xmlns="http://hipchat.com/protocol/muc#room">
    //			<name>lowecase sad room</name>
    //			<privacy>public</privacy>
    //			<topic/>
    //		</x>
    //	</message>
    XMPPJID *fromJid = [XMPPJID jidWithString:[[message attributeForName:@"from"] stringValue]];
    inviteJid = fromJid;

    if (![self getRoomInfo:inviteJid]) {
        [self.roomList addRoomWithInvite:message];
    }

    XMPPJID *inviteFrom = [XMPPJID jidWithString:[[inviteNode attributeForName:@"from"] stringValue]];
    if ([[inviteFrom bare] isEqualToString:[[self.currentUser jid] bare]]) {
        // If this is a syncted chat tab, just join the room automatically
        DDLogInfo(@"Syncing chat join from another session. JID: %@", inviteJid);
        [self joinChat:inviteJid];
        return;
    }

    NSString *reason = [[inviteNode elementForName:@"reason"] stringValue];
    NSDictionary *args = [NSDictionary dictionaryWithObjectsAndKeys:
            fromJid, @"roomJid",
            inviteFrom, @"sender",
            reason, @"reason",
            nil];
    [self postNotifWithName:HipChatInviteNotification args:args];
}

- (void)handleMessage:(XMPPMessage *)message {
    if (!message.delayedDeliveryDate) {
        NSString *body = [[message elementForName:@"body"] stringValue];
        if (body.length > 0) {
            NSXMLElement *mucNode = [message elementForName:@"x" xmlns:@"http://hipchat.com/protocol/muc#room"];
            NSXMLElement *fileNode = [mucNode elementForName:@"file"];
            if (fileNode) {
                [self postNotifWithName:HipChatFileAddedNotification
                                   args:[NSDictionary dictionaryWithObject:message.from forKey:@"jid"]];
            } else {
                NSMutableArray *matches = [NSMutableArray array];
                [Linkify linkify:body withEmotions:NO andTruncate:0 matches:matches];
                if (matches.count > 0) {
                    [self postNotifWithName:HipChatLinkAddedNotification
                                       args:[NSDictionary dictionaryWithObject:message.from forKey:@"jid"]];
                }
            }
        }
    }

    XMPPJID *fromJid = [XMPPJID jidWithString:[[message attributeForName:@"from"] stringValue]];
    XMPPJID *senderJid = [Helpers getSenderForMessage:message];
    NSString *bareSenderJid = [senderJid bare];

    BOOL isFromCurrentUser = [bareSenderJid isEqualToString:currentUser.jid.bare];
    BOOL isDelayed = ([[message elementsForName:@"delay"] count] > 0);
    id activeChat = [self getActiveChat:fromJid];
    if (isFromCurrentUser) {
        NSXMLElement *goneNode = [message elementForName:GONE_CHAT_STATE xmlns:NS_CHAT_STATES];
        if (goneNode && activeChat) {
            [self closeChat:fromJid];
            return;
        }
        NSXMLElement *activeNode = [message elementForName:ACTIVE_CHAT_STATE xmlns:NS_CHAT_STATES];
        if (activeNode && !activeChat) {
            [self joinChat:fromJid withFocus:NO];
            return;
        }
    }

    // If we get a non-historymessage for a private chat we don't have open,
    // join the chat since it means someone just messaged us specifically
    BOOL isPrivateChat = [Helpers isUserJid:fromJid];
    NSString *body = [[message elementForName:@"body"] stringValue];
    if (isPrivateChat &&
            !activeChat &&
            [body length] > 0 &&
            !isDelayed) {
        [self joinChat:fromJid withMessage:message];
    }

    // If we get a private chat message, but we don't have a last presence for the sender, make that request
    if (isPrivateChat && [self jidNeedsPresence:bareSenderJid]) {
        [roster requestPresencesForJidsImmediate:[NSSet setWithObject:bareSenderJid]];
        [jidsPendingPresence addObject:bareSenderJid];
    }

    // Determine whether we should notify / increment unread counts...

    // Don't notify on history message (don't even bother checking the other stuff)
    if (message.delayedDeliveryDate) {
        return;
    }

    // Only notify for chat/file type or system types with flag set
    BOOL sysNotify = NO;
    BOOL isSystemMessage = NO;

    NSXMLElement *mucNode = [message elementForName:@"x" xmlns:@"http://hipchat.com/protocol/muc#room"];
    NSString *typeValue = [[mucNode elementForName:@"type"] stringValue];
    if ([typeValue isEqualToString:@"system"] || [typeValue isEqualToString:@"announcement"]) {
        isSystemMessage = YES;
        sysNotify = [[[mucNode elementForName:@"notify"] stringValue] isEqualToString:@"1"];
    }

    BOOL doNotify = !isSystemMessage || sysNotify;

    // Don't notify if there's no body to the message
    doNotify = doNotify && ([body length] > 0);
    doNotify = doNotify && ([body rangeOfRegex:@"^(/me|/em)"].location == NSNotFound);

    // Don't notify if we are the person that sent the message
    // This can happen when you switch chats before a message returns from the server
    XMPPJID *sender = [Helpers getSenderForMessage:message];

    doNotify = doNotify && (!sender || ![sender.bare isEqualToString:currentUser.jid.bare]);

    // Don't notify if this is the currently focused chat
    // TODO: Should we / can we notify anyway if the app is in the background (or at least buzz?)
    if (doNotify) {
        dispatch_async(dispatch_get_main_queue(), ^{
            BOOL isFocusedChat = (currentJid && [currentJid.bare isEqualToString:fromJid.bare]);
            if ([self notifyUserForMessage:message]) {
                // Only increment chats when the user was notified
                // (e.g. don't increment counts if the user get a message to an open room but
                // has open room notifications disabled via preferences)
                [self incrementUnreadForJid:fromJid];
            } else if (!isFocusedChat) {
                // Always mark tabs as unread when new notification-worty messages
                // come in regardless of actual notification preferences
                [self markChatUnread:fromJid];
            }

            [self postDelegateBlock:^{
                [multicastDelegate hipChatAppUnreadChangedForJid:fromJid];
            }];
        });
    }
}

- (Profile *)handleProfileResponse:(XMPPIQ *)iq {
    DDLogInfo(@"Got response for profile fetch");
    Profile *profile = [[Profile alloc] initWithIq:iq];
    [profiles setObject:profile forKey:profile.jid.bare];
    return profile;
}

- (void)handleRosterPush:(XMPPIQ *)iq {
    NSXMLElement *rosterQuery = [iq elementForName:@"query" xmlns:@"jabber:iq:roster"];

    // Just update version value and local caches here
    // The roster will listen for IQ stanzas itself and handle
    // roster pushes by updating the user object(s) required
    NSString *newVersion = [rosterQuery attributeStringValueForName:@"ver"];
    if (newVersion && newVersion.length > 0) {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setObject:newVersion forKey:@"rosterVersion"];
    }

    DDLogInfo(@"Got roster push - updating roster version and local caches");
    NSXMLElement *item = [rosterQuery elementForName:@"item"];
    XMPPJID *jid = [XMPPJID jidWithString:[item attributeStringValueForName:@"jid"]];
    [self setDisplayNameForJid:jid
                  withFullName:[item attributeStringValueForName:@"name"]
                   displayName:[item attributeStringValueForName:@"display_name"]
                   forceUpdate:YES];

    [self updateMentionRegex];
}

//<iq type="result" id="1">
//	<query xmlns="http://hipchat.com/protocol/preferences">
//		<mention_name>FirstLast</mention_name>
//		<name>First Last</name>
//		<preferences>
//			<autoJoin>
//				<item jid="1_chat_one@conf.hipchat.com">
//					... room data ...
//				</item>
//				<item jid="1_234@chat.hipchat.com" />
//			</autoJoin>
//			<notifications>
//				<mention>email</mention>
//				<oto_message>email</oto_message>
//				<oto_message>sms</oto_message>
//				<room_invite>sms</room_invite>
//			</notifications>
//		</preferences>
//	</query>
//</iq>
- (void)handleStartupResponse:(XMPPIQ *)iq {
    DDLogInfo(@"Got response for startup info");

    NSXMLElement *query = [iq elementForName:@"query" xmlns:@"http://hipchat.com/protocol/startup"];
    [currentUser updateWithStartupInfo:query];

    NSArray *items = [[[query elementForName:@"preferences"] elementForName:@"autoJoin"] elementsForName:@"item"];
    NSMutableArray *jidsForAutojoin = [NSMutableArray arrayWithCapacity:[items count]];
    for (NSXMLElement *item in items) {
        NSString *jid = [item attributeStringValueForName:@"jid"];
        [jidsForAutojoin addObject:jid];

        // Add rooms to roomList so we can join them right away
        if ([Helpers isRoomJid:[XMPPJID jidWithString:jid]]) {
            [roomList handleDiscoItem:item];
        }
    }

    dispatch_barrier_async(autoJoinList.collectionQueue, ^{
        [autoJoinList.internalArray removeAllObjects];
        [autoJoinList.internalArray addObjectsFromArray:jidsForAutojoin];
    });

    NSXMLElement *token = [query elementForName:@"token"];
    if (token) {
        [self setAuthTokenWithXML:token];
    }

    // Don't wait on this (helps show the rooms in the lobby view more quickly)
    isAutoJoinLoaded = YES;
    isUserLoaded = YES;
    [self checkFullyLoaded];
}

- (BOOL)isAppFocused {
    [NSException raise:@"InvalidFunctionCallException" format:@"Cannot test isAppFocused from super class"];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunreachable-code"
    return YES;
#pragma clang diagnostic pop
}

- (BOOL)isViewingChat {
    // We're viewing a chat if the current jid isn't the lobby or search view
    return (self.currentJid != self.lobbyJid && self.currentJid != self.searchJid);
}

- (XMPPJID *)jidForNickname:(NSString *)nickname {
    if (!nickname || nickname.length == 0) {
        DDLogError(@"Asked for jid for nil/empty nickname - returning nil");
        return nil;
    }

    XMPPJID *nickJid = [nicknameMap objectForKey:nickname];
    if (!nickJid) {
        DDLogError(@"Asked for jid for unavailable nickname: %@", nickname);
        return [XMPPJID jidWithString:[NSString stringWithFormat:@"%@@%@", nickname, mucHost]];
    }

    return nickJid;
}

- (void)loadRoomsAndPresence {
    DDLogInfo(@"Loading room list and presence data");
    [roomList fetchItems:mucHost];
    [roster requestPresences];
}

- (BOOL)notifyUserForMessage:(XMPPMessage *)message {
    return YES;
}

- (void)postNotifWithName:(NSString *)name args:(NSDictionary *)args {
    if (dispatch_get_current_queue() == dispatch_get_main_queue()) {
        [[NSNotificationCenter defaultCenter] postNotificationName:name object:self userInfo:args];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:name object:self userInfo:args];
        });
    }
}

- (void)showFlash:(NSString *)flashMessage {
    DDLogInfo(@"Showing flash message to user: %@", flashMessage);
    NSDictionary *args = [NSDictionary dictionaryWithObject:flashMessage forKey:@"message"];
    [self postNotifWithName:HipChatFlashNotification args:args];
}

- (void)showFlash:(NSString *)flashMessage duration:(NSTimeInterval)duration {
    DDLogInfo(@"Showing flash message to user: %@", flashMessage);
    NSDictionary *args = [NSDictionary dictionaryWithObjectsAndKeys:
            flashMessage, @"message",
            [NSNumber numberWithDouble:duration], @"duration",
            nil];
    [self postNotifWithName:HipChatFlashNotification args:args];
}

- (void)showSearchResultsForQuery:(NSString *)query jid:(XMPPJID *)jid {
    DDLogInfo(@"Showing search results for query: %@ -- JID: %@", query, jid);

    NSMutableDictionary *args = [NSMutableDictionary dictionary];
    if (query) {
        [args setObject:query forKey:@"query"];
    }
    if (jid) {
        [args setObject:jid forKey:@"jid"];
    }
    [self postNotifWithName:HipChatShowSearchResultsNotification args:args];
}

- (void)sendIQ:(XMPPIQ *)iq callback:(IQCallback)callback {
    NSString *iqId = [iq attributeStringValueForName:@"id"];
    if (!iqId) {
        iqId = [XMPPStream generateUUID];
        [iq addAttributeWithName:@"id" stringValue:iqId];
    }
    if (callback) {
        [iqCallbacks setObject:callback forKey:iqId];
    }
    [self.conn sendElement:iq];
}


- (void)setActiveChatNotificationSetting:(ActiveChatNotificationSetting)setting forJid:(XMPPJID *)jid {
    NSUserDefaults *userPrefs = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *activeChatNotifications = [NSMutableDictionary dictionaryWithDictionary:[userPrefs objectForKey:@"notifyForSpecificRooms"]];
    NSNumber *settingNumber = [NSNumber numberWithInteger:setting];
    [activeChatNotifications setObject:settingNumber forKey:jid.bare];
    [userPrefs setObject:activeChatNotifications forKey:@"notifyForSpecificRooms"];
    [userPrefs synchronize];
}

- (void)setAuthTokenWithXML:(NSXMLElement *)tokenElem {
    authToken = [NSDictionary dictionaryWithObjectsAndKeys:
            [tokenElem stringValue], @"token",
            [tokenElem attributeNumberIntValueForName:@"expiration"], @"expiration",
            nil];
    [self setupCookies];
}

- (void)setDisplayNameForJid:(XMPPJID *)jid withFullName:(NSString *)name displayName:(NSString *)displayName {
    [self setDisplayNameForJid:jid withFullName:name displayName:displayName forceUpdate:NO];
}

- (void)setDisplayNameForJid:(XMPPJID *)jid withFullName:(NSString *)name displayName:(NSString *)displayName forceUpdate:(BOOL)forceUpdate {
    if (!name) {
        DDLogError(@"Attempt to set jid with empty display name: %@", jid);
        return;
    }

    // Don't need to do anything if we already have this name
    id currentEntry = [displayNames objectForKey:jid.bare];
    if (!forceUpdate && currentEntry) {
        return;
    }

    [nicknameMap setObject:jid forKey:name];
    if (displayName && displayName.length > 0) {
        [nicknameMap setObject:jid forKey:displayName];
    }

    [displayNames setObject:name forKey:jid.bare];

    if (displayName && displayName.length > 0) {
        [abbreviatedNames setObject:displayName forKey:jid.bare];
    } else {
        [abbreviatedNames setObject:[Helpers getAbbreviatedName:name] forKey:jid.bare];
    }
}

- (void)setNicknameForJid:(XMPPJID *)jid withNickname:(NSString *)nickname {
    [nicknameMap setObject:jid forKey:nickname];
}

- (void)updateMentionRegex {
    if (!self.isRosterLoaded) {
        return;
    }

    NSDictionary *users = [self getAllUsers];
    NSMutableArray *mentionNames = [NSMutableArray array];
    for (id key in users) {
        User *u = [users objectForKey:key];
        NSString *regexMention = u.regexMentionName;
        if (regexMention) {
            [mentionNames addObject:regexMention];
        } else {
            DDLogError(@"Failed to get a regex mention name for user: %@", u);
        }
    }
    mentionRegex = [NSString stringWithFormat:@"@(%@)", [mentionNames componentsJoinedByString:@"|"]];
    DDLogInfo(@"Mention regex for %@: %@", @"APP", mentionRegex);
}

- (NSString *)getConnectHost {
    if ([self connectsToHipChatServer]) {
        return [Helpers getStringPreference:@"connectHost" default:CONNECT_HOST];
    } else {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        if ([[prefs stringForKey:@"connectHost"] isEqualToString:CONNECT_HOST]) {
            [prefs removeObjectForKey:@"connectHost"];
        }
        return CONNECT_HOST;
    }
}

- (BOOL)connectsToHipChatServer {
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"useCustomConnectHost"];
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark API methods
///////////////////////////////////////////////////////////////////////

- (void)clearAuthTokenQueue {
    NSArray *queueCopy = [NSArray arrayWithArray:authTokenRequestQueue];
    [authTokenRequestQueue removeAllObjects];
    for (NSDictionary *requestInfo in queueCopy) {
        [self makeAPIRequest:[requestInfo objectForKey:@"function"]
                    withArgs:[requestInfo objectForKey:@"args"]
               tokenRequired:[[requestInfo objectForKey:@"tokenRequired"] boolValue]
           completionHandler:[requestInfo objectForKey:@"callback"]];
    }
}

- (NSDictionary *)getAPIAuthToken {
    NSTimeInterval now = [[NSDate date] timeIntervalSince1970];
    if (authToken && [[authToken objectForKey:@"expiration"] intValue] > now) {
        return authToken;
    }

    if (authTokenRequested && (now - authTokenRequestStamp) < 5000) {
        return nil;
    }
    authTokenRequestStamp = now;

    [self requestAPIAuthToken];
    return nil;
}

- (void)makeAPIRequest:(NSString *)func
              withArgs:(NSMutableDictionary *)args
         tokenRequired:(BOOL)tokenRequired
     completionHandler:(void (^)(NSString *, NSError *))callback {
    [self makeAPIRequest:func
                withArgs:args
           tokenRequired:tokenRequired
       completionHandler:callback
                 timeout:DEFAULT_REQUEST_TIMEOUT];
}

- (void)makeAPIRequest:(NSString *)func
              withArgs:(NSMutableDictionary *)args
         tokenRequired:(BOOL)tokenRequired
     completionHandler:(void (^)(NSString *, NSError *))callback
               timeout:(NSInteger)timeout {
    if (tokenRequired) {
        NSDictionary *tokenInfo = [self getAPIAuthToken];
        if (tokenInfo) {
            [args setObject:[tokenInfo objectForKey:@"token"] forKey:@"token"];
        } else {
            DDLogInfo(@"Auth token not available for API request - queueing... %@", func);
            [authTokenRequestQueue addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                    func, @"function",
                    args, @"args",
                    [NSNumber numberWithBool:tokenRequired], @"tokenRequired",
                    [callback copy], @"callback",
                    nil]];
            return;
        }
    }

    // Send up version with API request
    NSString *versionNumber = [[NSBundle mainBundle]
            objectForInfoDictionaryKey:@"CFBundleVersion"];
    [args setObject:versionNumber forKey:@"version"];

    // Add user_id if available
    if (currentUser) {
        NSString *userId = [Helpers getJidUserId:currentUser.jid];
        [args setObject:userId forKey:@"user_id"];
        NSString *groupId = [Helpers getJidGroupId:currentUser.jid];
        [args setObject:groupId forKey:@"group_id"];
    }

    DDLogInfo(@"Making API request: %@", func);

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@/api/%@", apiHost, func]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:30];
    [request setHTTPShouldHandleCookies:NO];
    [request setHTTPMethod:@"POST"];

    // Set the Content-Type HTTP header
    // Boundary is simply a random string that will never appear in the form data itself
    NSString *boundary = @"-------------F20BEB1C-11AB-4435-A5E9-A8D69C785620";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];


    // Add POST data
    NSMutableData *body = [Helpers generateHTTPRequestBodyWithArgs:args boundary:boundary];
    [request setHTTPBody:body];

    // Set the content-length based on fully-generated body data
    NSString *postLength = [NSString stringWithFormat:@"%ld", (long) [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];

    // Create the response handler
    void (^responseHandler)(NSURLResponse *, NSData *, NSError *) =
            ^(NSURLResponse *response, NSData *data, NSError *err) {
                if (![response isKindOfClass:[NSHTTPURLResponse class]]) {
                    if (callback) {
                        callback(nil, err);
                    }
                    return;
                }

                // Check to see if we got an invalid token response - if so, clear out the token and retry
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                if ([responseString rangeOfString:@"Invalid token"
                                          options:NSCaseInsensitiveSearch].location != NSNotFound) {
                    authToken = nil;
                    DDLogInfo(@"Auth token expired API request - queueing... %@", func);
                    // Store the number of times we retried
                    NSNumber *timesRetried = [args objectForKey:@"timesRetried"];
                    timesRetried = (timesRetried ?
                            [NSNumber numberWithInt:([timesRetried intValue] + 1)] :
                            [NSNumber numberWithInt:1]);

                    // If we reach the max retries, just fail out and let the callback know with an error
                    if ([timesRetried intValue] > MAX_API_RETRY_COUNT) {
                        DDLogError(@"API call failled %ld times - aborting...", (long) MAX_API_RETRY_COUNT);
                        if (callback) {
                            NSString *errMsg = @"Authentication token could not be validated. Please try again later.";
                            NSDictionary *info = [NSDictionary dictionaryWithObject:errMsg forKey:NSLocalizedDescriptionKey];
                            callback(responseString, [NSError errorWithDomain:HipChatErrorDomain code:HipChatErrorBadToken userInfo:info]);
                        }
                        return;
                    }
                    [args setObject:timesRetried forKey:@"timesRetried"];
                    [authTokenRequestQueue addObject:[NSDictionary dictionaryWithObjectsAndKeys:
                            func, @"function",
                            args, @"args",
                            [NSNumber numberWithBool:tokenRequired], @"tokenRequired",
                            [callback copy], @"callback",
                            nil]];
                    [self getAPIAuthToken];
                    return;
                }

                if (callback) {
                    callback(responseString, err);
                }
            };

    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request
                                                                  delegate:[ApiURLConnectionDelegate delegateWithResponseHandler:responseHandler]
                                                          startImmediately:NO];
    [connection setDelegateQueue:apiRequestQueue];
    [connection start];
}

- (void)requestAPIAuthToken {
    authTokenRequested = YES;

    // <iq type="get" to="chat.hipchat.com">
    //   <query xmlns="http://hipchat.com/protocol/auth" />
    // </iq>
    NSXMLElement *query = [NSXMLElement elementWithName:@"query" xmlns:@"http://hipchat.com/protocol/auth"];

    NSXMLElement *iq = [NSXMLElement elementWithName:@"iq"];
    [iq addAttributeWithName:@"type" stringValue:@"get"];
    [iq addAttributeWithName:@"to" stringValue:chatHost];
    [iq addAttributeWithName:@"id" stringValue:[conn generateUUID]];
    [iq addChild:query];

    [conn sendElement:iq];
}


- (void)savePreferences {
    // Stub function - iOS and OSX have different sets of applicable preferences
}

- (void)savePreferences:(BOOL)immediately {
    // Stub
}

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Stored sign in information
///////////////////////////////////////////////////////////////////////

// HIPCHAT UPDATE
- (void)clearSignInPassword {
}

- (BOOL)getRememberLogin {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [prefs objectForKey:@"rememberLogin"] == nil || [prefs boolForKey:@"rememberLogin"];
}

- (NSString *)getSignInEmail {
    return nil;
}

- (NSString *)getSignInPassword {
    return nil;
}

- (void)setRememberLogin:(BOOL)rememberLogin {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:rememberLogin forKey:@"rememberLogin"];
    if (!rememberLogin) {
        [self clearSignInPassword];
    }
}

- (void)storeSignInEmail:(NSString *)email andPassword:(NSString *)password {
}

@end

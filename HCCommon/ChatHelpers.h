//
//  ChatHelpers.h
//  HCCommon
//
//  Created by Christopher Rivers on 10/25/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class XMPPMessage;

@interface ChatHelpers : NSObject

+ (NSString *)initialHTML;

///////////////////////////////////////////////////////////////////////
#pragma mark Chat display logic
///////////////////////////////////////////////////////////////////////

+ (NSString *)generateBody:(NSString *)text type:(NSString *)type args:(NSDictionary *)args;
+ (NSArray *)generateJavascriptsForText:(NSString *)text type:(NSString *)type args:(NSMutableDictionary *)args;

+ (NSMutableDictionary *)getArgsForMessage:(XMPPMessage *)msg;
+ (NSString *)getChatBlockJavascriptForBody:(NSString *)body type:(NSString *)type args:(NSMutableDictionary *)args;
+ (NSString *)getChatDividerJavascript;
+ (NSString *)getChatLineJavascriptForBody:(NSString *)body args:(NSMutableDictionary *)args;
+ (NSString *)getDateDividerJavascript:(NSDate *)date insertOnTop:(BOOL)insertOnTop;
+ (NSTimeInterval)getMessageTimeForArgs:(NSDictionary *)args;
+ (NSString *)getPreviewDataJavascriptForType:(NSString *)type args:(NSMutableDictionary *)args;
+ (NSString *)getReasonForDeparture:(NSString *)status;

///////////////////////////////////////////////////////////////////////
#pragma mark Chat block HTML
///////////////////////////////////////////////////////////////////////

+ (NSString *)getChatHtmlForBody:(NSString *)body args:(NSMutableDictionary *)args;
+ (NSString *)getChatStateHtmlForBody:(NSString *)body args:(NSMutableDictionary *)args;
+ (NSString *)getFileHtmlForBody:(NSString *)body args:(NSMutableDictionary *)args;
+ (NSString *)getInfoHtmlForBody:(NSString *)body args:(NSMutableDictionary *)args;
+ (NSString *)getSystemHtmlForBody:(NSString *)body args:(NSMutableDictionary *)args;
+ (NSString *)getWelcomeHtmlForBody:(NSString *)body args:(NSMutableDictionary *)args;

///////////////////////////////////////////////////////////////////////
#pragma mark Preview data HTML
///////////////////////////////////////////////////////////////////////

+ (NSString *)getImageHtmlWithArgs:(NSMutableDictionary *)args;
+ (NSString *)getLinkHtmlWithArgs:(NSMutableDictionary *)args;
+ (NSString *)getTwitterHtmlWithArgs:(NSMutableDictionary *)args;
+ (NSString *)getVideoHtmlWithArgs:(NSMutableDictionary *)args;

@end

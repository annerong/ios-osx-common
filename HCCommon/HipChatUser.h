//
//  HipChatUser.h
//  Representation for the user currently using the app
//  Store for information / perms specific to the logged in user
//
//  Created by Christopher Rivers on 9/24/10.
//  Copyright 2010 HipChat. All rights reserved.
//

@class NSXMLElement;
@class User;
@class XMPPJID;
@class XMPPPresence;


@interface HipChatUser : NSObject {
    NSString *mentionName;
    NSString *name;
    NSMutableDictionary *perms;
    NSMutableDictionary *state;
	
    XMPPJID *jid;
    XMPPJID *nicknameJid;

    NSString *mentionHighlightRegex;
    NSString *specificMentionRegex;
}

- (BOOL)getPerm:(NSString *)permName;
- (id)initWithJid:(XMPPJID *)userJID;
- (void)setStateWithKey:(NSString *)key value:(id)value;
- (void)updateWithStartupInfo:(NSXMLElement *)data;

@property (readwrite, copy) NSString *email;
@property (readwrite, assign) BOOL isAdmin;
@property (readonly, retain) NSString *mentionHighlightRegex;
@property (readonly, retain) NSString *specificMentionRegex;
@property (readwrite, copy) XMPPJID *jid;
@property (readwrite, copy) NSString *mentionName;
@property (readwrite, copy) NSString *name;
@property (readwrite, copy) XMPPJID *nicknameJid;
@property (readwrite, copy) NSURL *photoURLLarge;
@property (readwrite, copy) NSURL *photoURLSmall;
@property (readwrite, copy) NSString *title;


@end

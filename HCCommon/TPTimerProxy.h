//
//  TPTimerProxy.h
//
//	@see http://atastypixel.com/blog/easy-delayed-messaging-using-nsproxy-and-nsinvocation/
//

@interface TPTimerProxy : NSProxy {
    id target;
    NSTimeInterval timeInterval;
    BOOL repeats;

    NSTimer *timer;
}

+ (TPTimerProxy*)scheduledTimerProxyWithTarget:(id)target timeInterval:(NSTimeInterval)timeInterval repeats:(BOOL)repeats;
- (void)invalidate;

@end
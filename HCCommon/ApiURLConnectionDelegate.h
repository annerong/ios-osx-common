//
// Created by Doug Keen on 11/7/13.
//


#import <Foundation/Foundation.h>

typedef void (^ApiResponseHandler)(NSURLResponse *, NSData *, NSError *);

@interface ApiURLConnectionDelegate : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate> {
    ApiResponseHandler responseHandler;
}
- (instancetype)initWithResponseHandler:(void (^)(NSURLResponse *, NSData *, NSError *))aResponseHandler;

+ (instancetype)delegateWithResponseHandler:(void (^)(NSURLResponse *, NSData *, NSError *))aResponseHandler;


@end
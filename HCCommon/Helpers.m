//
//  Helpers.m
//  hipchat
//
//  Created by Christopher Rivers on 9/29/10.
//  Copyright 2010 HipChat. All rights reserved.
//

#import "Helpers.h"
#import "Linkify.h"
#import "HipChatApp.h"
#import "HipChatUser.h"
#import "Room.h"
#import "RoomOccupant.h"
#import "RegexKitLite.h"
#import "User.h"
#import "XMPPFramework.h"

#import "DDLog.h"

static NSNumberFormatter *numberFormatter;
static NSDateFormatter *dateFormatter;
static NSDateFormatter *timeFormatter;
static NSDateFormatter *historyFormatter;
static NSDateFormatter *dateParser;
static NSDictionary *defaultErrorData;
static NSDictionary *fileTypeIcons;

static const NSInteger SECONDS_PER_MINUTE = 60;
static const NSInteger SECONDS_PER_HOUR = 3600;
static const NSInteger SECONDS_PER_DAY = 86400;
static const NSInteger SECONDS_PER_WEEK = 604800;
static const NSInteger SECONDS_PER_MONTH = 2592000;
static const NSInteger SECONDS_PER_YEAR = 31536000;

@implementation Helpers

+ (void)initialize {
    numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];

    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE MMMM d, yyyy"]; // Monday January 4, 2010

    if ([Helpers is24HourTime]) {
        timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setDateFormat:@"HH:mm"];
        historyFormatter = [[NSDateFormatter alloc] init];
        [historyFormatter setDateFormat:@"MMM-d HH:mm"];
    } else {
        timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setDateFormat:@"h:mm a"];
        historyFormatter = [[NSDateFormatter alloc] init];
        [historyFormatter setDateFormat:@"MMM-d h:mm a"];
    }


    dateParser = [[NSDateFormatter alloc] init];
    [dateParser setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
    [dateParser setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [dateParser setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];

    fileTypeIcons = [NSDictionary dictionaryWithObjectsAndKeys:
            @"audio.png", @"(mp3|ogg|aiff|wma|ra|wav|mid|midi|m4a|m4r)",
            @"code.png", @"(py|c|cpp|as|vb|pl|pm|java|jsp|asp)",
            @"excel.png", @"xl(?:s|t)x?",
            @"flash.png", @"(fla|swf)",
            @"illustrator.png", @"ai",
            @"image.png", @"(png|jpg|jpeg|gif)",
            @"photoshop.png", @"psd",
            @"pdf.png", @"pdf",
            @"text.png", @"(txt|rtf|md)",
            @"video.png", @"(avi|wmv|mp4|mpg|mpeg|mkv|asf|divs|flv|mov|qt|rm|xvid)",
            @"word.png", @"docx?",
            @"zip.png", @"(zip|tgz|7z|tar|rar)",
            nil];

    defaultErrorData = [NSDictionary dictionaryWithObjectsAndKeys:
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"You must reset your password before you can sign in.", @"message",
                    [NSNumber numberWithInteger:HipChatErrorResetPassword], @"code", nil],
            @"credentials-expired",
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"This account logged in from another location", @"message",
                    [NSNumber numberWithInteger:HipChatErrorLoginConflict], @"code", nil],
            @"conflict",
            //                         @"host-unknown",
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"The server encountered an error. Please try again in a few minutes.", @"message",
                    [NSNumber numberWithInteger:HipChatErrorServerError], @"code", nil],
            @"internal-server-error",
            [NSDictionary dictionaryWithObjectsAndKeys:@"Could not authenticate with the server. Please check your email and password and try again.", @"message",
                                                       [NSNumber numberWithInteger:HipChatErrorBadEmailPassword], @"code", nil],
            @"not-authorized",
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"You have been disconnected. An administrator is changing your HipChat subscription.", @"message",
                    [NSNumber numberWithInteger:HipChatErrorPlanChange], @"code", nil],
            @"plan-change",
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"Your account has been rate limited. Please try again in a few minutes.", @"message",
                    [NSNumber numberWithInteger:HipChatErrorPolicyViolation], @"code", nil],
            @"policy-violation",
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"HipChat was unable to connect to the server, please check your network connection.", @"message",
                    [NSNumber numberWithInteger:HipChatErrorServerUnreachable], @"code", nil],
            @"remote-server-not-found",
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"There was an error requesting data from the HipChat server. Please wait a moment and try again.", @"message",
                    [NSNumber numberWithInteger:HipChatErrorServerError], @"code", nil],
            @"resource-constraint",
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"HipChat is  currently unavailable. Please try again in a moment.", @"message",
                    [NSNumber numberWithInteger:HipChatErrorServiceUnavailable], @"code", nil],
            @"service-unavailable",
            [NSDictionary dictionaryWithObjectsAndKeys:
                    @"The HipChat server you're connected to is being upgraded.", @"message",
                    [NSNumber numberWithInteger:HipChatErrorSystemShutdown], @"code", nil],
            @"system-shutdown",
            nil];
}

+ (NSString *)escape:(NSString *)text {
    if (!text || [text length] == 0) {
        return text;
    }

    NSMutableString *mutableText = [NSMutableString stringWithString:text];
    [mutableText replaceOccurrencesOfString:@"&"
                                 withString:@"&amp;"
                                    options:0
                                      range:NSMakeRange(0, mutableText.length)];
    [mutableText replaceOccurrencesOfString:@"<"
                                 withString:@"&lt;"
                                    options:0
                                      range:NSMakeRange(0, mutableText.length)];
    [mutableText replaceOccurrencesOfString:@">"
                                 withString:@"&gt;"
                                    options:0
                                      range:NSMakeRange(0, mutableText.length)];

    return mutableText;
}

+ (NSString *)escapeAndLinkify:(NSString *)text
                     nameRegex:(NSString *)nameRegex
                  mentionRegex:(NSString *)mentionRegex
                      fullHtml:(BOOL)usingFullHtml
                       matches:(NSMutableArray *)matches
                      doEscape:(BOOL)doEscape
                     doLinkify:(BOOL)doLinkify
                   doEmoticons:(BOOL)doEmoticons
                    doMentions:(BOOL)doMentions {

    // Escape before linkifying, otherwise we'll escape all the HTML generated by linkify
    if (doEscape) {
        text = [Helpers escape:text];
    }

    NSMutableString *mutableText = [NSMutableString stringWithString:text];
    if (doLinkify) {
        // Do hex preview before linkification since links add hex strings
        [mutableText replaceOccurrencesOfRegex:@"(?<!\\w|\\d|/)(#[\\da-fA-F]{6})(?!\\w|\\d)"
                                    withString:@"$1 <u class='hexPreview' style='background-color: $1'>&nbsp;</u>"];

        mutableText = [NSMutableString stringWithString:[Linkify linkify:mutableText withEmotions:doEmoticons andTruncate:100 matches:matches]];
    }

    // Check for @ symbol before running mention replacement
    BOOL hasAtSymbol = ([mutableText rangeOfString:@"@"].location != NSNotFound);
    if (doMentions && hasAtSymbol) {
        // Highlight self @ mentions first, then any other @ mentions
        if ([nameRegex length] > 0) {
            [mutableText replaceOccurrencesOfRegex:nameRegex
                                        withString:@"<span onmouseover='showMentionTooltip(this);' class='atTag atTagMe'>@$1</span>"
                                           options:RKLCaseless
                                             range:NSMakeRange(0, mutableText.length)
                                             error:nil];
        }

        if (mentionRegex) {
            // Need to add the @ symbol when replacing b/c the format of the mention regex is:
            // @(name|name|name|name...)
            [mutableText replaceOccurrencesOfRegex:[NSString stringWithFormat:@"(^|\\s|\\()%@\\b", mentionRegex]
                                        withString:@"$1<span onmouseover='showMentionTooltip(this);' class='atTag'>@$2</span>"
                                           options:RKLCaseless
                                             range:NSMakeRange(0, mutableText.length)
                                             error:nil];
        } else {
//            [mutableText replaceOccurrencesOfRegex:@"(\\s)(@[\\w\\.]+|@\"[\\w\\.]+ [\\w\\.]+\")" withString:@"$1<span onmouseover='showMentionTooltip(this);' class='atTag'>$2</span>"];
//            [mutableText replaceOccurrencesOfRegex:@"^(@[\\w\\.]+|@\"[\\w\\.]+ [\\w\\.]+\")" withString:@"<span onmouseover='showMentionTooltip(this);' class='atTag'>$1</span>"];
        }
    }

    // Escape line breaks after adding word break tags
    // That way we don't screw up the <br> tags with <wbr>s
    if (doEscape && usingFullHtml) {
        [mutableText replaceOccurrencesOfRegex:@"\\r\\n" withString:@"\\n"];
        [mutableText replaceOccurrencesOfRegex:@"[\\r\\n\\u2028]"
                                    withString:@"<br />"];
    }

    if (usingFullHtml && [mutableText rangeOfString:@"  "].location != NSNotFound) {
        [mutableText replaceOccurrencesOfString:@"  "
                                     withString:@"&ensp; "
                                        options:0
                                          range:NSMakeRange(0, mutableText.length)];
    }

    return mutableText;
}

+ (NSString *)escapeAndLinkify:(NSString *)text {
    return [self escapeAndLinkify:text
                        nameRegex:nil
                     mentionRegex:nil
                         fullHtml:NO
                          matches:nil
                         doEscape:YES
                        doLinkify:YES
                      doEmoticons:YES
                       doMentions:YES];
}

+ (NSString *)escapeFileUrl:(NSString *)url {
    NSString *basepath = [url substringToIndex:[url rangeOfString:@"/" options:NSBackwardsSearch].location + 1];
    NSString *name = [url substringFromIndex:[url rangeOfString:@"/" options:NSBackwardsSearch].location + 1];
    return [NSString stringWithFormat:@"%@%@",
                                      basepath,
                                      (__bridge_transfer NSString *) CFURLCreateStringByAddingPercentEscapes(NULL,
                                              (__bridge CFStringRef) name,
                                              NULL,
                                              (CFStringRef) @"!*'();:@&=+$,/?%#[]",
                                              kCFStringEncodingUTF8)];
}

+ (NSString *)escapeUrl:(NSString *)url {
    return (__bridge_transfer NSString *) CFURLCreateStringByAddingPercentEscapes(NULL,
            (__bridge CFStringRef) url,
            NULL,
            (CFStringRef) @"!*'();:@&=+$,/?%#[]",
            kCFStringEncodingUTF8);
}

+ (NSString *)urlEncodedValue:(NSString *)str {
    return (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef) str, NULL, CFSTR(":/?#[]@!$&’()*+,;="), kCFStringEncodingUTF8));
}

+ (NSString *)stripLeadingWhitespace:(NSString *)str {
    NSInteger i = 0;
    while ((i < [str length]) && [[NSCharacterSet whitespaceCharacterSet] characterIsMember:[str characterAtIndex:i]]) {
        i++;
    }
    return [str substringFromIndex:i];
}

+ (NSString *)fileUrlWithBucket:(NSString *)bucket andPath:(NSString *)path {
    return [NSString stringWithFormat:@"https://%@/%@/%@",
                                      [Helpers getS3Endpoint:bucket],
                                      bucket,
                                      [Helpers escapeFileUrl:path]];
}

+ (NSString *)formatDate:(NSDate *)date {
    return [dateFormatter stringFromDate:date];
}

+ (NSString *)formatMultilineBlock:(NSString *)text {
    NSMutableString *mutableText = [NSMutableString stringWithString:text];
    NSInteger numLines = [Helpers getNumLines:mutableText];
    if (numLines > 1) {
        while ([[mutableText componentsMatchedByRegex:@"^(  |\t)"] count] == numLines) {
            [mutableText replaceOccurrencesOfRegex:@"^(  |\t)" withString:@""];
        }
    }
    return mutableText;
}

+ (NSString *)formatNumber:(NSInteger)number {
    return [numberFormatter stringFromNumber:[NSNumber numberWithLong:number]];
}

+ (NSString *)formatTime:(NSDate *)time {
    if (!time) {
        // If null, return right now as a time
        return [timeFormatter stringFromDate:[NSDate date]];
    } else {
        NSDate *today = [NSDate date];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *todayComponents = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit) fromDate:today];
        NSDateComponents *argComponents = [gregorian components:(NSDayCalendarUnit | NSMonthCalendarUnit) fromDate:time];

        if ([todayComponents day] != [argComponents day] ||
                [todayComponents month] != [argComponents month]) {
            return [historyFormatter stringFromDate:time];
        } else {
            return [timeFormatter stringFromDate:time];
        }
    }
}

+ (NSString *)formatTimeAgo:(NSDate *)date {
    NSDate *now = [NSDate date];
    NSTimeInterval timeDiff = ([now timeIntervalSince1970] - [date timeIntervalSince1970]);
    NSString *unit = nil;
    double count = 1;

    if (timeDiff > SECONDS_PER_YEAR) {
        count = (timeDiff / SECONDS_PER_YEAR);
        unit = @"year";
    } else if (timeDiff > SECONDS_PER_MONTH) {
        count = (timeDiff / SECONDS_PER_MONTH);
        unit = @"month";
    } else if (timeDiff > SECONDS_PER_WEEK) {
        count = (timeDiff / SECONDS_PER_WEEK);
        unit = @"week";
    } else if (timeDiff > SECONDS_PER_DAY) {
        count = (timeDiff / SECONDS_PER_DAY);
        unit = @"day";
    } else if (timeDiff > SECONDS_PER_HOUR) {
        count = (timeDiff / SECONDS_PER_HOUR);
        unit = @"hour";
    } else if (timeDiff > SECONDS_PER_MINUTE) {
        count = (timeDiff / SECONDS_PER_MINUTE);
        unit = @"minute";
    }

    if (count > 1) {
        unit = [NSString stringWithFormat:@"%@s", unit];
    }

    return (unit ? [NSString stringWithFormat:@"%ld %@ ago", (long) count, unit] : @"a moment ago");
}

+ (NSString *)formatXMPPDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    return [dateFormatter stringFromDate:date];
}

+ (NSString *)formatXMPPTimeInterval:(NSTimeInterval)time {
    return [Helpers formatXMPPDate:[NSDate dateWithTimeIntervalSince1970:time]];
}

+ (NSMutableData *)generateHTTPRequestBodyWithArgs:(NSMutableDictionary *)args boundary:(NSString *)boundary {
    NSMutableData *body = [NSMutableData data];
    for (NSString *key in [args allKeys]) {
        NSObject *value = [args objectForKey:key];
        if ([value isKindOfClass:[NSData class]]) {
            NSString *fileName = [args objectForKey:@"fileName"];
            NSString *contentType = [args objectForKey:@"contentType"];
            if (!fileName || !contentType) {
                continue;
            }
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary]
                    dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",
                                                         key,
                                                         fileName]
                    dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n",
                                                         contentType]
                    dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:(NSData *) value];
            [body appendData:[[NSString stringWithFormat:@"\r\n"]
                    dataUsingEncoding:NSUTF8StringEncoding]];
        } else if ([value isKindOfClass:[NSURL class]]) {
            //            NSURL *fileUrl = (NSURL *)value;
            //            NSURLRequest* fileUrlRequest = [NSURLRequest requestWithURL:fileUrl];
            //            NSError* error = nil;
            //            NSURLResponse* response = nil;
            //            [NSURLConnection sendSynchronousRequest:fileUrlRequest returningResponse:&response error:&error];
            //
            //            NSString* mimeType = [response MIMEType];
            //            [request addFile:value withFileName:[fileUrl lastPathComponent] andContentType:mimeType forKey:key];
        } else {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [args objectForKey:key]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }

    // Add the final boundary line and set the body data
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    return body;
}

+ (NSString *)getAbbreviatedName:(NSString *)fullName {
    NSArray *nameParts = [fullName componentsSeparatedByString:@" "];
    if ([nameParts count] <= 1 || [[nameParts objectAtIndex:(nameParts.count - 1)] length] < 1) {
        return fullName;
    }

    return [NSString stringWithFormat:@"%@ %@.", [nameParts objectAtIndex:0], [[nameParts objectAtIndex:([nameParts count] - 1)] substringToIndex:1]];
}

+ (NSDictionary *)getErrorDataForError:(NSXMLElement *)error {
    NSString *errorType = [[error childAtIndex:0] name];
    if (!errorType) {
        return nil;
    }

    return [defaultErrorData objectForKey:errorType];
}

+ (NSString *)getFileExtension:(NSString *)fileName {
    NSInteger dotPos = [fileName rangeOfString:@"." options:NSBackwardsSearch].location;
    if (dotPos == NSNotFound) {
        return nil;
    }

    return [fileName substringFromIndex:(dotPos + 1)];
}

+ (NSString *)getFileNameFromPath:(NSString *)path {
    if (!path) {
        return nil;
    }
    NSRange lastSlash = [path rangeOfString:@"/" options:NSBackwardsSearch];
    return [path substringFromIndex:lastSlash.location + 1];
}

+ (NSString *)getFileSizeString:(NSInteger)size {
    if (!size) {
        return @"";
    }

    NSString *magnitude;
    NSString *sizeString;
    if (size > 1048576) {
        [numberFormatter setMaximumFractionDigits:1];
        sizeString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:(size / 1048576.0)]];
        magnitude = @"M";
    } else if (size > 1024) {
        [numberFormatter setMaximumFractionDigits:1];
        sizeString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:(size / 1024.0)]];
        magnitude = @"K";
    } else {
        [numberFormatter setMaximumFractionDigits:2];
        sizeString = [numberFormatter stringFromNumber:[NSNumber numberWithDouble:(size / 1024.0)]];
        magnitude = @"K";
    }

    return [NSString stringWithFormat:@"%@%@", sizeString, magnitude];
}

+ (NSString *)getFileTypeIcon:(NSString *)fileName {
    NSString *icon = @"file.png";
    NSString *extension = [Helpers getFileExtension:fileName];
    if (!extension) {
        return icon;
    }

    // The keys in fileTypeIcons are the regexes to match on
    for (NSString *key in [fileTypeIcons allKeys]) {
        NSString *regex = [NSString stringWithFormat:@"%@$", key];
        if ([extension rangeOfRegex:regex
                            options:RKLCaseless
                            inRange:NSMakeRange(0, extension.length)
                            capture:0
                              error:nil].location != NSNotFound) {
            icon = [fileTypeIcons objectForKey:key];
            break;
        }
    }

    return icon;
}

+ (NSString *)getFriendlyStatusForPresence:(XMPPPresence *)presence mobile:(NSString *)mobile includeIdle:(BOOL)includeIdle {
    if (!presence) {
        return @"Unavailable";
    }

    NSString *show = presence.show;
    NSString *type = presence.type;
    if ([type caseInsensitiveCompare:@"unavailable"] == NSOrderedSame) {
        return (mobile ? @"Mobile" : @"Unavailable");
    } else if (!show || [show caseInsensitiveCompare:@"chat"] == NSOrderedSame) {
        return @"Available";
    } else if ([show caseInsensitiveCompare:@"away"] == NSOrderedSame) {
        NSString *idleString = nil;
        if (includeIdle) {
            idleString = [Helpers getIdleString:[Helpers getIdleSeconds:presence]];
        }
        return (includeIdle && idleString && idleString.length > 0 ?
                [NSString stringWithFormat:@"Idle - %@", idleString] :
                @"Idle");
    } else if ([show caseInsensitiveCompare:@"dnd"] == NSOrderedSame) {
        return @"Do not disturb";
    } else if ([show caseInsensitiveCompare:@"xa"] == NSOrderedSame) {
        return @"Away";
    }

    return @"";
}

+ (NSString *)getFriendlyStatusForUser:(User *)user includeIdle:(BOOL)includeIdle {
    XMPPPresence *presence = user.lastPresence;
    return [Helpers getFriendlyStatusForPresence:presence mobile:user.mobile includeIdle:includeIdle];
}

+ (NSString *)getIconNameForRoom:(Room *)room {
    return [Helpers getIconNameForRoom:room lightVersion:NO];
}

+ (NSString *)getIconNameForRoom:(Room *)room lightVersion:(BOOL)lightVersion {
    return ([room isPrivate] ?
            (lightVersion ? @"icon_lock_light" : @"icon_lock") :
            (lightVersion ? @"icon_people_light" : @"icon_people"));
}

+ (NSTimeInterval)getIdleSeconds:(XMPPPresence *)pres {
    NSDate *delayDate = pres.delayedDeliveryDate;

    NSTimeInterval idleSeconds = 0;
    NSXMLElement *lastActivityNode = [pres elementForName:@"query" xmlns:@"jabber:iq:last"];
    if (lastActivityNode) {
        idleSeconds = [lastActivityNode attributeIntegerValueForName:@"seconds"];
    }

    if (delayDate) {
        idleSeconds += [[NSDate date] timeIntervalSinceDate:delayDate];
    }

    return idleSeconds;
}

+ (NSString *)getIdleString:(NSTimeInterval)idleSeconds {
    if (idleSeconds == 0) {
        return @"";
    }

    // Just return "1m" if it's been under 60 seconds
    if (idleSeconds < 60) {
        return @"1m";
    }

    NSInteger days, hours, minutes = 0;
    NSInteger seconds = (int) idleSeconds;
    days = (int) (seconds / 86400);
    if (days > 0) {
        seconds %= 86400;
    }
    hours = (int) (seconds / 3600);
    if (hours > 0) {
        seconds %= 3600;
    }
    minutes = (int) (seconds / 60);

    return [NSString stringWithFormat:@"%@%@%@",
                                      (days > 0 ? [NSString stringWithFormat:@"%ldd", (long) days] : @""),
                                      (hours > 0 ? [NSString stringWithFormat:@"%@%ldh", (days > 0 ? @" " : @""), (long) hours] : @""),
                                      (minutes > 0 ? [NSString stringWithFormat:@"%@%ldm", (hours > 0 || days > 0 ? @" " : @""), (long) minutes] : @"")];
}

+ (NSString *)getJidGroupId:(XMPPJID *)jid {
    NSInteger underscorePos = [jid.user rangeOfString:@"_"].location;
    if (underscorePos == NSNotFound) {
        DDLogError(@"No underscore found when attempting to get group ID from JID: %@", jid);
        return nil;
    }
    return [jid.user substringToIndex:underscorePos];
}

+ (NSString *)getJidUserId:(XMPPJID *)jid {
    NSInteger underscorePos = [jid.user rangeOfString:@"_"].location;
    if (underscorePos == NSNotFound) {
        DDLogError(@"No underscore found when attempting to get user ID from JID: %@", jid);
        return nil;
    }
    return [jid.user substringFromIndex:underscorePos + 1];
}

+ (BOOL)getNodeBoolValue:(NSXMLElement *)root forName:(NSString *)name withDefault:(BOOL)defaultValue {
    NSXMLElement *node = [root elementForName:name];
    if (node) {
        return ([[node stringValue] caseInsensitiveCompare:@"true"] == NSOrderedSame ||
                [[node stringValue] isEqualToString:@"1"]);
    }

    return defaultValue;
}

+ (NSString *)getNodeStringValue:(NSXMLElement *)root forPath:(NSString *)path {
    NSError *err;
    return [Helpers getNodeStringValue:root forPath:path error:&err];
}

+ (NSString *)getNodeStringValue:(NSXMLElement *)root forPath:(NSString *)path error:(NSError **)errPtr {
    NSArray *nodes = [root nodesForXPath:path error:errPtr];
    if (nodes.count > 0) {
        return [[nodes objectAtIndex:0] stringValue];
    } else {
        return nil;
    }
}

+ (NSString *)getNodeStringValue:(NSXMLElement *)root forName:(NSString *)name {
    NSArray *nodes = [root elementsForName:name];
    if (nodes.count > 0) {
        return [[nodes objectAtIndex:0] stringValue];
    } else {
        return nil;
    }
}

+ (NSInteger)getNumLines:(NSString *)text {
    return [Helpers getNumLines:text isHTML:NO];
}

+ (NSInteger)getNumLines:(NSString *)text isHTML:(BOOL)isHTML {
    if (isHTML) {
        return [[text componentsMatchedByRegex:@"<br />"] count] + 1;
    } else {
        return [[text componentsMatchedByRegex:@"[\\n\\r\\u2028]+.+"] count] + 1;
    }
}

+ (NSString *)getS3Endpoint:(NSString *)bucket {
    if ([bucket isEqualToString:@"uploads-eu.hipchat.com"] ||
            [bucket isEqualToString:@"devuploads-eu.hipchat.com"]) {
        return @"s3-eu-west-1.amazonaws.com";
    }
    if ([bucket isEqualToString:@"uploads-ap.hipchat.com"] ||
            [bucket isEqualToString:@"devuploads-ap.hipchat.com"]) {
        return @"s3-ap-southeast-1.amazonaws.com";
    }

    return @"s3.amazonaws.com";
}

+ (XMPPJID *)getSenderForMessage:(XMPPMessage *)msg {
    XMPPJID *sender = nil;
    XMPPJID *from = msg.from;
    NSXMLElement *delay = [msg elementForName:@"delay" xmlns:@"urn:xmpp:delay"];
    NSString *fromJidStr = [delay attributeStringValueForName:@"from_jid"];
    if (fromJidStr) {
        sender = [XMPPJID jidWithString:fromJidStr];
    } else if ([Helpers isRoomJid:from]) {
        sender = [HipChatApp.instance jidForNickname:from.resource];
    } else {
        sender = [XMPPJID jidWithString:from.full];
    }

    return sender;
}

+ (NSString *)getStatusIconNameForOccupant:(RoomOccupant *)occupant {
    if ([occupant.role isEqualToString:VISITOR_ROLE]) {
        return ([occupant.lastPresence.show isEqualToString:@"away"] ?
                @"icon_guest_away" :
                @"icon_guest_available");
    }
    return [Helpers getStatusIconNameForPresence:occupant.lastPresence withMobile:nil];
}

+ (NSString *)getStatusIconNameForPresence:(XMPPPresence *)presence withMobile:(NSString *)mobile {
    // If we have no presence data at all, the user is considered offline
    if (presence == nil) {
        return @"icon_offline";
    }

    NSXMLElement *mobileElem = [presence elementForName:@"mobile"];
    if (mobileElem) {
        mobile = [mobileElem attributeStringValueForName:@"type"];
    }
    NSString *statusImage = @"icon_available";

    // Check for mobile resources first - it trumps regular status icons
    if ([[presence from].resource isMatchedByRegex:@"^iphone"]) {
        statusImage = @"icon_iphone";
    } else if ([[presence from].resource isMatchedByRegex:@"^android"]) {
        statusImage = @"icon_android";
    } else if ([[presence show] isEqualToString:@"away"] || [[presence show] isEqualToString:@"xa"]) {
        statusImage = @"icon_away";
    } else if ([[presence show] isEqualToString:@"dnd"]) {
        statusImage = @"icon_dnd";
    } else if ([[presence type] isEqualToString:@"unavailable"]) {
        if ([mobile isEqualToString:@"iphone"]) {
            statusImage = @"icon_iphone";
        } else if ([mobile isEqualToString:@"android"]) {
            statusImage = @"icon_android";
        } else if (mobile) {
            statusImage = @"icon_sms";
        } else {
            statusImage = @"icon_offline";
        }
    }

    return statusImage;
}

+ (NSString *)getStatusIconNameForUser:(User *)user {
    XMPPPresence *pres = user.lastPresence;
    return [Helpers getStatusIconNameForPresence:pres withMobile:user.mobile];
}

+ (NSString *)getStringPreference:(NSString *)prefName default:(NSString *)defaultVal {
    NSString *val = [[NSUserDefaults standardUserDefaults] stringForKey:prefName];
    if (!val) {
        return defaultVal;
    }
    return val;
}

+ (NSString *)getTextForError:(NSXMLElement *)errorNode {
    NSString *errorText = [[errorNode elementForName:@"text" xmlns:@"urn:ietf:params:xml:ns:xmpp-stanzas"] stringValue];

    if (!errorText || errorText.length == 0) {
        errorText = [[errorNode elementForName:@"text"] stringValue];
    }

    // If we don't find any error text in the stanza itself, try using default error messages
    if (!errorText || errorText.length == 0) {
        NSString *errorNodeName = [[errorNode childAtIndex:0] name];
        NSDictionary *errorData = [defaultErrorData objectForKey:errorNodeName];
        errorText = [errorData objectForKey:@"message"];
    }
    return errorText;
}

+ (BOOL)is24HourTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
    NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
    return (amRange.location == NSNotFound && pmRange.location == NSNotFound);
}

+ (BOOL)isBlockChatType:(NSString *)type {
    return ([type isEqualToString:@"info"] ||
            [type isEqualToString:@"system"] ||
            [type isEqualToString:@"file"] ||
            [type isEqualToString:@"welcome"]);
}

+ (BOOL)isRoomJid:(XMPPJID *)jid {
    HipChatApp *app = HipChatApp.instance;
    return [jid.domain isEqualToString:app.mucHost];
}

+ (BOOL)isUserJid:(XMPPJID *)jid {
    HipChatApp *app = HipChatApp.instance;
    return [jid.domain isEqualToString:app.chatHost];
}

+ (XMPPJID *)jidFromChatName:(NSString *)chatName {
    NSString *node = [chatName stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    node = [node stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    node = [node stringByReplacingOccurrencesOfRegex:@"[ '\"&/:<>@]" withString:@""];
    node = [node lowercaseString];
    if (node.length == 0) {
        return nil;
    }

    HipChatApp *app = HipChatApp.instance;
    return [XMPPJID jidWithString:[NSString stringWithFormat:@"%@_%@@%@",
                                                             [Helpers getJidGroupId:app.currentUser.jid],
                                                             node,
                                                             app.mucHost]];
}

+ (XMPPJID *)jidFromUserId:(NSString *)userId {
    HipChatApp *app = HipChatApp.instance;
    return [XMPPJID jidWithString:[NSString stringWithFormat:@"%@_%@@%@",
                                                             [Helpers getJidGroupId:app.currentUser.jid],
                                                             userId,
                                                             app.chatHost]];
}

+ (NSDate *)parseDate:(NSString *)dateStr {
    return [dateParser dateFromString:dateStr];
}

+ (NSString *)sanitizeXML:(NSString *)str {
    // TODO: Maybe not as necessary on the phone since it's less likely to get weird hex entities?
    return [str stringByReplacingOccurrencesOfRegex:@"[\\u0000-\\u0008\\u000b\\u000c\\u000e-\\u001f\\ud800-\\udfff\\ufffe-\\uffff]"
                                         withString:@""];
}

// Truncate a block of pasted text to have the 'Show full text' link after
// a certain number of lines / characters
+ (NSString *)truncatePaste:(NSString *)text maxLines:(NSInteger)maxLines maxLength:(NSInteger)maxLength isHTML:(BOOL)isHTML {
    if ([Helpers getNumLines:text isHTML:isHTML] <= maxLines && text.length < maxLength) {
        return text;
    }

    text = [NSString stringWithFormat:@"<span><div class='truncated'>%@</div>"
                                              "<div class='showHideLinkContainer nocode'><a name='toggle_paste' href='javascript:;'>Show full text</a></div></span>",
                                      text];

    return text;
}

@end

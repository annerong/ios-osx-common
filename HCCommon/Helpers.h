//
//  Helpers.h
//  hipchat
//
//  Created by Christopher Rivers on 9/29/10.
//  Copyright 2010 HipChat. All rights reserved.
//

#import <Foundation/Foundation.h>

#if TARGET_OS_IPHONE
#import <CoreGraphics/CoreGraphics.h>
#endif

@class NSXMLElement;
@class Room;
@class RoomOccupant;
@class User;
@class XMPPJID;
@class XMPPMessage;
@class XMPPPresence;

static const CGFloat BANNER_FRAME_HEIGHT = 40.0f;

@interface Helpers : NSObject {}

+ (NSString *)escape:(NSString *)text;
+ (NSString *)escapeAndLinkify:(NSString *)text
                     nameRegex:(NSString *)nameRegex
                  mentionRegex:(NSString *)mentionRegex
                      fullHtml:(BOOL)usingFullHtml
                       matches:(NSMutableArray *)matches
                      doEscape:(BOOL)doEscape
                     doLinkify:(BOOL)doLinkify
                   doEmoticons:(BOOL)doEmoticons
                    doMentions:(BOOL)doMentions;
+ (NSString *)escapeAndLinkify:(NSString *)text;
+ (NSString *)escapeFileUrl:(NSString *)url;
+ (NSString *)escapeUrl:(NSString *)url;
+ (NSString *)urlEncodedValue:(NSString *)str;
+ (NSString *)stripLeadingWhitespace:(NSString *)str;

+ (NSString *)fileUrlWithBucket:(NSString *)bucket andPath:(NSString *)path;

+ (NSString *)formatDate:(NSDate *)date;
+ (NSString *)formatMultilineBlock:(NSString *)text;
+ (NSString *)formatNumber:(NSInteger)number;
+ (NSString *)formatTime:(NSDate *)time;
+ (NSString *)formatTimeAgo:(NSDate *)date;
+ (NSString *)formatXMPPDate:(NSDate *)date;
+ (NSString *)formatXMPPTimeInterval:(NSTimeInterval)time;

+ (NSMutableData *)generateHTTPRequestBodyWithArgs:(NSMutableDictionary *)args boundary:(NSString *)boundary;

+ (NSString *)getAbbreviatedName:(NSString *)fullName;
+ (NSDictionary *)getErrorDataForError:(NSXMLElement *)error;
+ (NSString *)getFileExtension:(NSString *)fileName;
+ (NSString *)getFileNameFromPath:(NSString *)path;
+ (NSString *)getFileTypeIcon:(NSString *)fileName;
+ (NSString *)getFriendlyStatusForPresence:(XMPPPresence *)presence mobile:(NSString *)mobile includeIdle:(BOOL)includeIdle;
+ (NSString *)getFriendlyStatusForUser:(User *)user includeIdle:(BOOL)includeIdle;
+ (NSString *)getFileSizeString:(NSInteger)size;
+ (NSString *)getIconNameForRoom:(Room *)room;
+ (NSString *)getIconNameForRoom:(Room *)room lightVersion:(BOOL)lightVersion;
+ (NSTimeInterval)getIdleSeconds:(XMPPPresence *)pres;
+ (NSString *)getIdleString:(NSTimeInterval)idleSeconds;
+ (NSString *)getJidGroupId:(XMPPJID *)jid;
+ (NSString *)getJidUserId:(XMPPJID *)jid;
+ (BOOL)getNodeBoolValue:(NSXMLElement *)root forName:(NSString *)name withDefault:(BOOL)defaultValue;
+ (NSString *)getNodeStringValue:(NSXMLElement *)node forPath:(NSString *)path;
+ (NSString *)getNodeStringValue:(NSXMLElement *)node forPath:(NSString *)path error:(NSError **)errPtr;
+ (NSString *)getNodeStringValue:(NSXMLElement *)node forName:(NSString *)name;
+ (NSInteger)getNumLines:(NSString *)text;
+ (NSInteger)getNumLines:(NSString *)text isHTML:(BOOL)isHTML;
+ (NSString *)getS3Endpoint:(NSString *)bucket;
+ (XMPPJID *)getSenderForMessage:(XMPPMessage *)msg;
+ (NSString *)getStatusIconNameForOccupant:(RoomOccupant *)occupant;
+ (NSString *)getStatusIconNameForPresence:(XMPPPresence *)presence withMobile:(NSString *)mobile;
+ (NSString *)getStatusIconNameForUser:(User *)user;
+ (NSString *)getStringPreference:(NSString *)prefName default:(NSString *)defaultVal;
+ (NSString *)getTextForError:(NSXMLElement *)errorNode;

+ (BOOL)isBlockChatType:(NSString *)type;
+ (BOOL)isRoomJid:(XMPPJID *)jid;
+ (BOOL)isUserJid:(XMPPJID *)jid;

+ (XMPPJID *)jidFromChatName:(NSString *)chatName;
+ (XMPPJID *)jidFromUserId:(NSString *)userId;

+ (NSDate *)parseDate:(NSString *)dateStr;
+ (NSString *)sanitizeXML:(NSString *)str;
+ (NSString *)truncatePaste:(NSString *)text maxLines:(NSInteger)maxLines maxLength:(NSInteger)maxLength isHTML:(BOOL)isHTML;

@end

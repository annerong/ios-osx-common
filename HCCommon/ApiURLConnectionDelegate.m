 //
// Created by Doug Keen on 11/7/13.
//


#import "ApiURLConnectionDelegate.h"
#import "HipChatApp.h"

@implementation ApiURLConnectionDelegate {
    NSData *_data;
    NSURLResponse *_response;
}

- (instancetype)initWithResponseHandler:(ApiResponseHandler)aResponseHandler {
    self = [super init];
    if (self) {
        responseHandler = aResponseHandler;
    }

    return self;
}

+ (instancetype)delegateWithResponseHandler:(ApiResponseHandler)aResponseHandler {
    return [[self alloc] initWithResponseHandler:aResponseHandler];
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    BOOL dontValidateConnectCert = [[NSUserDefaults standardUserDefaults] boolForKey:@"useCustomConnectHost"]
            && ![[NSUserDefaults standardUserDefaults] boolForKey:@"validateConnectCert"];
    if (dontValidateConnectCert
            && [challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]
            && [[HipChatApp.instance getConnectHost] isEqualToString:challenge.protectionSpace.host]) {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust]
             forAuthenticationChallenge:challenge];
    } else {
        [challenge.sender performDefaultHandlingForAuthenticationChallenge:challenge];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    _response = response;
    [self executeHandlerIfDone];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    _data = data;
    [self executeHandlerIfDone];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    responseHandler(nil, nil, error);
}

- (void)executeHandlerIfDone {
    if (_response && _data) {
        responseHandler(_response, _data, nil);
    }
}

@end
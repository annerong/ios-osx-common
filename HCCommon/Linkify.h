//
//  Linkify.h
//  hipchat
//
//  Created by Christopher Rivers on 9/30/10.
//  Copyright 2010 HipChat. All rights reserved.
//

@class Emoticon;

@interface Linkify : NSObject {}

+ (void)initialize;


/**
 * Linkify a string, replacing text urls with <a href="url">url</a>
 *
 * @param text - String to be linkified
 * @param emoticonify - Whether to replace text emoticons with images
 * @param matchedLinks - Return param (pass by ref) - Array of links matched during linkification
 **/
+ (NSString *)linkify:(NSString *)text withEmotions:(BOOL)emoticonify andTruncate:(NSInteger)truncateLength matches:(NSMutableArray *)matches;
+ (void)addEmoticon:(Emoticon *)emoticon;
+ (void)addEmoticonWithFilename:(NSString *)filename shortcut:(NSString *)shortcut height:(NSInteger)height width:(NSInteger)width;
+ (NSArray *)getDefaultEmoticons;
+ (NSString *)matchAndReplaceWithPattern:(NSString *)pattern
							 input:(NSString *)input
							 isURL:(BOOL)isURL
						   addHTTP:(BOOL)addHTTP
					truncateLength:(NSInteger)truncateLength
						   matches:(NSMutableArray *)matchedLinks;
+ (NSString *)emoticonText:(NSString *)text;

+ (void)setEmoticonBasePath:(NSString *)path;

+ (NSString *)emoticonsBasePath;
+ (NSArray *)parenEmoticons;

@end


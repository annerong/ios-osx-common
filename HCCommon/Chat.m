//
//  Chat.m
//  HCCommon
//
//  Created by Christopher Rivers on 10/30/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

#import "Chat.h"

#import "ChatHelpers.h"
#import "ChatStateMessage.h"
#import "Helpers.h"
#import "HipChatUser.h"
#import "RegexKitLite.h"
#import "RoomList.h"
#import "TPTimerProxy.h"
#import "User.h"
#import "XMPPFramework.h"

#if TARGET_OS_IPHONE
#import "DDXMLNode+CDATA.h"

#endif

@implementation Chat

@synthesize delegate;

static NSInteger DEFAULT_HISTORY_COUNT = 50;
static NSInteger MESSAGE_EDIT_WINDOW = 50;
static NSInteger MESSAGE_CONFIRM_SECONDS = 8;

#if TARGET_OS_IPHONE
static NSInteger ROOM_JOIN_HISTORY_COUNT = 0;
#else
static NSInteger ROOM_JOIN_HISTORY_COUNT = 50;
#endif

// Globally incrementing ids for sent/displayed messages
static NSInteger currentMessageId = 1;

- (Chat *)initWithJid:(XMPPJID *)initJid delegate:(id)initDelegate {
    DDLogInfo(@"Initializing chat with jid: %@", initJid);
    HipChatApp *app = HipChatApp.instance;

    [app.conn addDelegate:self delegateQueue:HipChatApp.hipchatQueue];
    [app addDelegate:self];

    self.delegate = initDelegate;

    // Properties accessible outside of the class
    self.jid = initJid;
    self.backgroundEnterTime = 0;
    self.autocompleteList = [NSMutableArray array];
    self.autocompleteMatches = [NSMutableArray array];
    self.historyLoaded = NO;
    self.executingPreload = NO;
    self.finishedExecutingPreload = NO;

    // Member variables (should not be accessed outside the class)
    currentBlockId = 1;
    departureTimers = [NSMutableDictionary dictionary];
    historyMessages = [[ThreadSafeArray alloc] initWithQueueName:"com.hipchat.historyMessages"];
    lastMessages = [NSMutableDictionary dictionary];
    markedUnconfirmed = [NSMutableDictionary dictionary];
    preloadEvents = [[ThreadSafeArray alloc] initWithQueueName:"com.hipchat.preloadEvents"];

    // Set oldest time and chat divider time to now
    chatDividerTime = [[NSDate date] timeIntervalSince1970];
    oldestMessageTime = [[NSDate date] timeIntervalSince1970];
    typingMessageTimer = nil;
    unconfirmedMessages = [NSMutableDictionary dictionary];
    unsentMessages = [[ThreadSafeArray alloc] initWithQueueName:"com.hipchat.unsentMessages"];
    historyReplacements = [NSMutableDictionary dictionary];

    firstLoading = YES; // First time load vs re-load after reconnection

    if ([Helpers isUserJid:self.jid]) {
        XMPPMessage *activeMessage = [XMPPMessage messageWithType:@"chat" to:self.jid];
        NSXMLElement *chatState = [NSXMLElement elementWithName:ACTIVE_CHAT_STATE xmlns:@"http://jabber.org/protocol/chatstates"];
        [activeMessage addChild:chatState];
        [app.conn sendElement:activeMessage];
    }

    if (![self checkFullyLoaded]) {
        // There's a race condition where it's possible that the appFullyLoaded
        // event triggers between the time we call checkFullyLoaded here and the
        // time we actually get added as a delegate to HipChatApp (since
        // [app addDelegate] is an async operation.
        // Setup a timer to avoid this case - fixes the "no history loaded" issue
        // when initially starting up the app
        fullyLoadedTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                            target:self
                                                          selector:@selector(checkFullyLoaded)
                                                          userInfo:nil
                                                           repeats:YES];
    }

    return self;
}

- (BOOL)checkFullyLoaded {
    // Only join rooms/chats if the roster/roomlist is loaded
    // Otherwise, we will rely on the hipChatAppFullyLoaded event to trigger this logic
    HipChatApp *app = HipChatApp.instance;
    if ([app isRosterLoaded] && [app isRoomListLoaded] && [app.conn isConnected] && [app isEmoticonListLoaded]) {
        [self onAppFullyLoaded];
        return YES;
    }
    return NO;
}

- (void)onAppFullyLoaded {
    if (fullyLoadedTimer) {
        [fullyLoadedTimer invalidate];
        fullyLoadedTimer = nil;
    }

    if (initialized) {
        // Don't initialize more than once
        DDLogInfo(@"Chat has already been initialized - not initializing twice - %@", self.jid);
        return;
    }
    initialized = YES;

    DDLogInfo(@"App fully loaded - joining room / checking for load history: %@", self.jid);
    HipChatApp *app = HipChatApp.instance;

    if ([Helpers isRoomJid:self.jid]) {
        [app.roomList addDelegate:self delegateQueue:HipChatApp.hipchatQueue];

        self.room = [app getRoomInfo:self.jid];
        self.user = nil;
        [self.room addDelegate:self];

        // If we're loading history on join, set up vars / call delegate functions
        if (ROOM_JOIN_HISTORY_COUNT > 0) {
            [self beforeLoadHistory];
            [self.room joinWithHistoryCount:ROOM_JOIN_HISTORY_COUNT];
            requestedHistory = YES;
        } else {
            [self.room joinWithHistoryCount:0];
        }
    } else {
        self.user = [app getUserInfo:self.jid];
        self.room = nil;
    }

    if ([self shouldLoadHistory]) {
        // Wait a second before loading history if we need to create the room
        double delayInSeconds = 1.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            [self loadHistory];
        });
    }

    chatDividerTime = [[NSDate date] timeIntervalSince1970];
    oldestMessageTime = [[NSDate date] timeIntervalSince1970];
}

- (void)close {
    DDLogInfo(@"Closing chat: %@", self.jid);
    [self deinitialize];

    HipChatApp *app = HipChatApp.instance;
    [app.conn removeDelegate:self];
    [app removeDelegateImmediate:self];
}

- (void)dealloc {
    DDLogInfo(@"Deallocating Chat: %@", self.jid);
}

- (void)deinitialize {
    if (!initialized) {
        return;
    }

    DDLogInfo(@"Deinitializing chat: %@", self.jid);
    HipChatApp *app = HipChatApp.instance;

    // Clear these out when we disconnect
    // We will populate them again when we initialize the chat
    // after reconnection
    if (self.room) {
        [app.roomList removeDelegate:self];
        [self.room removeDelegate:self];
        self.room = nil;
    } else if (self.user) {
        self.user = nil;
    }
    initialized = NO;

    preloadEvents = [[ThreadSafeArray alloc] initWithQueueName:"com.hipchat.preloadEvents"];
    departureTimers = [NSMutableDictionary dictionary];
    historyMessages = [[ThreadSafeArray alloc] initWithQueueName:"com.hipchat.historyMessages"];
    markedUnconfirmed = [NSMutableDictionary dictionary];
    unconfirmedMessages = [NSMutableDictionary dictionary];

    // TODO: If we want to keep all the existing loaded history, don't reset
    // these values and request history "since" the lastActive date
    self.finishedExecutingPreload = NO;
    self.historyLoaded = NO;
    historyRequestId = nil;
    lastActive = 0;
    requestedHistory = NO;
}

///////////////////////////////////////////////////////////////////////
#pragma mark RoomList Delegate Methods
///////////////////////////////////////////////////////////////////////

- (void)roomListLoaded:(RoomList *)sender {
    DDLogInfo(@"Loaded room list. Refreshing link to room data for jid %@", self.jid);
    // Refresh room object when room list loads
    [self.room removeDelegate:self];
    self.room = nil;
    self.room = [HipChatApp.instance getRoomInfo:self.jid];
    [self.room addDelegate:self];
}

- (void)room:(Room *)room occupantAdded:(XMPPJID *)jid {
    if (room.isPrivate) {
        User *user = [HipChatApp.instance getUserInfo:jid];
        if (!user) {
            return;
        }
        NSString *chatText = [NSString stringWithFormat:@"%@ was added to the room", user.name];
        [self showPresenceMessage:chatText forJid:jid];
        // Add a delay to clearing the depart timer (since the presence
        // might be handled after we receive this event
        double delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            TPTimerProxy *departTimer = [departureTimers objectForKey:jid.bare];
            if (departTimer) {
                [departTimer invalidate];
                [departureTimers removeObjectForKey:jid.bare];
            }
        });
    }
}

- (void)room:(Room *)room occupantRemoved:(XMPPJID *)jid {
    if (room.isPrivate) {
        User *user = [HipChatApp.instance getUserInfo:jid];
        if (!user) {
            return;
        }
        NSString *chatText = [NSString stringWithFormat:@"%@ was removed from the room", user.name];
        [self showPresenceMessage:chatText forJid:jid];
        // Add a delay to clearing the depart timer (since the presence
        // might be handled after we receive this event
        double delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t) (delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            TPTimerProxy *departTimer = [departureTimers objectForKey:jid.bare];
            if (departTimer) {
                [departTimer invalidate];
                [departureTimers removeObjectForKey:jid.bare];
            }
        });
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark XMPPStream Delegate Methods
///////////////////////////////////////////////////////////////////////

- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message {
    [self handleMessage:message];
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence {
    XMPPJID *fromJid = [presence from];
    if (![fromJid.bare isEqualToString:self.jid.bare]) {
        return;
    }

    DDLogInfo(@"Received presence: %@", self.jid);

    if ([self.delegate respondsToSelector:@selector(chat:didReceivePresence:)]) {
        [self.delegate chat:self didReceivePresence:presence];
    }

    HipChatApp *app = HipChatApp.instance;
    XMPPJID *fromUser = nil;
    NSXMLElement *mucNode = [presence elementForName:@"x" xmlns:@"http://jabber.org/protocol/muc#user"];
    NSXMLElement *itemNode = [mucNode elementForName:@"item"];
    if (mucNode) {
        NSXMLElement *destroyNode = [mucNode elementForName:@"destroy"];
        if (destroyNode) {
            if ([delegate respondsToSelector:@selector(chat:wasDestroyedWithNode:)]) {
                [delegate chat:self wasDestroyedWithNode:destroyNode];
            }
            return;
        }

        // TODO: Handle affiliation / role data if we display room rosters

        // Store information from muc#user extension (nickname->jid mapping)
        if ([itemNode attributeForName:@"jid"]) {
            fromUser = [XMPPJID jidWithString:[[itemNode attributeForName:@"jid"] stringValue]];
            // Make sure to set display names for guest users
            [app setDisplayNameForJid:fromUser withFullName:fromJid.resource displayName:fromJid.resource];
            [app setNicknameForJid:fromUser withNickname:fromJid.resource];
        }
    }

    // For private chats, we don't display anything for presence messages
    if ([Helpers isUserJid:fromJid]) {
        return;
    }

    // TODO: Check for other extension data? (room name / topic)

    // Don't show presence messages before history is loaded
    // Or if we're in the process of loading the room
    if (!self.historyLoaded || (self.room && !self.room.rosterLoaded)) {
        return;
    }

    // Check pref for showing presence messages
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([prefs boolForKey:@"hidePresenceMessages"]) {
        return;
    }

    NSString *chatText = nil;
    NSString *name = [fromJid resource];
    NSString *type = [[presence attributeForName:@"type"] stringValue];
    NSString *status = [[presence elementForName:@"status"] stringValue];
    NSString *show = [[presence elementForName:@"show"] stringValue];

    // If no jid value was found in the muc extension, try using our nickname mapping
    // (This is unlikely - I think all presences send jid data with them)
    if (!fromUser) {
        fromUser = [app jidForNickname:name];
    }

    if (!fromUser || [[fromUser bare] isEqualToString:[app.currentUser.jid bare]]) {
        // We don't need to display presence messages for the current user
        // Also don't display anything if we were unable to get a fromUser
        return;
    }

    // Room leaves are type="unavailable", joins have no type and no show
    if ([type isEqualToString:@"unavailable"]) {
        // Don't show presence messages if it's actually a "kick" (status code 307)
        NSString *statusCode = [[mucNode elementForName:@"status"] attributeStringValueForName:@"code"];
        if (self.room.isPrivate && [statusCode isEqualToString:@"307"]) {
            return;
        }

        NSString *reason = [ChatHelpers getReasonForDeparture:status];
        if (reason) {
            chatText = [NSString stringWithFormat:@"%@ left the room (%@)", name, reason];
        } else {
            chatText = [NSString stringWithFormat:@"%@ left the room", name];
        }

        // Delay the display of departure messages to avoid repeated joins/leaves
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *args = [NSDictionary dictionaryWithObjectsAndKeys:
                    chatText, @"text",
                    fromUser, @"userJid",
                    nil];
            NSTimer *departTimer = [NSTimer scheduledTimerWithTimeInterval:10.0
                                                                    target:self
                                                                  selector:@selector(handleDepartTimer:)
                                                                  userInfo:args
                                                                   repeats:NO];
            [departureTimers setObject:departTimer forKey:fromUser.bare];
        });
    } else {
        if (!show) {
            // If we have a departure timer for this user, just invalidate it
            // Don't display this join message
            TPTimerProxy *departTimer = [departureTimers objectForKey:fromUser.bare];
            if (departTimer) {
                [departTimer invalidate];
                [departureTimers removeObjectForKey:fromUser.bare];
                return;
            }

            chatText = [NSString stringWithFormat:@"%@ joined the room", name];
        }

        if (chatText) {
            [self showPresenceMessage:chatText forJid:fromUser];
        }
    }
}

- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq {
    if (!historyRequestId) {
        return NO;
    }

    NSString *iqId = [iq attributeStringValueForName:@"id"];
    if ([iqId isEqualToString:historyRequestId]) {
        [self handleIQ:iq];
        return YES;
    }

    return NO;
}

///////////////////////////////////////////////////////////////////////
#pragma mark HipChatApp Delegate Methods
///////////////////////////////////////////////////////////////////////

- (void)hipChatAppFullyLoaded {
    DDLogInfo(@"App fully loaded - initializing chat for %@", self.jid);
    [self onAppFullyLoaded];
}

///////////////////////////////////////////////////////////////////////
#pragma mark Room Delegate Methods
///////////////////////////////////////////////////////////////////////

- (void)roomHistoryLoaded:(Room *)theRoom {
    DDLogInfo(@"Room history finished loading for %@", [theRoom jid]);

    if (ROOM_JOIN_HISTORY_COUNT > 0) {
        self.historyLoaded = YES;
        requestedHistory = NO;
        [self executePreloadEvents];
    } else {
        // Checking window is a test to see if this controller is currently visible
        // We always want to load history for the visible controller
        if ([self shouldLoadHistory]) {
            [self loadHistory];
        }
    }
}

///////////////////////////////////////////////////////////////////////
#pragma mark UI Methods
///////////////////////////////////////////////////////////////////////

- (void)addChatDivider {
    [delegate chat:self didAddJavascript:[ChatHelpers getChatDividerJavascript]];
    lastMessageFrom = nil;
}

- (void)addChatText:(NSString *)text withType:(NSString *)type andArgs:(NSMutableDictionary *)args {
    if (![self shouldAddChatText] || !self.historyLoaded) {
        DDLogInfo(@"Not ready to addChatText - adding message to preload");
        [preloadEvents addObject:[NSDictionary dictionaryWithObjectsAndKeys:type, @"type", text, @"text", args, @"args", nil]];
        return;
    }

    BOOL isCurrent = [[args objectForKey:@"isCurrent"] boolValue];
    BOOL isEcho = [[args objectForKey:@"isEcho"] boolValue];
    if (!isCurrent && !isEcho && [delegate respondsToSelector:@selector(chatShouldInsertTextAtTop:)]) {
        [args setValue:[NSNumber numberWithBool:[delegate chatShouldInsertTextAtTop:self]] forKey:@"insertOnTop"];
    } else {
        BOOL shouldInsertOnTop = !isCurrent &&
                !isEcho &&
                lastActive &&
                lastActive > [ChatHelpers getMessageTimeForArgs:args];
        [args setValue:[NSNumber numberWithBool:shouldInsertOnTop] forKey:@"insertOnTop"];
    }

    if ([type isEqualToString:@"chat"] && [self handleTypoMessage:text args:args]) {
        return;
    }

    [args setObject:[NSNumber numberWithDouble:lastActive] forKey:@"lastActive"];
    if (lastMessageFrom) {
        [args setObject:lastMessageFrom forKey:@"lastMessageFrom"];
    }
    [args setObject:[NSNumber numberWithDouble:chatDividerTime] forKey:@"chatDividerTime"];
    [args setObject:[NSNumber numberWithInteger:currentBlockId] forKey:@"currentBlockId"];
    [args setObject:[NSNumber numberWithBool:(self.room != nil)] forKey:@"isRoom"];
    [args setObject:[NSNumber numberWithDouble:oldestMessageTime] forKey:@"oldestMessageTime"];
    NSString *fromBare = ((XMPPJID *) [args objectForKey:@"from"]).bare;
    NSDictionary *historyReplacement = [historyReplacements objectForKey:fromBare];
    if (historyReplacement) {
        [args setObject:historyReplacement forKey:@"historyReplacement"];
    }

    // This call to generate javascripts will change values in the args dictionary
    NSArray *javascripts = [ChatHelpers generateJavascriptsForText:text type:type args:args];
    for (NSString *javascript in javascripts) {
        [delegate chat:self didAddJavascript:javascript];
    }

    // Update local variables based on changes to args dictionary
    NSNumber *chatDividerNumber = [args objectForKey:@"chatDividerTime"];
    if (chatDividerNumber) {
        chatDividerTime = [chatDividerNumber integerValue];
    }
    currentBlockId = [[args objectForKey:@"currentBlockId"] integerValue];
    oldestMessageTime = [[args objectForKey:@"oldestMessageTime"] integerValue];
    lastMessageFrom = [args objectForKey:@"lastMessageFrom"];
    if (lastMessageFrom) {
        NSDictionary *messageData = [NSDictionary dictionaryWithObjectsAndKeys:
                text, @"text",
                type, @"type",
                args, @"args",
                nil];
        [lastMessages setObject:messageData forKey:lastMessageFrom.bare];
    }

    // We will only clear out the history replacement for this user, it will never
    // change as a result of running generateJavascripts (only changes in handleTypoMessage)
    if (![args objectForKey:@"historyReplacement"] && fromBare) {
        [historyReplacements removeObjectForKey:fromBare];
    }

    // If we're in the background, set the last active time to whenever we entered background state
    // (since we won't have a specific msg.delayedDeliveryDate to use when we become active)
    // and we don't want to use "now" b/c when we reconnect, it would skip over history
    lastActive = (self.backgroundEnterTime ? self.backgroundEnterTime : [[args objectForKey:@"lastActive"] doubleValue]);

    if ([delegate respondsToSelector:@selector(chatDidAddText:)]) {
        [delegate chatDidAddText:self];
    }
}

- (void)addHistoryMessages {
    if ([historyMessages count] == 0) {
        DDLogInfo(@"No history messages - we must be at the top of history");
        if ([delegate respondsToSelector:@selector(chatLoadedFullHistory:)]) {
            [delegate chatLoadedFullHistory:self];
        }
        return;
    }

    DDLogInfo(@"Adding previous history to chat display");

    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"delayedDeliveryDate"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [historyMessages sortedArrayUsingDescriptors:sortDescriptors];

    if ([delegate respondsToSelector:@selector(chatWillLoadHistoryJavascript:)]) {
        [delegate chatWillLoadHistoryJavascript:self];
    }
    for (XMPPMessage *message in sortedArray) {
        [self processMessage:message];
    }
    [historyMessages removeAllObjects];

    NSString *javascript = @"showHistory();";
    [delegate chat:self didAddJavascript:javascript];

    [delegate chatFinishedLoadingHistory:self];
}

- (void)executePreloadEvents {
    // Only execute the events once the chat view is ready AND we have all the history
    if (![self shouldAddChatText]) {
        DDLogInfo(@"Not ready to add chat text. Not executing preload");
        return;
    }

    if (!self.historyLoaded) {
        DDLogInfo(@"History is not loaded. Not executing preload");
        return;
    }

    if (self.finishedExecutingPreload || self.executingPreload) {
        DDLogInfo(@"Already executed/executing preload events. Not executing twice.");
        return;
    }

    self.executingPreload = YES;
    // Clear out chat view in preparation for loading history
    [self clearChatHistory];

    DDLogInfo(@"Going through %ld preload events - %@", (long) [preloadEvents count], self.jid);
    NSArray *sortedEvents = [preloadEvents sortedArrayUsingComparator:^(id a, id b) {
        NSDate *first = [(NSDictionary *) [(NSDictionary *) a objectForKey:@"args"] objectForKey:@"time"];
        NSDate *second = [(NSDictionary *) [(NSDictionary *) b objectForKey:@"args"] objectForKey:@"time"];
        return [first compare:second];
    }];

    NSInteger numEvents = [sortedEvents count];
    NSString *lastMessage = nil;
    for (NSInteger idx = 0; idx < numEvents; idx++) {
        NSDictionary *data = [sortedEvents objectAtIndex:idx];
        if (idx == numEvents - 1 && [[data objectForKey:@"text"] isEqualToString:lastMessage]) {
            // If the last two message are duplicates, ignore the last one
            // We're assuming we probably got it while we were loading history.
            continue;
        }

        lastMessage = [data objectForKey:@"text"];
        [self addChatText:[data objectForKey:@"text"] withType:[data objectForKey:@"type"] andArgs:[data objectForKey:@"args"]];
    }

    // Check for initial message after we've executed all the preload events
    if (self.initialMessage) {
        DDLogInfo(@"Checking for initial message (%@): %@", self.jid, self.initialMessage);
        NSString *body = [[self.initialMessage elementForName:@"body"] stringValue];
        NSString *lastMsgBody = [[lastMessageReceived elementForName:@"body"] stringValue];
        if (![body isEqualToString:lastMsgBody]) {
            DDLogInfo(@"History did not end with initially received message. Processing separately...");
            [self processMessage:self.initialMessage];
        }
        self.initialMessage = nil;
    }

    [self scrollChat];

    DDLogInfo(@"Finished executing preload events - %@", self.jid);
    self.executingPreload = NO;
    self.finishedExecutingPreload = YES;
    [preloadEvents removeAllObjects];
    [delegate chatFinishedExecutingPreload:self];

    DDLogInfo(@"Going through %ld unsent messages - %@", (long) [unsentMessages count], self.jid);
    for (NSString *text in [unsentMessages immutableCopy]) {
        [self sendMessage:text];
    }
    [unsentMessages removeAllObjects];
}

- (void)removeChatDivider {
    [delegate chat:self didAddJavascript:@"removeChatDivider();"];
}

- (void)showPresenceMessage:(NSString *)text forJid:(XMPPJID *)userJid {
    [self removeLastPresence:userJid];
    NSMutableDictionary *args = [NSMutableDictionary dictionaryWithObjectsAndKeys:
            userJid, @"from",
            @"presence", @"type",
            userJid.resource, @"displayName",
            nil];
    [self addChatText:text withType:@"info" andArgs:args];
}

///////////////////////////////////////////////////////////////////////
#pragma mark Other / Helper Methods
///////////////////////////////////////////////////////////////////////

- (void)beforeLoadHistory {
    // Set the oldest message time when we initially load chat history
    // This avoids setting it upon deinitialize and then later requesting
    // previous history using the wrong date
    oldestMessageTime = [[NSDate date] timeIntervalSince1970];

    // If this is not our first time loading history (i.e. this is a reconnect)
    // then indicate this is a history refresh
    if (!firstLoading) {
        if ([delegate respondsToSelector:@selector(chatWillRefresh:)]) {
            [delegate chatWillRefresh:self];
        }
    }

    if ([delegate respondsToSelector:@selector(chatWillLoadHistory:)]) {
        [delegate chatWillLoadHistory:self];
    }

    DDLogInfo(@"Loading history for chat: %@", self.jid);
    waitingToLoadHistory = NO;
    firstLoading = NO;
}

- (void)changeTopic:(NSString *)newTopic {
    if (![Helpers isRoomJid:self.jid]) {
        DDLogInfo(@"Cannot change topic for a 1:1 conversation");
        return;
    }

    XMPPMessage *topicMessage = [NSXMLElement elementWithName:@"message"];
    [topicMessage addAttributeWithName:@"to" stringValue:self.jid.bare];
    [topicMessage addAttributeWithName:@"type" stringValue:@"groupchat"];
    NSXMLElement *subject = [NSXMLElement elementWithName:@"subject" stringValue:newTopic];
    [topicMessage addChild:subject];

    HipChatApp *app = HipChatApp.instance;
    [app.conn sendElement:topicMessage];
}

- (void)clearChatHistory {
    NSString *javascript = @"clearChat();";
    [delegate chat:self didAddJavascript:javascript];
}

- (void)handleDepartTimer:(NSNotification *)notif {
    NSDictionary *args = [notif userInfo];
    [self showPresenceMessage:[args objectForKey:@"text"] forJid:[args objectForKey:@"userJid"]];
}

- (void)handleFileUploadResponse:(NSString *)responseString error:(NSError *)err {
    if (err) {
        DDLogError(@"Got error on request to upload_file: %@", err);
        [self showFileUploadError:@"Bad response from server"];
        return;
    }

    HipChatApp *app = HipChatApp.instance;
    NSError *error;
    NSXMLDocument *result = [[NSXMLDocument alloc] initWithXMLString:responseString
                                                             options:0
                                                               error:&error];

    if (!result) {
        DDLogError(@"Error in XML response from server for upload_file: %@", [err localizedDescription]);
        [self showFileUploadError:@"Bad response from server"];
        return;
    }

    NSXMLElement *root = [result rootElement];
    if ([root elementForName:@"error"]) {
        [self showFileUploadError:[[root elementForName:@"error"] stringValue]];
        return;
    }

    if ([delegate respondsToSelector:@selector(chatFinishedUploadingFile:)]) {
        [delegate chatFinishedUploadingFile:self];
    }

    XMPPJID *uploadJid = [XMPPJID jidWithString:[[root elementForName:@"jid"] stringValue]];
    NSString *type = ([Helpers isRoomJid:uploadJid] ? @"groupchat" : @"chat");
    NSString *fileId = [[root elementForName:@"file_id"] stringValue];

    NSXMLElement *message = [NSXMLElement elementWithName:@"message"];
    [message addAttributeWithName:@"to" stringValue:uploadJid.bare];
    [message addAttributeWithName:@"type" stringValue:type];

    NSXMLElement *hcExt = [NSXMLElement elementWithName:@"x" xmlns:@"http://hipchat.com"];
    NSXMLElement *fileNode = [NSXMLElement elementWithName:@"file"];
    [fileNode addAttributeWithName:@"id" stringValue:fileId];
    [hcExt addChild:[NSXMLElement elementWithName:@"echo"]];
    [hcExt addChild:fileNode];
    [message addChild:hcExt];

    [app.conn sendElement:message];
}

- (void)handleIQ:(XMPPIQ *)iq {
    NSString *iqId = [iq attributeStringValueForName:@"id"];
    if ([iqId isEqualToString:historyRequestId]) {
        if (historyRequestId && self.historyLoaded) {
            // If we already loaded history, this must be a request to load previous history
            DDLogInfo(@"Finished loading extra history - add messages to chat - %@", self.jid);
            historyRequestId = nil;
            requestedHistory = NO;
            [self addHistoryMessages];
        } else {
            // Finished loading history - add all the messages to the chat
            DDLogInfo(@"History has finished loading - try execute preload events - %@", self.jid);
            historyRequestId = nil;
            requestedHistory = NO;
            self.historyLoaded = YES;
            [self executePreloadEvents];
        }
    }
}

- (void)handleMessage:(XMPPMessage *)message {
    XMPPJID *fromJid = [message from];
    if (![fromJid.bare isEqualToString:self.jid.bare]) {
        return;
    }
    if ([message elementForName:@"error"]) {
        return;
    }

    if ([delegate respondsToSelector:@selector(chat:didReceiveMessage:)]) {
        [delegate chat:self didReceiveMessage:message];
    }

    // Always confirm messages as we get them
    [self confirmMessage:[message attributeStringValueForName:@"id"]];

    DDLogInfo(@"Received message: %@", self.jid);
    // If we're receiving a message that's from before our last message, we can ignore it
    // This would happen if we request history on reconnection (and we already have a bunch of history)
    if (message.delayedDeliveryDate) {
        NSTimeInterval delayedTime = [message.delayedDeliveryDate timeIntervalSince1970];
        if (historyRequestId && self.historyLoaded) {
            // We are loading previous history - add this message to the list of history messages
            DDLogInfo(@"Adding message to list of history messages");
            [historyMessages addObject:message];
            return;
        }
        if (lastActive && lastActive > delayedTime) {
            // This is an old message and we're not in the process of requesting previous history
            // Ignore it
            return;
        }
    }

    // If we haven't loaded or requested history, ignore the message.
    // We'll get it when we load history (when viewing the chat for the first time)
    if (!self.historyLoaded && !requestedHistory) {
        DDLogInfo(@"History not loaded yet or not requested. Ignore message: %@", self.jid);
        return;
    }

#if !TARGET_OS_IPHONE
    BOOL isEcho = ([message attributeForName:@"id"] != nil);
    NSXMLElement *activeNode = [message elementForName:ACTIVE_CHAT_STATE xmlns:NS_CHAT_STATES];
    NSXMLElement *inactiveNode = [message elementForName:INACTIVE_CHAT_STATE xmlns:NS_CHAT_STATES];
    NSXMLElement *composingNode = [message elementForName:COMPOSING_CHAT_STATE xmlns:NS_CHAT_STATES];
    if (composingNode) {
        XMPPJID *sender = [Helpers getSenderForMessage:message];
        [self showChatStateMessage:[HipChatApp.instance getDisplayName:sender abbreviated:NO]];

        dispatch_async(dispatch_get_main_queue(), ^{
            [typingMessageTimer invalidate];
            typingMessageTimer = [NSTimer scheduledTimerWithTimeInterval:15.0
                                                                  target:self
                                                                selector:@selector(hideChatStateMessage)
                                                                userInfo:nil
                                                                 repeats:NO];
        });
    } else if ((activeNode || inactiveNode) && !isEcho) {
        // Our own messages shouldn't hide the chat state message
        [self hideChatStateMessage];
    }
#endif

    [self processMessage:message];
}

- (BOOL)handleTypoMessage:(NSString *)text args:(NSDictionary *)args {
    if ([[args objectForKey:@"isSystem"] boolValue]) {
        // System messages can't be typo-replacement messages
        return NO;
    }

    NSArray *matches = [text arrayOfCaptureComponentsMatchedByRegex:@"^s/([^/]+)/([^/]+)/?"];
    if (matches.count == 0) {
        return NO;
    }

    matches = [matches objectAtIndex:0];
    if (matches.count == 3) {
        NSString *match = [matches objectAtIndex:1];
        NSString *replacement = [matches objectAtIndex:2];
        NSString *fromBare = ((XMPPJID *) [args objectForKey:@"from"]).bare;

        if ([[args objectForKey:@"isEcho"] boolValue]) {
            // If we are getting an echoed s/// message, just return YES and don't attempt substitution
            DDLogInfo(@"Echo message - ignoring");
            return YES;
        }
        if ([[args objectForKey:@"insertOnTop"] boolValue]) {
            NSDictionary *replacementInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                    match, @"match",
                    replacement, @"replacement",
                    [args objectForKey:@"time"], @"time",
                    nil];
            [historyReplacements setObject:replacementInfo forKey:fromBare];
            return YES;
        }

        NSDictionary *lastMessageData = [lastMessages objectForKey:fromBare];
        if (!lastMessageData) {
            DDLogInfo(@"No data found for last message - not performing substitution");
            return NO;
        }

        NSDictionary *lastMessageArgs = [lastMessageData objectForKey:@"args"];
        NSString *messageId = [lastMessageArgs objectForKey:@"messageId"];
        if (!messageId) {
            DDLogInfo(@"No message ID found for last message data - not performing substitution");
            return NO;
        }

        // Messages can only be replaced for a limited amount of time after they're sent
        NSDate *lastSentDate = [lastMessageArgs objectForKey:@"time"];
        NSDate *now = [NSDate date];
        NSDate *messageDate = [args objectForKey:@"time"];
        if (!messageDate) {
            messageDate = now;
        }
        if (!lastSentDate || (messageDate.timeIntervalSince1970 - lastSentDate.timeIntervalSince1970 > MESSAGE_EDIT_WINDOW)) {
            DDLogInfo(@"S/// message it outside of edit window - not performing substitution");
            return NO;
        }

        // Generate the new HTML body for the message
        NSString *lastMessageText = [lastMessageData objectForKey:@"text"];
        NSRange matchRange = [lastMessageText rangeOfString:match];
        NSString *replacedMessage;
        if (matchRange.length) {
            replacedMessage = [lastMessageText stringByReplacingCharactersInRange:matchRange withString:replacement];
        } else {
            replacedMessage = lastMessageText;
        }
        NSString *body = [ChatHelpers generateBody:replacedMessage
                                              type:[lastMessageArgs objectForKey:@"type"]
                                              args:args];
        // Replace quotes and newlines
        body = [[body stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]
                stringByReplacingOccurrencesOfRegex:@"[\n\r]+" withString:@"\\\\n"];


        // Update the lastMessage info for this user (with the new message text)
        NSDictionary *newMessageData = [NSDictionary dictionaryWithObjectsAndKeys:
                replacedMessage, @"text",
                [lastMessageData objectForKey:@"type"], @"type",
                lastMessageArgs, @"args",
                nil];
        [lastMessages setObject:newMessageData forKey:fromBare];

        // Actually do the replacement
        NSString *javascript = [NSString stringWithFormat:@"replaceMessageById(\"%@\", \"%@\");",
                                                          messageId,
                                                          body];
        [delegate chat:self didAddJavascript:javascript];
        return YES;
    }

    return NO;
}

- (void)hideChatStateMessage {
    [typingMessageTimer invalidate];
    typingMessageTimer = nil;

    NSString *javascript = [NSString stringWithFormat:@"hideChatStateMessage();"];
    [delegate chat:self didAddJavascript:javascript];
}

- (void)loadHistory {
    if (!self.user && !self.room) {
        DDLogInfo(@"Room and user not set - must be reconnecting - aborting loadHistory: %@", self.jid);
        return;
    }

    if (self.historyLoaded || requestedHistory) {
        DDLogInfo(@"Already loading or loaded history - aborting loadHistory: %@", self.jid);
        return;
    }

    HipChatApp *app = HipChatApp.instance;
    if (![app isRosterLoaded] || ![app isRoomListLoaded]) {
        DDLogInfo(@"Roster or room list not loaded - aborting loadHistory: %@", self.jid);
        // If we haven't been initialized yet, just mark us as waiting for initialization to load history
        waitingToLoadHistory = YES;
        return;
    }

    [self beforeLoadHistory];

    NSInteger historyCount = DEFAULT_HISTORY_COUNT;
    if ([delegate respondsToSelector:@selector(numberOfHistoryStanzasToRequestForChat:)]) {
        historyCount = [delegate numberOfHistoryStanzasToRequestForChat:self];
    }
    if ([Helpers isRoomJid:self.jid]) {
        historyRequestId = [app requestHistoryForChat:self.jid beforeTime:0 count:historyCount];
    } else {
        historyRequestId = [app requestHistoryForChat:self.user.jid beforeTime:0 count:historyCount];

        if (!historyRequestId) {
            if ([delegate respondsToSelector:@selector(chatFailedToRequestHistory:)]) {
                [delegate chatFailedToRequestHistory:self];
            }
        }
    }
    requestedHistory = YES;
}

- (void)populateAutoComplete {
    DDLogInfo(@"Populating autocomplete list...");
    [self.autocompleteList removeAllObjects];

    if (![Helpers isRoomJid:self.jid] || !self.room) {
        return;
    }

    BOOL isPrivate = [self.room isPrivate];
    NSDictionary *allUsers = [[HipChatApp instance] getAllUsers];
    for (id key in allUsers) {
        User *u = [allUsers objectForKey:key];
        if (!isPrivate) {
            DDLogInfo(@"Adding %@ to autocomplete", u.name);
            [self.autocompleteList addObject:u];
        } else if ([self.room getOccupant:u.jid]) {
            DDLogInfo(@"Adding %@ to autocomplete", u.name);
            [self.autocompleteList addObject:u];
        }
    }
    DDLogInfo(@"Autocomplete list populated with %ld users", (long) [self.autocompleteList count]);
}

// Generic processing for a message (whether it be history or current)
// This will be called for all the messages added to the history queue
- (void)processMessage:(XMPPMessage *)message {
    NSMutableDictionary *args = [ChatHelpers getArgsForMessage:message];
    XMPPJID *from = [args objectForKey:@"from"];
    if (!from) {
        return;
    }

    NSString *msgId = [message attributeStringValueForName:@"id"];
    if ([msgId length] > 0) {
        [args setObject:msgId forKey:@"messageId"];
        [args setObject:[NSNumber numberWithBool:YES] forKey:@"isEcho"];
    } else {
        currentMessageId++;
        [args setObject:[NSString stringWithFormat:@"message_%ld", (long) currentMessageId] forKey:@"messageId"];
        [args setObject:[NSNumber numberWithBool:NO] forKey:@"isEcho"];
    }

    // getMessageArgs may define a body to use (e.g. for topic and guest access changes)
    // Use that if it's defined for us
    NSString *body = [args objectForKey:@"body"];
    if (!body || [body length] == 0) {
        body = [[[message elementForName:@"body"] stringValue]
                stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }

    // Ignore messages that don't have a body
    if ([body length] == 0) {
        DDLogInfo(@"Message body length is 0 - not adding chat text");
        return;
    }

    NSString *type = ([args objectForKey:@"messageType"] ? [args objectForKey:@"messageType"] : @"chat");
    [self addChatText:body withType:type andArgs:args];
    lastMessageReceived = message;
}

- (void)removeLastPresence:(XMPPJID *)userJid {
    NSString *javascript = [NSString stringWithFormat:@"removeLastPresence(\"%@\");", userJid.bare];
    [delegate chat:self didAddJavascript:javascript];
}

- (BOOL)requestPreviousHistoryWithCount:(NSInteger)count {
    if (historyRequestId) {
        return NO;
    }
    if (count < 0) {
        count = DEFAULT_HISTORY_COUNT;
    }

    HipChatApp *app = HipChatApp.instance;
    historyRequestId = [app requestHistoryForChat:self.jid beforeTime:oldestMessageTime count:count];
    requestedHistory = YES;

    // Clear out lastMessageFrom when requesting previous history
    // Since history messages will start using new block IDs, we don't want to
    // end up calling addChatLine on a new message using a block ID from a history message
    lastMessageFrom = nil;

    self.currentAnchor = [NSString stringWithFormat:@"anchor%ld", (long) currentBlockId];
    DDLogInfo(@"Setting new currentAnchor: %@", self.currentAnchor);
    NSString *javascript = [NSString stringWithFormat:@"addAnchor('%@');", self.currentAnchor];
    [delegate chat:self didAddJavascript:javascript];

    return YES;
}

- (NSString *)getNewMessageId {
    currentMessageId++;
    NSString *idString = [NSString stringWithFormat:@"message_%ld", (long) currentMessageId];
    return idString;
}

- (BOOL)runSlashCommand:(NSString *)text {
    HipChatApp *app = HipChatApp.instance;

    NSInteger spaceIdx = [text rangeOfString:@" "].location;
    NSString *command = (spaceIdx == NSNotFound ? text : [text substringToIndex:spaceIdx]);
    NSString *arg = (spaceIdx == NSNotFound ? nil : [text substringFromIndex:(spaceIdx + 1)]);
    if ([command isEqualToString:@"/available"] ||
            [command isEqualToString:@"/here"] ||
            [command isEqualToString:@"/back"]) {
        [app updatePresenceWithShow:nil status:arg];
        return YES;
    } else if ([command isEqualToString:@"/away"]) {
        [app updatePresenceWithShow:@"xa" status:arg];
        return YES;
    } else if ([command isEqualToString:@"/dnd"]) {
        [app updatePresenceWithShow:@"dnd" status:arg];
        return YES;
    } else if ([command isEqualToString:@"/part"]) {
        [app closeChat:self.jid];
        return YES;
    } else if ([command isEqualToString:@"/topic"]) {
        if (arg && [Helpers isRoomJid:self.jid]) {
            [self changeTopic:arg];
            return YES;
        }
    } else if ([command isEqualToString:@"/clear"] && !arg) {
        [self clearChatHistory];
        return YES;
    }

    return false;
}

- (void)scrollChat {
    NSString *javascript = @"scrollToBottom();";
    [delegate chat:self didAddJavascript:javascript];
}

- (void)scrollChatByPixels:(NSInteger)pixels {
    NSString *javascript = [NSString stringWithFormat:@"scrollByPixels(%ld);", (long) pixels];
    [delegate chat:self didAddJavascript:javascript];
}

- (void)sendFileUploadWithData:(NSData *)data
                   contentType:(NSString *)contentType
                      fileName:(NSString *)fileName
                          desc:(NSString *)desc {
    if (!data || !contentType || !fileName) {
        DDLogError(@"Call to upload file missing argument. DATA IS NULL? %d -- CONTENT TYPE: %@ -- FILENAME: %@",
        (data == nil), contentType, fileName);
        return;
    }

    NSMutableDictionary *args = [NSMutableDictionary dictionaryWithObjectsAndKeys:
            self.jid.bare, @"jid",
            contentType, @"contentType",
            fileName, @"fileName",
            desc, @"desc",
            data, @"Filedata",
            nil];

    HipChatApp *app = HipChatApp.instance;
    Chat *chat = self;
    [app makeAPIRequest:@"upload_file"
               withArgs:args
          tokenRequired:YES
      completionHandler:^(NSString *responseString, NSError *err) {
          [chat handleFileUploadResponse:responseString error:err];
      }];
}

- (void)sendMessage:(NSString *)text {
    // If we're getting a string directly, we must be going through unsent messages
    // Don't auto-clear the message input for these messages
    if (text == nil) {
        return;
    }

    if ([text length] == 0) {
        return;
    }

    if ([preloadEvents count] > 0 || !self.historyLoaded) {
        DDLogInfo(@"Cannot send message yet - queueing");
        [unsentMessages addObject:[NSString stringWithString:text]];

        // Mark us as waiting to load history so we send this message
        // regardless of whether the chat is visible or not
        waitingToLoadHistory = YES;
        return;
    }

    if ([[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
        return;
    }

    NSString *msgText = [text stringByReplacingOccurrencesOfRegex:@"(\r\n|\r)" withString:@"\n"];
    msgText = [Helpers sanitizeXML:msgText];

    // Check to see if we have a slash command first
    if ([msgText characterAtIndex:0] == '/' && [self runSlashCommand:msgText]) {
        return;
    }

    HipChatApp *app = HipChatApp.instance;

    NSString *idString = [self getNewMessageId];

    DDLogInfo(@"Sending message: %@", msgText);

    XMPPMessage *message = [XMPPMessage message];
    [message addAttributeWithName:@"to" stringValue:self.jid.bare];
    [message addAttributeWithName:@"id" stringValue:idString];
    if ([msgText rangeOfString:@"]]>"].location != NSNotFound) {
#if TARGET_OS_IPHONE
        NSXMLElement *body = [DDXMLNode cdataElementWithName:@"body" stringValue:msgText];
#else
        NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
        NSXMLNode *bodyTextNode = [[NSXMLNode alloc] initWithKind:NSXMLTextKind options:NSXMLNodeIsCDATA];
        bodyTextNode.stringValue = msgText;
        [body addChild:bodyTextNode];
#endif
        [message addChild:body];
    } else {
        NSXMLElement *body = [NSXMLElement elementWithName:@"body" stringValue:msgText];
        [message addChild:body];
    }

    if ([Helpers isRoomJid:self.jid]) {
        [message addAttributeWithName:@"type" stringValue:@"groupchat"];
        [app.conn sendElement:message];
    } else {
        [message addAttributeWithName:@"type" stringValue:@"chat"];
        [message addAttributeWithName:@"from" stringValue:app.currentUser.jid.full];

        NSXMLElement *chatState = [NSXMLElement elementWithName:ACTIVE_CHAT_STATE xmlns:NS_CHAT_STATES];
        [message addChild:chatState];

        NSXMLElement *hcExt = [NSXMLElement elementWithName:@"x" xmlns:@"http://hipchat.com"];
        [hcExt addChild:[NSXMLElement elementWithName:@"echo"]];
        [message addChild:hcExt];

        [app.conn sendElement:message];
    }

    [unconfirmedMessages setObject:message forKey:idString];
    NSMutableDictionary *args = [NSMutableDictionary dictionaryWithObjectsAndKeys:
            @"chat", @"messageType",
            app.currentUser.jid, @"from",
            idString, @"messageId",
            [NSDate date], @"time",
            [NSNumber numberWithBool:YES], @"isCurrent",
            [NSNumber numberWithBool:NO], @"isEcho",
            nil];
    if (self.room && self.room.mentionRegex) {
        [args setObject:self.room.mentionRegex forKey:@"mentionRegex"];
    } else if (app.mentionRegex) {
        [args setObject:app.mentionRegex forKey:@"mentionRegex"];
    }

    // Add the message to the chat immediately, mark it unconfirmed
    // if we don't have confirmation after MESSAGE_CONFIRMATION_SECONDS
    [self addChatText:msgText withType:@"chat" andArgs:args];
    [self performSelector:@selector(markMessageUnconfirmed:) withObject:idString afterDelay:MESSAGE_CONFIRM_SECONDS];
}

- (BOOL)shouldAddChatText {
    BOOL doesRespond = [delegate respondsToSelector:@selector(chatShouldAddText:)];
    return (doesRespond && [delegate chatShouldAddText:self]) || !doesRespond;

}

- (BOOL)shouldLoadHistory {
    if (waitingToLoadHistory) {
        return YES;
    }

    BOOL doesRespond = [delegate respondsToSelector:@selector(chatShouldLoadHistory:)];
    return (doesRespond && [delegate chatShouldLoadHistory:self]) || !doesRespond;

}

- (void)showChatStateMessage:(NSString *)name {
    NSString *javascript = [NSString stringWithFormat:@"addChatStateMessage(\"%@\");",
                                                      [name stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
    [delegate chat:self didAddJavascript:javascript];
}

- (void)showFileUploadError:(NSString *)errMsg {
    if ([delegate respondsToSelector:@selector(chatFailedToUploadFile:)]) {
        [delegate chatFailedToUploadFile:self];
    }
    NSMutableDictionary *args = [NSMutableDictionary dictionaryWithObjectsAndKeys:nil];

    [self addChatText:[NSString stringWithFormat:@"Error uploading file: %@", errMsg]
             withType:@"info"
              andArgs:args];
}

- (void)updateAutoCompleteMatchesWithSearch:(NSString *)searchText {
    DDLogInfo(@"Updating match status for autocomplete list...");
    [self.autocompleteMatches removeAllObjects];
    if (!searchText) {
        return;
    }

    // Set the search string now so it doesn't get changed by the main thread
    NSString *regexString = [NSString stringWithFormat:@"\\b(?i)%@", (searchText ? searchText : @"")];

    User *u = nil;
    for (u in self.autocompleteList) {
        NSInteger nameMatchLoc = [u.name rangeOfRegex:regexString].location;
        NSInteger mentionMatchLoc = [u.mentionName rangeOfRegex:regexString].location;
        if (nameMatchLoc != NSNotFound || mentionMatchLoc != NSNotFound) {
            NSNumber *firstNameMatch = [NSNumber numberWithBool:(nameMatchLoc == 0)];
            NSNumber *mentionMatch = [NSNumber numberWithBool:(mentionMatchLoc == 0)];
            NSNumber *isInRoom = [NSNumber numberWithBool:([self.room getOccupant:u.jid] != nil)];
            NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:
                    u.jid, @"jid",
                    u.name, @"name",
                    firstNameMatch, @"firstNameMatch",
                    mentionMatch, @"mentionMatch",
                    isInRoom, @"isInRoom",
                    nil];
            [self.autocompleteMatches addObject:info];
        }
    }

    [self.autocompleteMatches sortUsingComparator:^NSComparisonResult(NSDictionary *aInfo, NSDictionary *bInfo) {
        NSString *aName = [aInfo objectForKey:@"name"];
        NSInteger aIsFirstNameMatch = ([[aInfo objectForKey:@"firstNameMatch"] boolValue] ? 2 : 0);
        NSInteger aIsInRoom = ([[aInfo objectForKey:@"isInRoom"] boolValue] ? 3 : 0);
        NSInteger aIsMentionNameMatch = (!aIsFirstNameMatch && [[aInfo objectForKey:@"mentionMatch"] boolValue] ? 4 : 0);
        NSString *bName = [bInfo objectForKey:@"name"];
        NSInteger bIsFirstNameMatch = ([[bInfo objectForKey:@"firstNameMatch"] boolValue] ? 2 : 0);
        NSInteger bIsInRoom = ([[bInfo objectForKey:@"isInRoom"] boolValue] ? 3 : 0);
        NSInteger bIsMentionNameMatch = (!bIsFirstNameMatch && [[bInfo objectForKey:@"mentionMatch"] boolValue] ? 4 : 0);

        NSInteger aMatchRating = (aIsFirstNameMatch + aIsInRoom + aIsMentionNameMatch);
        NSInteger bMatchRating = (bIsFirstNameMatch + bIsInRoom + bIsMentionNameMatch);
        if (aMatchRating > bMatchRating) return NSOrderedAscending;
        else if (bMatchRating > aMatchRating) return NSOrderedDescending;
        else {
            return [aName caseInsensitiveCompare:bName];
        }
    }];
}

///////////////////////////////////////////////////////////////////////
#pragma mark Unconfirmed messages
///////////////////////////////////////////////////////////////////////

- (void)confirmMessage:(NSString *)messageId {
    if (!messageId || ![unconfirmedMessages objectForKey:messageId]) {
        return;
    }

    DDLogInfo(@"Confirming message %@", messageId);
    if ([markedUnconfirmed objectForKey:messageId]) {
        NSString *javascript = [NSString stringWithFormat:@"markMessageConfirmed(\"%@\");", messageId];
        [delegate chat:self didAddJavascript:javascript];
        [markedUnconfirmed removeObjectForKey:messageId];
    }
    [unconfirmedMessages removeObjectForKey:messageId];
}

- (void)markMessageUnconfirmed:(NSString *)messageId {
    if (![unconfirmedMessages objectForKey:messageId]) {
        // This is expected as long as we confirmed the message within 8 seconds
        // We always *try* to mark unconfirmed and most of the time it should fail here
        return;
    }

    DDLogInfo(@"Marking message as unconfirmed: %@", messageId);
    [markedUnconfirmed setObject:[NSNumber numberWithBool:YES] forKey:messageId];
    NSString *javascript = [NSString stringWithFormat:@"markMessageUnconfirmed(\"%@\");", messageId];
    [delegate chat:self didAddJavascript:javascript];
}

- (void)resendUnconfirmedMessage:(NSString *)messageId {
    NSXMLElement *msg = [unconfirmedMessages objectForKey:messageId];
    if (!msg) {
        DDLogError(@"Tried to resend unconfirmed message '%@' but no data availble", messageId);
        return;
    }

    NSString *javascript = [NSString stringWithFormat:@"markMessageConfirmed(\"%@\");", messageId];
    [delegate chat:self didAddJavascript:javascript];

    DDLogInfo(@"Resending unconfirmed message: %@", messageId);
    HipChatApp *app = HipChatApp.instance;
    [app.conn sendElement:msg];
    [self performSelector:@selector(markMessageUnconfirmed:) withObject:messageId afterDelay:8];
}

- (void)updateAutoCompleteWithSearch:(NSString *)searchText {
    // Handle autocomplete updates in serial (send it to hipchatQueue)
    dispatch_async(HipChatApp.hipchatQueue, ^{
        if ([self.autocompleteList count] == 0) {
            [self populateAutoComplete];
        }

        [self updateAutoCompleteMatchesWithSearch:searchText];

        if ([delegate respondsToSelector:@selector(chatDidUpdateAutocomplete:)]) {
            [delegate chatDidUpdateAutocomplete:self];
        }
    });
}


@end

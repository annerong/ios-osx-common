//
//  HipChatConstants.m
//  HCCommon
//
//  Created by Christopher Rivers on 10/23/12.
//  Copyright (c) 2012 Atlassian Inc. All rights reserved.
//

// Chat / Focus notifications
NSString *const HipChatFileAddedNotification = @"fileAdded";
NSString *const HipChatFlashNotification = @"flash";
NSString *const HipChatFocusChatNotification = @"focusChat";
NSString *const HipChatFocusNextChatNotification = @"focusNextChat";
NSString *const HipChatFocusPreviousChatNotification = @"focusPreviousChat";
NSString *const HipChatFocusLobbyNotification = @"focusLobby";
NSString *const HipChatFocusSettingsNotification = @"focusSettings";
NSString *const HipChatInviteNotification = @"inviteReceived";
NSString *const HipChatJoinChatNotification = @"joinChat";
NSString *const HipChatLeaveChatNotification = @"leaveChat";
NSString *const HipChatLinkAddedNotification = @"linkAdded";
NSString *const HipChatShowSearchResultsNotification = @"showSearchResults";
NSString *const HipChatCloseSearchResultsNotification = @"closeSearchResults";
NSString *const HipChatPresenceChangedNotification = @"presenceChanged";

NSString *const OWNER_AFFILIATION = @"owner";
NSString *const ADMIN_AFFILIATION = @"admin";
NSString *const MEMBER_AFFILIATION = @"member";

NSString *const MODERATOR_ROLE = @"moderator";
NSString *const MEMBER_ROLE = @"member";
NSString *const VISITOR_ROLE = @"visitor";

NSString *const PERM_CREATE_ROOMS = @"create_rooms";
NSString *const PERM_SHARE_FILES = @"file_sharing";
NSString *const PERM_EDIT_HISTORY = @"history_edit_perm";
NSString *const PERM_EDIT_PROFILE = @"profile_edit";
NSString *const PERM_GUEST_ACCESS = @"guest_access";
NSString *const PERM_PRIVATE_CHAT = @"oto_chat";
NSString *const PERM_SEND_INVITES = @"send_invites";

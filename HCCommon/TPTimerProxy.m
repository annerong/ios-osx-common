//
//  TPTimerProxy.m
//

#import "TPTimerProxy.h"


@implementation TPTimerProxy

- (id)initWithTarget:(id)aTarget timeInterval:(NSTimeInterval)aTimeInterval repeats:(BOOL)repeatFlag {
    target = aTarget;
    timeInterval = aTimeInterval;
    repeats = repeatFlag;
    return self;
}

+ (TPTimerProxy*)scheduledTimerProxyWithTarget:(id)target timeInterval:(NSTimeInterval)timeInterval repeats:(BOOL)repeats {
    return [[TPTimerProxy alloc] initWithTarget:target timeInterval:timeInterval repeats:repeats];
}

- (void)invalidate {
    if ( timer ) {
        [timer invalidate];
        timer = nil;
    }
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)selector {
    return [target methodSignatureForSelector:selector];
}

- (void)forwardInvocation:(NSInvocation *)invocation {
    [invocation setTarget:target];
    timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval invocation:invocation repeats:repeats];
}

@end
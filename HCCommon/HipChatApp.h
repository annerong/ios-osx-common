//
//  HipChatApp.h
//  hipchat
//
//  Created by Christopher Rivers on 9/22/10.
//  Copyright 2010 HipChat. All rights reserved.
//

#import "XMPPLogging.h"
#import "XMPPPing.h"

#import "ThreadSafeArray.h"
#import "ThreadSafeDictionary.h"

#import <Foundation/Foundation.h>

@class EmoticonList;
@class GCDMulticastDelegate;
@class HipChatUser;
@class NSXMLElement;
@class Profile;
@class RoomList;
@class Room;
@class Roster;
@class User;
@class XMPPIQ;
@class XMPPJID;
@class XMPPMessage;
@class XMPPPresence;
@class XMPPReconnect;
@class XMPPStream;

@protocol HipChatAppDelegate;
@class XMPPPing;

enum HipChatErrorCode {
    HipChatErrorBadEmailPassword = 1,   // No user found with the given email
    HipChatErrorBadToken,               // Token could not be validated for authentication
    HipChatErrorBadResponse,            // Unexpected / invalid response from the server
    HipChatErrorServerError,            // Internal server error (generic)
    HipChatErrorLoginConflict,          // User logged in from another location
    HipChatErrorNetworkUnavailable,     // No network connection, cannot connect
    HipChatErrorNoSessionsAvailable,    // Group is over simultaneous chatter limit, cannot log in
    HipChatErrorPlanChange,             // Admin is changing subscription - all users must disconnect
    HipChatErrorPolicyViolation,        // Rate limited
    HipChatErrorResetPassword,          // User must reset their password before continuing
    HipChatErrorServerUnreachable,      // Could not connect to the server (no internet?)
    HipChatErrorServerHostNotFound,     // Server DNS failed
    HipChatErrorServerHostSSLInvalid,   // Server SSL Certificate invalid
    HipChatErrorServiceUnavailable,     // HipChat server is not available, but we have internet access
    HipChatErrorSystemShutdown,         // Server upgrade - temporary disconnect
    HipChatErrorUnknown,                // Unknown issue
};
typedef enum HipChatErrorCode HipChatErrorCode;

typedef void (^IQCallback)(XMPPIQ *);

extern NSString *const XMPPStreamErrorDomain;

@interface HipChatApp : NSObject <XMPPPingDelegate, XMPPStreamDelegate, NSCoding> {

    /** MULTI-THREADED COLLECTIONS
     * Be careful when using collections that may be accessed by multiple thread! They should always
     * use the ThreadSafe* class equivalents, which wrap collection operations in dispatch_sync for
     * reads, and dispatch_barrier_async for writes (on a collection-specific queue). This ensures that
     * reads may happen concurrently, but writes create a synchronization point via barrier block.
     * If you need to perform some custom operation or collection of operations that must be atomic,
     * manually wrap your operation in dispatch_barrier_[a]sync and directly use the collection's
     * collectionQueue and internalArray/Dict properties.
     */
    ThreadSafeDictionary *activeChats;
    ThreadSafeArray *activeChatJids;
    ThreadSafeDictionary *abbreviatedNames;
    ThreadSafeArray *autoJoinList;
    ThreadSafeDictionary *displayNames;
    ThreadSafeDictionary *nicknameMap;
    ThreadSafeDictionary *profiles;
    ThreadSafeDictionary *unreadChats;
    ThreadSafeDictionary *unreadMessageCounts;

    EmoticonList *emoticonList;
    RoomList *roomList;

    XMPPStream *conn;
    XMPPReconnect *reconnector;
    Roster *roster;

    NSTimeInterval authTokenRequestStamp;
    XMPPJID *currentJid;
    XMPPJID *currentNicknameJid;
    NSString *currentPresenceStatus;
    NSString *currentPresenceShow;
    NSDate *disconnectedAt;
    XMPPJID *inviteJid;
    NSString *mentionRegex;
    NSString *nameRegex;
    NSString *previousPresenceStatus;
    NSString *previousPresenceShow;

    BOOL authTokenRequested;
    BOOL doAutoJoin;
    BOOL doReconnect;
    BOOL isAutoJoinFinished;
    BOOL isAutoJoinLoaded;
    BOOL isConnecting;
    BOOL isFirstConnect;
    BOOL isFullyLoaded;
    BOOL isEmoticonListLoaded;
    BOOL isManualDisconnect;
    BOOL isManualReconnect;
    BOOL isPresenceLoaded;
    BOOL isRoomListLoaded;
    BOOL isRosterLoaded;
    BOOL isUserLoaded;

    NSString *apiHost;
    NSString *chatHost;
    NSString *connectHost;
    NSString *deviceToken;
    NSString *mucHost;
    UInt16 port;
    NSString *resource;
    NSString *webHost;

    HipChatUser *currentUser;

    NSMutableArray *apiErrorRequestQueue;
    NSOperationQueue *apiRequestQueue;
    NSMutableDictionary *appPerms;
    NSDictionary *authToken;
    NSMutableArray *authTokenRequestQueue;
    NSMutableDictionary *iqCallbacks;

    GCDMulticastDelegate <HipChatAppDelegate> *multicastDelegate;
}

+ (dispatch_queue_t)hipchatQueue;

///////////////////////////////////////////////////////////////////////
#pragma mark Singleton methods
///////////////////////////////////////////////////////////////////////

+ (HipChatApp *)instance;

// PRIVATE METHODS - Only used by subclasses
+ (void)setSharedInstance:(HipChatApp *)instance;

+ (HipChatApp *)getSharedInstance;

///////////////////////////////////////////////////////////////////////
#pragma mark Delegation
///////////////////////////////////////////////////////////////////////

- (void)addDelegate:(id)delegate;

- (void)removeDelegate:(id)delegate;

- (void)removeDelegateImmediate:(id)delegate;

- (void)postDelegateBlock:(dispatch_block_t)block;

///////////////////////////////////////////////////////////////////////
#pragma mark Connect / Disconnect methods
///////////////////////////////////////////////////////////////////////

- (void)clearUserData;

- (void)connect;

- (void)connectWithEmail:(NSString *)email password:(NSString *)password;

- (void)disconnect;

- (void)handleErrorStanza:(NSXMLElement *)stanza;

- (void)handleXMPPError:(NSXMLElement *)error;

- (BOOL)reconnect;

- (BOOL)secureConnection:(NSError **)errPtr;

- (NSError *)sendConnectFailed:(NSString *)errMsg code:(NSInteger)code;

- (void)setHostsWithAuth:(NSXMLElement *)authResponse;

- (void)setReconnectDelay:(NSInteger)delay andInterval:(NSInteger)interval;

- (void)signOutUser;

- (void)signOutUserAndClearCredentials;

///////////////////////////////////////////////////////////////////////
#pragma mark XMPP Stream Delegate methods
///////////////////////////////////////////////////////////////////////

- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender;

- (void)onDisconnect;

///////////////////////////////////////////////////////////////////////
#pragma mark Chat methods
///////////////////////////////////////////////////////////////////////

- (void)autoJoinChats;

- (void)addToAutoJoin:(XMPPJID *)jid;

- (void)addToAutoJoin:(XMPPJID *)jid atIndex:(NSInteger)index;

- (void)clearUnreadCount:(XMPPJID *)jid;

- (void)closeChat:(XMPPJID *)jid;

- (void)closeChat:(XMPPJID *)jid isManual:(BOOL)isManual;

- (void)closeChat:(XMPPJID *)jid notifyDelegates:(BOOL)notifyDelegates isManual:(BOOL)isManual;

- (void)focusChat:(XMPPJID *)jid;

- (void)focusLobby;

- (void)focusNextChat;

- (void)focusPreviousChat;

- (ActiveChatNotificationSetting)getActiveChatNotificationSetting:(XMPPJID *)jid;

- (NSInteger)getTotalUnreadCount;

- (NSInteger)getUnreadCountForJid:(XMPPJID *)jid;

- (BOOL)getUnreadForJid:(XMPPJID *)jid;

- (void)incrementUnreadForJid:(XMPPJID *)jid;

- (NSUInteger)getActiveChatIndex:(XMPPJID *)jid;

- (void)enqueueActiveChatJid:(XMPPJID *)jid;

- (BOOL)isActiveChat:(XMPPJID *)jid;

- (id)getActiveChat:(XMPPJID *)jid;

- (void)setActiveChat:(id)chat forJid:(XMPPJID *)jid;
    
- (void)setActiveChatNotificationSetting:(ActiveChatNotificationSetting)setting forJid:(XMPPJID *)jid;

- (void)joinChat:(XMPPJID *)jid;

- (void)joinChat:(XMPPJID *)jid withFocus:(BOOL)doFocus;

- (void)joinChat:(XMPPJID *)jid withMessage:(XMPPMessage *)initialMessage;

- (void)removeFromAutoJoin:(XMPPJID *)jid;

- (void)setAutoJoin:(NSMutableArray *)newAutoJoin;

///////////////////////////////////////////////////////////////////////
#pragma mark IQ methods
///////////////////////////////////////////////////////////////////////

- (NSString *)requestHistoryForChat:(XMPPJID *)jid beforeTime:(NSTimeInterval)fetchTime count:(NSInteger)historyCount;

- (NSString *)requestProfileForJid:(XMPPJID *)jid callback:(IQCallback)callback;

- (NSString *)requestStartup;

- (void)handleStartupResponse:(XMPPIQ *)iq;

- (Profile *)handleProfileResponse:(XMPPIQ *)iq;

///////////////////////////////////////////////////////////////////////
#pragma mark Presence methods
///////////////////////////////////////////////////////////////////////

- (void)sendPresenceWithArgs:(NSDictionary *)args;

- (void)updatePresenceWithShow:(NSString *)show status:(NSString *)status;

- (void)updatePresenceWithShow:(NSString *)show status:(NSString *)status idle:(NSTimeInterval)secondsIdle;

- (void)updatePresenceWithStatus:(NSString *)status;

- (NSSet *)jidsWithoutPresences;

- (BOOL)jidNeedsPresence:(NSString *)jid;

- (BOOL)jidHasPresence:(NSString *)jid;


///////////////////////////////////////////////////////////////////////
#pragma mark Helper Methods
///////////////////////////////////////////////////////////////////////

- (NSMutableArray *)generateAutoJoinArg;

- (NSDictionary *)getAllRooms;

- (NSDictionary *)getAllUsers;

- (NSString *)getDisplayName:(XMPPJID *)jid abbreviated:(BOOL)abbreviate;

- (Profile *)getProfileData:(XMPPJID *)jid;

- (Room *)getRoomInfo:(XMPPJID *)jid;

- (User *)getUserInfo:(XMPPJID *)jid;

- (void)handleMessage:(XMPPMessage *)message;

- (void)handleRosterPush:(XMPPIQ *)iq;

- (BOOL)isAppFocused; // Override in subclass

- (BOOL)isViewingChat;

- (XMPPJID *)jidForNickname:(NSString *)nickname;

- (BOOL)notifyUserForMessage:(XMPPMessage *)message;

- (void)postNotifWithName:(NSString *)name args:(NSDictionary *)args;

- (void)showFlash:(NSString *)flashMessage;

- (void)showFlash:(NSString *)message duration:(NSTimeInterval)duration;

- (void)showSearchResultsForQuery:(NSString *)query jid:(XMPPJID *)jid;

- (void)sendIQ:(XMPPIQ *)iq callback:(IQCallback)callback;

- (void)setDisplayNameForJid:(XMPPJID *)jid withFullName:(NSString *)name displayName:(NSString *)displayName;

- (void)setDisplayNameForJid:(XMPPJID *)jid withFullName:(NSString *)name displayName:(NSString *)displayName forceUpdate:(BOOL)forceUpdate;

- (void)setNicknameForJid:(XMPPJID *)jid withNickname:(NSString *)nickname;

- (void)updateMentionRegex;

- (NSString *)getConnectHost;

- (BOOL) connectsToHipChatServer;

///////////////////////////////////////////////////////////////////////
#pragma mark API Methods
///////////////////////////////////////////////////////////////////////

- (void)clearAuthTokenQueue;

- (NSDictionary *)getAPIAuthToken;

- (void)makeAPIRequest:(NSString *)func
              withArgs:(NSMutableDictionary *)args
         tokenRequired:(BOOL)tokenRequired
     completionHandler:(void (^)(NSString *, NSError *))callback;

- (void)makeAPIRequest:(NSString *)func
              withArgs:(NSMutableDictionary *)args
         tokenRequired:(BOOL)tokenRequired
     completionHandler:(void (^)(NSString *, NSError *))callback
               timeout:(NSInteger)timeout;

- (void)savePreferences;

- (void)savePreferences:(BOOL)immediately;

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Stored sign in information
///////////////////////////////////////////////////////////////////////

- (void)clearSignInPassword;

- (BOOL)getRememberLogin;

- (NSString *)getSignInEmail;

- (NSString *)getSignInPassword;

- (void)setRememberLogin:(BOOL)rememberLogin;

- (void)storeSignInEmail:(NSString *)email andPassword:(NSString *)password;

///////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Properties
///////////////////////////////////////////////////////////////////////

@property(readonly) NSDictionary *activeChats;
@property(readonly) NSArray *activeChatJids;
@property(readwrite, copy) NSString *apiHost;
@property(readwrite, copy) NSString *chatHost;
@property(readonly, retain) XMPPStream *conn;
@property(readwrite, retain) XMPPJID *currentJid;
@property(readonly, retain) HipChatUser *currentUser;
@property(readwrite, copy) NSString *deviceToken;
@property(readonly) BOOL doReconnect;
@property(readonly, retain) EmoticonList *emoticonList;
@property(readonly) BOOL isAutoJoinFinished;
@property(readonly) BOOL isConnecting;
@property(readonly) BOOL isEmoticonListLoaded;
@property(readonly) BOOL isManualDisconnect;
@property(readonly) BOOL isPresenceLoaded;
@property(readonly) BOOL isRoomListLoaded;
@property(readonly) BOOL isRosterLoaded;
@property(readonly) BOOL isFullyLoaded;
@property(readwrite, retain) XMPPJID *jidToFocus;
@property NSError *lastConnectError;
@property(copy, nonatomic) NSString *loginEmail;
@property(copy, nonatomic) NSString *loginPassword;
@property(readwrite, copy) XMPPJID *lobbyJid;
@property(readonly, retain) NSString *mentionRegex;
@property(readwrite, copy) NSString *mucHost;
@property(readonly, retain) NSString *nameRegex;
@property(nonatomic, strong) XMPPPing *ping;
@property(readonly, retain) RoomList *roomList;
@property(readonly) Roster *roster;
@property(readwrite, copy) XMPPJID *searchJid;
@property(readwrite, copy) NSString *webHost;

@end


///////////////////////////////////////////////////////////////////////
#pragma mark GCDMulticastDelegate protocol
///////////////////////////////////////////////////////////////////////

@protocol HipChatAppDelegate
@optional

/**
 * The app has finished auto-joining any rooms
 **/
- (void)hipChatAppAutoJoinFinished;

/**
 * Connecting failed because of some app-specific logic (e.g. bad email, API is down)
 *
 * To listen for base xmpp connect / disconnect use the conn variable
 * and XMPPStreamDelegate functions
**/
- (void)hipChatAppConnectFailed:(NSError *)err;

/**
 * Connect action succeeded (counterpart to connectFailed)
 * Should essentially be equivalent to listening for xmppStreamDidConnect
 * on the .conn property
 **/
- (void)hipChatAppConnectSucceeded;

/**
 * A chat was joined or closed
 */
- (void)hipChatAppChatsChanged;

/**
 * The app is ready to join rooms / start chatting
 * The roster and room list have been loaded (all display names available)
 **/
- (void)hipChatAppFullyLoaded;

/**
 * The total unread count or unread state has changed (i.e. we received a message with a non-empty body)
 **/
- (void)hipChatAppUnreadChangedForJid:(XMPPJID *)jid;

/**
 * Called when we are about to manually disconnect (user clicked sign out)
 **/
- (void)hipChatAppWillManuallyDisconnect;

@end


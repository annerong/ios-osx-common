//
//  HipChatUser.m
//  hipchat
//
//  Created by Christopher Rivers on 9/24/10.
//  Copyright 2010 HipChat. All rights reserved.
//

#import "HipChatUser.h"
#import "Helpers.h"

#import "XMPP.h"

@implementation HipChatUser

@synthesize email;
@synthesize jid;
@synthesize mentionName;
@synthesize name;
@synthesize mentionHighlightRegex;
@synthesize specificMentionRegex;
@synthesize nicknameJid;
@synthesize photoURLLarge;
@synthesize photoURLSmall;
@synthesize title;

- (id)init {
    if (self = [super init]) {
        state = [NSMutableDictionary dictionary];
        perms = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id)initWithJid:(XMPPJID *)userJID {
    self.jid = userJID;
    self.name = nil;

    return [self init];
}

- (BOOL)getPerm:(NSString *)permName {
    NSNumber *permVal = [perms objectForKey:permName];
    return permVal != nil && [permVal boolValue];

}

- (void)setStateWithKey:(NSString *)key value:(NSObject *)value {
    [state setValue:value forKey:key];
}

- (void)updateWithStartupInfo:(NSXMLElement *)data {
    DDLogInfo(@"Setting current user's name to %@", [[data elementForName:@"name"] stringValue]);
    NSString *adminString = [[data elementForName:@"is_admin"] stringValue];
    self.isAdmin = ([adminString isEqualToString:@"True"] || [adminString isEqualToString:@"1"]);
    self.email = [[data elementForName:@"email"] stringValue];
    self.mentionName = [[data elementForName:@"mention_name"] stringValue];
    self.name = [[data elementForName:@"name"] stringValue];
    self.photoURLLarge = [NSURL URLWithString:[[data elementForName:@"photo_large"] stringValue]];
    self.photoURLSmall = [NSURL URLWithString:[[data elementForName:@"photo_small"] stringValue]];
    self.title = [[data elementForName:@"title"] stringValue];

    [[NSUserDefaults standardUserDefaults] setObject:self.name forKey:@"account_name"];

    // Detect @mentionName, @all for sending notifications
    mentionHighlightRegex = [NSString stringWithFormat:@"@(%@\\b|all\\b|here\\b)", self.mentionName];
    specificMentionRegex = [NSString stringWithFormat:@"@(%@\\b)", self.mentionName];

    // Get notification preferences info
    NSXMLElement *prefs = [data elementForName:@"preferences"];
    NSArray *nodes = [[prefs elementForName:@"notifications"] children];
    if (nodes.count > 0) {
        NSMutableDictionary *notifs = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                [NSNumber numberWithBool:NO], @"push_mention",
                [NSNumber numberWithBool:NO], @"push_oto_message",
                [NSNumber numberWithBool:NO], @"push_room_invite",
                nil];
        for (NSXMLElement *elem in nodes) {
            NSString *event = [elem name];
            NSString *method = [elem stringValue];
            bool pushEnabled = [method rangeOfString:@"ios"].location != NSNotFound;

            DDLogInfo(@"Push enabled for %@? %@", event, pushEnabled ? @"YES" : @"NO");
            if (pushEnabled) {
                NSString *prefKey = [NSString stringWithFormat:@"push_%@", event];
                [notifs setObject:[NSNumber numberWithBool:pushEnabled] forKey:prefKey];
            }
        }

        // Set preferences in NSUserDefaults
        for (NSString *key in [notifs allKeys]) {
            [[NSUserDefaults standardUserDefaults] setInteger:[[notifs objectForKey:key] intValue] forKey:key];
        }

    } else {
        DDLogError(@"Received no notification preferences");
    }

    // Get perms data
    NSXMLElement *permNode = [data elementForName:@"perms"];
    for (NSXMLElement *elem in [permNode children]) {
        NSString *permVal = [elem stringValue];
        if ([permVal isEqualToString:@"all"]) {
            [perms setObject:[NSNumber numberWithBool:YES] forKey:[elem name]];
        } else {
            BOOL isAdmin = [permVal isEqualToString:@"admins"] && self.isAdmin;
            [perms setObject:[NSNumber numberWithBool:isAdmin] forKey:[elem name]];
        }
    }

//    <notifyForTag>true</notifyForTag>
//    <chatToFocus>10804_hc_ops@conf.hipchat.com</chatToFocus>
//    <timeFormat24Hour>false</timeFormat24Hour>
//    <showToasters>true</showToasters>
//    <notifyForPrivate>true</notifyForPrivate>
//    <secondsToIdle>300</secondsToIdle>
//    <notifyForRoom>false</notifyForRoom>
//    <visualNotifications>true</visualNotifications>
//    <hidePresenceMessages>false</hidePresenceMessages>
//    <disableSounds>true</disableSounds>
    NSUserDefaults *localPrefs = [NSUserDefaults standardUserDefaults];
    NSArray *boolPrefs = [NSArray arrayWithObjects:@"notifyForTag", @"notifyForPrivate", @"notifyForPrivateRoom",
                                                   @"notifyForRoom", @"showToasters", @"visualNotifications", @"disableSounds",
                                                   @"timeFormat24Hour", @"hidePresenceMessages", @"checkMinorUpdates", @"enableEmoticons", nil];
    NSString *prefName = nil;
    for (prefName in boolPrefs) {
        BOOL prefValue = [Helpers getNodeBoolValue:prefs forName:prefName withDefault:YES];
        [localPrefs setBool:prefValue forKey:prefName];
    }
}

@end
